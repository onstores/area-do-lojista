(function() {
	var App = this._app$;
	var vars = this._var$;
	var Utils = vars.Utils;

	var component = vars.compLoader.vueLazyLoad;
	var appPage = function(id) {
		return component(String('app/pages/'+id).replace(/\/+/g,'--'));
	}
	var routes = [
		{ path: '/', name: 'index', component: appPage('index') },
		// { path: '/mobile.html', redirect: '/' },
		{ path: '/login', name: 'login', component: appPage('login') },
		{ path: '/admin', name: 'admin',  component: appPage('admin'),
			children: [
				{ path: '', redirect: 'recebimentos' },
				{ path: 'recebimentos', component: appPage('admin/recebimentos') },
				// { path: 'minhas-vendas', component: appPage('admin/minhas-vendas') },
				{ path: 'detalhes-pedido/:id?', component: appPage('admin/detalhes-pedido') },
				{ path: 'busca/:busca', component: appPage('admin/busca') }
			]
		},
		{ path: '/loja', component: appPage('loja'),
			children: [
				{ path: '', redirect: 'recebimentos' },
				{ path: 'configuracoes', component: appPage('loja/configuracoes') },
				{ path: 'recebimentos', component: appPage('loja/recebimentos') },
				// { path: 'minhas-vendas', component: appPage('loja/minhas-vendas') },
				{ path: 'detalhes-pedido/:id?', component: appPage('loja/detalhes-pedido') },
				{ path: 'busca/:busca', component: appPage('loja/busca') }
			]
		},
		{ path: '/cadastro', component: appPage('cadastro') },
		{ path: '/cadastro-efetuado', component: appPage('cadastro') },
		{ path: '/api',
			children: [
				{ path: '*', redirect: '/' }
			]
		}
	];
	var router = new VueRouter({
		mode: 'history',
		base: App.routerBaseUrl || '/',
		routes: routes
	});

	App.router = router;
})();
