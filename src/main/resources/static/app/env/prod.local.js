// (function() {
// 	var App = this._app$;
// 	var Env = {
// 		name: 'prod'
// 	};
// 	App.Env = Env;
// })();

(function() {
    var urlAPI = "http://ec2-3-87-114-13.compute-1.amazonaws.com";
    var vars = this._var$;
    var App = this._app$;
    var Utils = vars.Utils;
    var Services = {};
    var Env = {
        name: 'local',
        Services: Services
    };
    App.Env = Env;

    Services.cadastroLoja = function(opt) {        
        var formData = new FormData();
        Utils.forEachProperty(opt.req, function(val, key) {
            formData.append(key, val);
        });

        opt.ajax = {
            method: 'POST',
            body: formData,
            // headers: [
            //  { name: 'Content-Type', value: 'application/json; charset=UTF-8' }
            // ],
            url: 'http://onstore-seller-dash.us-east-1.elasticbeanstalk.com/masterdata'
            // url: '#'
        };
        
        return opt;
    };

    Services.recebiveis = function(opt) {
        var req = opt.req;
        var q = [];
        var reqStart = req && req.dateStart;
        var reqEnd = req && req.dateEnd;  
        var reqShopping = req && req.shopping;
        var reqSeller = req && req.seller;
        var reqStatus = req && req.status;
                      
        var formatDate = function(date) {
            return encodeURIComponent(date instanceof Date
                ? Utils.formatDateIso(date)
                : String(date)
            );
        }
        var reqOffset = req.offset;
        
        reqStart && q.push('start_date='+formatDate(reqStart));
        reqEnd && q.push('end_date='+formatDate(reqEnd));
        reqOffset && q.push('offset='+reqOffset);
        reqShopping && q.push('shopping='+reqShopping);
        reqSeller && q.push('seller='+reqSeller);
        reqStatus && q.push('status='+reqStatus);

        q = q.length ? '?'+q.join('&') : '';
        
        opt.ajax = {
            // method: 'GET',
            url: 'http://localhost:8080/json/receivables.json'+q
        };
        
        return opt;
    };

    Services.passReset = function(opt){
        // debugger;

        opt.ajax = {
            method: 'POST',
            headers: [
                { name: 'Content-Type', value: 'application/json; charset=UTF-8' }
            ],
            url: urlAPI + '/configurations/password/reset',
            body: opt.req
        };
        return opt;
    }
    
    Services.newPass = function(opt){
        // debugger;

        opt.ajax = {
            method: 'POST',
            headers: [
                { name: 'Content-Type', value: 'application/json; charset=UTF-8' }
            ],
            url: urlAPI + '/configurations/password/replace',
            body: opt.req
        };
        return opt;
    }

    Services.bancos = function(opt){

      opt.ajax = {
          method: 'GET',
          url: urlAPI + '/api/banks'
      };
      return opt;
  }

    Services.shoppings = function(opt){
        opt.ajax = {
            // method: 'GET',
            url: urlAPI + '/shopping-options'
        };
        return opt;
    }

    Services.lojas = function(opt){
        opt.ajax = {
            // method: 'GET',
            url: urlAPI + '/shoppings/' + opt.req + '/seller-options'
        };
        return opt;
    }

    Services.detalhesTransacao = function(opt) {
        opt.ajax = {
            // method: 'GET',
            url: urlAPI + 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/get_transaction/'+opt.req
        };
        return opt;
    };

    Services.pedidos = function(req, opt) {
        opt.ajax = {
            // method: 'GET',
            // url: '/order'
            url: 'http://localhost:8080/json/order.json' + '?shopping='+ req.shopping +'&seller='+ req.seller +'&limit=50&offset=' + req.offset + '&status='+ req.status +'&start_date=' + req.start_date + '&end_date=' + req.end_date
        };

        return opt;
    };

    Services.detalhesPedido = function(opt) {
        opt.ajax = {
            // method: 'GET',
            url: urlAPI + '/order/'+opt.req,
        };       
        
        
        return opt;
    };    

    Services.detalhesToken = function(opt) {
        opt.ajax = {
            // method: 'GET',
            url: urlAPI + '/order/by-token?token='+opt.req
        };
        return opt;
    };

    Services.reverseOrder = function(opt) {
        var formData = new FormData();
        Utils.forEachProperty(opt.req, function(val, key) {
            formData.append(key, val);
        });
        opt.ajax = {
            method: 'PUT',
            headers: [
             { name: 'Content-Type', value: 'application/json; charset=UTF-8' }
            ],
            // url: '#',
            url: urlAPI + '/refund_order',
            body: JSON.stringify(opt.req)
        };

        return opt;
    };

    Services.entregaProdutos = function(opt) {
        console.log(opt);
        opt.ajax = {
            method: 'PUT',
            headers: [
             { name: 'Content-Type', value: 'application/json; charset=UTF-8' }
            ],
            body: JSON.stringify(opt.req),
            
            // url: '#',
            url: urlAPI + '/checkout'
        };
        return opt;
    };

    var mapSession = {
        admin: 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/admin',
        not_admin: 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/not_admin',
        not_looged: 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/not_looged'
    };
    var defaultSession = window.location.pathname === urlAPI + '/cadastro' ? '' : urlAPI + '/loja/configurations';    

    Services.session = function(opt) {
        var url = App.sessionUrl;
        opt.ajax = {
            url: url && mapSession[url] || defaultSession
        };
        return opt;
    };

    Services.sawNotification = function(opt) {
        opt.ajax = {
            method: 'GET',
            url: urlAPI + '/api/saw-notifications'
        };
        return opt;
    };

})();