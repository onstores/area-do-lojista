(function() {
	var vars = this._var$;
	var App = this._app$;
	var Utils = vars.Utils;
	var Services = {};
	var Env = {
		name: 'local',
		Services: Services
	};
	App.Env = Env;

	Services.cadastroLoja = function(opt) {
		var formData = new FormData();
		Utils.forEachProperty(opt.req, function(val, key) {
			formData.append(key, val);
		});
		opt.ajax = {
			method: 'POST',
			body: formData,
			// headers: [
			// 	{ name: 'Content-Type', value: 'application/json; charset=UTF-8' }
			// ],
			url: 'http://onstore-seller-dash.us-east-1.elasticbeanstalk.com/masterdata'
		};
		return opt;
	};

	Services.recebiveis = function(opt) {
		var req = opt.req;
		var q = [];
		var reqStart = req && req.dateStart;
		var reqEnd = req && req.dateEnd;
		var formatDate = function(date) {
			return encodeURIComponent(date instanceof Date
				? Utils.formatDateIso(date)
				: String(date)
			);
		}
		reqStart && q.push('start_date='+formatDate(reqStart));
		reqEnd && q.push('end_date='+formatDate(reqEnd));
		q = q.length ? '?'+q.join('&') : '';
		opt.ajax = {
			// method: 'GET',
			url: 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/get_receivables'+q
		};
		return opt;
	};

	Services.detalhesTransacao = function(opt) {
		opt.ajax = {
			// method: 'GET',
			url: 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/get_transaction/'+opt.req
		};
		return opt;
	};

	Services.pedidos = function(opt) {
		opt.ajax = {
			// method: 'GET',
			url: 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/get_order'
		};
		return opt;
	};

	Services.detalhesPedido = function(opt) {
		opt.ajax = {
			// method: 'GET',
			url: 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/get_order/'+opt.req
		};
		return opt;
	};

	Services.detalhesToken = function(opt) {
		opt.ajax = {
			// method: 'GET',
			url: 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/get_order/token/'+opt.req
		};
		return opt;
	};

	Services.entregaProdutos = function(opt) {
		opt.ajax = {
			// method: 'PUT',
			// headers: [
			// 	{ name: 'Content-Type', value: 'application/json; charset=UTF-8' }
			// ],
			// body: JSON.stringify(data),
			url: 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/checkout'
		};
		return opt;
	};

	var mapSession = {
		admin: 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/admin',
		not_admin: 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/not_admin',
		not_looged: 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/not_looged'
	};
	var defaultSession = 'https://s3.amazonaws.com/static.ecommercecloud.com.br/onstore/session';

	Services.session = function(opt) {
		var url = App.sessionUrl;
		opt.ajax = {
			url: url && mapSession[url] || defaultSession
		};
		return opt;
	};

})();
