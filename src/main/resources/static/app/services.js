(function() {
	var App = this._app$;
	var vars = this._var$;
	var Utils = vars.Utils;

	var Env = App.Env;
	var Services = {};

	App.Services = Services;
	

	Services.cadastroLoja = function(data, callback) {
		var opt = Env.Services.cadastroLoja({
			req: data,
			// parse: Utils.loadService.parseJsonIgnoreType,
			callback: callback,
			reqValidate: function(data) {
				
				if (!data) {
					return {
						message: 'Dados do cadastro não informado'
					};
				}
			},
			dataValidate: function(data) {
				
				if (!data) {					
					// return {
					// 	message: 'Resposta vazia do servidor(2)'
					// };

					return 'Sucesso'
				} else if (data.message) {
					return data;
				}
			}
		});
		opt && Utils.loadService(opt);
	};

	Services.passReset = function(req, callback){
		var opt = Env.Services.passReset({
			req: req,
			callback: callback,
			reqValidate: function(data) {
				if (!data) {
					return {
						message: 'Dados do cadastro não informado'
					};
				}
			},
			dataValidate: function(data) {
				
				if (!data) {					
					// return {
					// 	message: 'Resposta vazia do servidor(2)'
					// };

					return 'Sucesso'
				} else if (data.message) {
					return data;
				}
			}
		});

		opt && Utils.loadService(opt);
	}
	
	Services.newPass = function(req, callback){
		var opt = Env.Services.newPass({
			req: req,
			callback: callback,
			reqValidate: function(data) {
				if (!data) {
					return {
						message: 'Dados do cadastro não informado'
					}
				}
			},
			dataValidate: function(data) {
				if (!data) {
					return 'Sucesso'
				} else if (data.message) {
					return data;
				}				
			}
		});

		opt && Utils.loadService(opt);
	}

	Services.shoppings = function(callback){
		var opt = Env.Services.shoppings({
			req: [],
			callback: callback,
			dataValidate: function(data) {
				if (!data) {
					return 'Resposta vazia'
				} else if (data.content) {
					data.error = "erro no dataValidate"
					return data;
				}				
			}
		});

		opt && Utils.loadService(opt);
	}

	Services.lojas = function(req, callback){
		var opt = Env.Services.lojas({
			req: req,
			callback: callback,
			dataValidate: function(data) {
				if (!data) {
					return 'Resposta vazia'
				} else if (data.content) {
					data.error = "erro no dataValidate"
					return data;
				}				
			}
		});

		opt && Utils.loadService(opt);
	}

	Services.recebiveis = function(req, callback) {
		var opt = Env.Services.recebiveis({
			req: req,
			parse: Utils.loadService.parseJsonIgnoreType,
			callback: callback,
			dataValidate: function(data) {
				if (!data) {
					return {
						message: 'Resposta vazia do servidor'
					};
				} else if (data.message) {
					return data;
				}
			}
		});
		opt && Utils.loadService(opt);
  };
  
  // API dos bancos
  Services.bancos = function(callback) {
		var opt = Env.Services.bancos({
			// req: req,
			// parse: Utils.loadService.parseJsonIgnoreType,
			callback: callback,
			dataValidate: function(data) {
				if (!data) {
					return {
						message: 'Resposta vazia do servidor'
					};
				} else if (data) {
					return data;
				}
			}
		});
		opt && Utils.loadService(opt);
	};
  // API dos bancos
	Services.detalhesTransacao = function(transacao, callback) {
		var opt = Env.Services.detalhesTransacao({
			req: transacao,
			parse: Utils.loadService.parseJsonIgnoreType,
			callback: callback,
			reqValidate: function(data) {
				if (!data) {
					return {
						message: 'Número da transação não informado'
					};
				} else if (!/^\d+$/.test(String(data))) {
					return {
						message: 'Número da transação inválido, esperado um número, obteve'+data
					};
				}
			},
			dataValidate: function(data) {
				if (!data) {
					return {
						message: 'Resposta vazia do servidor'
					};
				} else if (data.message) {
					return data;
				}
			}
		});
		opt && Utils.loadService(opt);
	};

	Services.pedidos = function(req, callback) {
		var opt = Env.Services.pedidos(req, {
			parse: Utils.loadService.parseJsonIgnoreType,
			callback: callback,
			dataValidate: function(data) {
				if (!data) {
					return {
						message: 'Resposta vazia do servidor'
					};
				} else if (data.message) {
					return data;
				}
			}
		} );
		opt && Utils.loadService(opt);
	};

	Services.reverseOrder = function(data, callback) {		
		var opt = Env.Services.reverseOrder({
			req: data,
			parse: Utils.loadService.parseJsonIgnoreType,
			callback: callback,
			dataValidate: function(data) {				
				if (!data) {
					return {
						message: 'Resposta vazia do servidor'
					};
				} else if (data.message) {
					return data;
				}
			}
		});		

		opt && Utils.loadService(opt);
	};

	Services.detalhesPedido = function(pedido, callback) {
		var opt = Env.Services.detalhesPedido({
			req: pedido,
			parse: Utils.loadService.parseJsonIgnoreType,
			callback: callback,
			reqValidate: function(data) {				
				if (!data) {
					return {
						message: 'Número do pedido não informado'
					};
				} 

				// else if (!/^\d+$/.test(String(data))) {
				//	return {
				//		message: 'Número do pedido inválido, esperado um número, obteve '+data
				//	};
				//}
			},
			dataValidate: function(data) {
				if (!data) {
					return {
						message: 'Resposta vazia do servidor'
					};
				} else if (data.message) {
					return data;
				}
			}
		});
		opt && Utils.loadService(opt);
	};

	Services.detalhesToken = function(token, callback) {
		var opt = Env.Services.detalhesToken({
			req: token,
			parse: Utils.loadService.parseJsonIgnoreType,
			callback: callback,
			reqValidate: function(data) {
				if (!data) {
					return {
						message: 'Token não informado'
					};
				} else if (!/^\w+$/.test(String(data))) {
					return {
						message: 'Token inválido, contém caracteres não permitidos: '+data
					};
				}
			},
			dataValidate: function(data) {
				if (!data) {
					return {
						message: 'Resposta vazia do servidor'
					};
				} else if (data.message) {
					return data;
				}
			}
		});
		opt && Utils.loadService(opt);
	};

	Services.entregaProdutos = function(data, callback) {
		var opt = Env.Services.entregaProdutos({
			req: data,
			parse: Utils.loadService.parseJsonIgnoreType,
			callback: callback,
			reqValidate: function(data) {
				if (!data) {
					return {
						message: 'Dados da entrega não informados'
					};
				} else if (!data.order_id) {
					return {
						message: 'Falta número do pedido que será entregue'
					};
				} else if (!data.items || !(data.items instanceof Array)) {
					return {
						message: 'Produtos a serem entregues não informados (ou não é uma lista)'
					};
				}
			},
			dataValidate: function(data) {
				if (!data) {
					return {
						message: 'Resposta vazia do servidor'
					};
				} else if (data.message) {
					return data;
				}
			}
		});
		opt && Utils.loadService(opt);
	};

	Services.session = function(callback) {
		var opt = Env.Services.session({
			parse: Utils.loadService.parseJsonIgnoreType,
			callback: callback,
			dataValidate: function(data) {
				if (!data) {
					return {
						message: 'Resposta vazia do servidor'
					};
				} else if (data.message) {
					return data;
				}
			}
		});
		opt && Utils.loadService(opt);
	};

	Services.sawNotification = function(callback) {
		var opt = Env.Services.sawNotification({
			callback: callback,
			dataValidate: function(data) {
				if (data) return data;
			}
		});
		opt && Utils.loadService(opt);
	}

})();
