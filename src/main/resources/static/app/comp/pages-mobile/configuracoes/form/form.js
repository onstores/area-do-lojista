(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["pages-mobile/configuracoes/form"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			return {
				checkboxAceito: false,
				enviandoCadastro: false,
				erroServicoCadastro: null,
				arquivoSelecionado: null,
				campoCNPJ: {
					nome: 'usuario',
					rotulo: 'CNPJ: *',
					valor: '',
					mask: Utils.mask.cnpj,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio,
						Utils.valida.cnpj
					]
				},
				campoRazaoSocial: {
					nome: 'razaosocial',
					rotulo: 'Razão Social: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoNomeFantasia: {
					nome: 'nomefantasia',
					rotulo: 'Nome Fantasia: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoSUC: {
					nome: 'suc',
					rotulo: 'SUC: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoAndar: {
					nome: 'Piso',
					rotulo: 'Piso: *',
					selecionado: null,
					opcoes: [
						{ valor: 0, texto: 'Térreo' },
						{ valor: 1, texto: 'Piso 1' },
						{ valor: 2, texto: 'Piso 2' },
						{ valor: 3, texto: 'Piso 3' },
						{ valor: 4, texto: 'Piso 4' },
						{ valor: 5, texto: 'Piso 5' },
						{ valor: 6, texto: 'Piso 1A' },
						{ valor: 7, texto: 'Piso 2A' },
						{ valor: 8, texto: 'Piso 3A' },
						{ valor: 9, texto: '1º Subsolo' },
						{ valor: 10, texto: '2º Subsolo' },
						{ valor: 11, texto: 'Estacionamento G1' },
						{ valor: 12, texto: 'Estacionamento G2' }
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.selecionado
					]
				},
				campoCategoria: {
					nome: 'categoria',
					rotulo: 'Categoria: *',
					selecionado: null,
					opcoes: [
						{ valor: 1, texto: '1' },
						{ valor: 2, texto: '2' },
						{ valor: 3, texto: '3' },
						{ valor: 4, texto: '4' },
						{ valor: 5, texto: '5' }
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.selecionado
					]
				},
				campoResponsavel: {
					nome: 'responsavel',
					rotulo: 'Nome do Responsável: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoTelefone: {
					nome: 'telefone',
					rotulo: 'Telefone do Responsável: *',
					valor: '',
					mask: Utils.mask.fone,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio,
						Utils.valida.fone
					]
				},
				campoEmail: {
					nome: 'email',
					rotulo: 'Email do Responsável: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio,
						Utils.valida.email
					]
				},
				campoDtRecebimento: {
					nome: 'DtRecebimento',
					rotulo: 'Data de recebimento: ',
					valor: 'Semanalmente',
					erro: null,
					falta: false,
					valido: false,
					disabled: true,
					valida: [
						Utils.valida.naoVazio,
					]
				},
				campoAntecipacao: {
					nome: 'antecipacao',
					rotulo: 'Antecipação: ',
					valor: 'Não',
					erro: null,
					falta: false,
					valido: false,
					disabled: true,
					valida: [
						Utils.valida.naoVazio,
					]
				},
				campoTaxa: {
					nome: 'taxa',
					rotulo: 'Taxa Onstore: ',
					valor: '4%',
					erro: null,
					falta: false,
					valido: false,
					disabled: true,
					valida: [
						Utils.valida.naoVazio,
					]
				},
				// Fim dos campos disable

				campoContaBanco: {
					nome: 'contaBanco',
					rotulo: 'Banco: *',
					selecionado: null,
					opcoes: [
						{ valor: '001', texto: '001 - Banco do Brasil' },
						{ valor: '237', texto: '237 - Bradesco' },
						{ valor: '104', texto: '104 - Caixa Econômica Federal' },
						{ valor: '341', texto: '341 - Itaú' },
						{ valor: '033', texto: '033 - Santander' }
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.selecionado
					]
				},
				campoContaAgencia: {
					nome: 'contaAgencia',
					rotulo: 'Agência: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoContaNumero: {
					nome: 'contaNumero',
					rotulo: 'Conta Corrente: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoDV: {
					nome: 'DV',
					rotulo: 'DV: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoAceito: {
					nome: 'aceitoTermos',
					rotulo: '',
					checked: true,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.checked
					]
				}
			};
		},
		methods: {
			clickCadastrar: function() {
				// this.$emit('cadastrar');
				var vm = this;
				this.enviandoCadastro = true;
				App.Services.cadastroLoja({
					"id": "csplojahavaianas",
					"cnpjfilial" : this.campoCNPJ.valor,
					"cnpjmatriz" : "teste",
					"razaosocial" : "teste",
					"nomefantasia" : "teste",
					"suc" : "teste",
					"andar" : 1,
					"categoria" : "teste",
					"nomeresponsavel" : "teste",
					"emailresponsavel" : "teste@teste.teste",
					"telefoneresponsavel" : "teste",
					"banco" : "teste",
					"agencia" : "0001",
					"contacorrente" : "123456"
					// "digitoverificadorconta" : ""
				}, function(loading, error, data) {
					vm.enviandoCadastro = loading;
					if (loading) return;
					console.log('cadastroLoja', error, data);
				});
			},
			clickTermos: function() {
				this.$emit('termos');
			},
			onCampoValor: function(payload) {
				this.$store.dispatch('campoValor', payload);
			},
			onCampoSelecionado: function(payload) {
				this.$store.dispatch('campoSelecionado', payload);
			},
			onCampoCheck: function(payload) {
				this.$store.dispatch('campoCheck', payload);
			},
			onCampoBlur: function(payload) {
				this.$store.dispatch('validarCampo', payload);
			},
			filterClickAceito: function(evt) {
				return !Utils.isChildOf(evt.target, this.$refs.btAbrirTermos);
			},
			changeComprovante: function(evt) {
				this.arquivoSelecionado = evt.target.files[0];
			}
		},
		created: function() {
			this.$on('termosAceitar', function() {
				this.onCampoCheck({
					campo: this.campoAceito,
					checked: true
				});
			});
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
