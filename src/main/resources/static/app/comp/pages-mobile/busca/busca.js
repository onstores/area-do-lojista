(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["pages-mobile/busca"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			return {
				pedidos: {
					loading: false,
					error: null,
					data: null
				}
			};
		},
		methods: {
			clickReload: function() {
				this.carregarPedidos();
			},
			carregarPedidos: function() {
				var vm = this;
				var ped = this.pedidos;
				ped.loading = true;
				App.Services.detalhesToken(this.$route.params.busca, function(loading, error, data) {
					if (loading) return;
					console.log('detalhesToken', error, data);
					ped.loading = false;
					if (!error && 'string' === typeof data) {
						try {
							data = JSON.parse(data);
						} catch(parseError) {
							error = parseError;
						}
					}
					ped.error = error;
					ped.data = data;
				});
			}
		},
		mounted: function() {
			this.carregarPedidos();
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
