(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["pages-mobile/minhas-vendas"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			var opcaoFiltrarPor = { valor: 0, texto: 'Filtrar por' };
			return {
				modalPeriodo: false,
				periodoInicio: null,
				periodoFim: null,
				heightClosed: 0,
				heightOpened: 172,
				heightUnit: 'px',
				pedidos: {
					loading: false,
					error: null,
					data: null
				},
				campoStatus: {
					nome: 'campoStatus',
					rotulo: '',
					selecionado: opcaoFiltrarPor,
					opcoes: [
						opcaoFiltrarPor,
						{ valor: 1, texto: 'Segmento 1' },
						{ valor: 2, texto: 'Segmento 2' },
						{ valor: 3, texto: 'Segmento 3' }
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				}
			};
		},
		computed: {
			isAdmin: function() {
				return this.$store.getters.isAdmin;
			},
			baseUrl: function() {
				return this.$store.state.baseUrl;
			},
			loading: function() {
				return this.pedidos.loading;
			},
			error: function() {
				return this.pedidos.error;
			},
			errorText: function() {
				var e = this.pedidos.error;
				var t = String(e && e.error || e);
				if (t === String({})) {
					t = JSON.stringify(e);
				}
				return t;
			},
			data: function() {
				return this.pedidos.data;
			},
			periodoText: function() {
				var inicio = this.periodoInicio;
				var fim = this.periodoFim;
				if (!inicio || !fim) {
					return 'Selecionar período';
				}
				if (+inicio == +fim) {
					return Utils.formatDateUser(inicio);
				} else {
					return [
						Utils.formatDateUser(inicio),
						Utils.formatDateUser(fim)
					].join(' a ');
				}
			}
		},
		methods: {
			setPeriodo: function(obj) {
				this.periodoInicio = obj.start;
				this.periodoFim = obj.end;
			},
			onCampoSelecionado: function(payload) {
				this.$store.dispatch('campoSelecionado', payload);
			},
			onCampoBlur: function(payload) {
				this.$store.dispatch('validarCampo', payload);
			},
			onClickExpand: function(ped) {
				var open = ped.expand;
				var from = open ? this.heightOpened : this.heightClosed;
				var to = open ? this.heightClosed : this.heightOpened;
				var unit = this.heightUnit;
				var ease = Utils.easing.easeQuad;
				var mod = Utils.easing.modOut;
				var rca = {};
				ped.currentAnimation = rca;
				ped.expand = !open;
				Utils.animate(from, to, 500, ease, mod, function(value, pos) {
					if (ped.currentAnimation !== rca) return true; // break
					ped.height = value.toFixed(0)+unit;
				});
			},
			clickReload: function() {
				this.carregarPedidos();
			},
			carregarPedidos: function() {
				var vm = this;
				var ped = this.pedidos;
				ped.loading = true;
				App.Services.pedidos(function(loading, error, data) {
					if (loading) return;
					// setTimeout(function() {
						ped.loading = false;
						if (!error && 'string' === typeof data) {
							try {
								data = JSON.parse(data);
							} catch(parseError) {
								error = parseError;
							}
						}
						if (data && data instanceof Array) {
							Utils.forEach(data, function(item) {
								item.expand = false;
								item.height = 0;
								item.currentAnimation = null;
							});
						} else {
							data = [];
						}
						// console.log('carregandoPedidos', error, data);
						ped.error = error;
						ped.data = data;
					// }, 5000);
				});
			},
			onClickPedido: function(ped) {
				this.$emit('detalhes', ped);
			}
		},
		mounted: function() {
			this.carregarPedidos();
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
