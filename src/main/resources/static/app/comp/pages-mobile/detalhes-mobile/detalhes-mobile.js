(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["pages-mobile/detalhes-mobile"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		props: {
			pedidoId: {
				type: String
			}
		},
		data: function() {
			return {
				pedido: {
					loading: false,
					error: null,
					data: null
				},
				validacaoSelecionada: 'entregar',
				estornoSelecionado: 'cartao',
				estornoOpcoes: [
					{ valor: 'cartao', texto: 'Estorno Cartão' },
					{ valor: 'boleto', texto: 'Estorno Boleto' }
				],
				campoEntregarPedido: {
					nome: 'campoEntregarPedido',
					rotulo: 'Finalizar Pedido',
					checked: true,
					valor: 'entregar',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.checked
					]
				},
				campoEstornarPedido: {
					nome: 'campoEstornarPedido',
					rotulo: 'Estornar Pedido',
					checked: false,
					valor: 'estornar',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.checked
					]
				},
				campoStatus: {
					nome: 'campoObs',
					rotulo: 'Observação: ',
					selecionado: null,
					opcoes: [
						{ valor: 1, texto: 'Todos' },
						{ valor: 2, texto: 'Recebido' },
						{ valor: 3, texto: 'Cancelado' }
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				},
				campoToken: {
					nome: 'campoToken',
					rotulo: 'Token do pedido',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				}
			};
		},
		computed: {
			validacaoOpcoes: function() {
				return [
					this.campoEntregarPedido,
					this.campoEstornarPedido
				];
			},
			loading: function() {
				return this.pedido.loading;
			},
			error: function() {
				return this.pedido.error;
			},
			errorText: function() {
				var e = this.pedido.error;
				var t = String(e && e.error || e);
				if (t === String({})) {
					t = JSON.stringify(e);
				}
				return t;
			},
			data: function() {
				return this.pedido.data;
			},
			client: function() {
				var data = this.pedido.data;
				return data && data.client || {};
			},
			payment: function() {
				var data = this.pedido.data;
				return data && data.payment || {};
			}
		},
		methods: {
			onCampoCheck: function(payload) {
				var vm = this;
				vm.validacaoSelecionada = payload.campo.valor;
				Utils.forEach(vm.validacaoOpcoes, function(opcao) {
					var checked = opcao === payload.campo;
					var sendCheck = {
						campo: opcao,
						checked: checked
					};
					vm.$store.dispatch('campoCheck', sendCheck);
				});
			},
			carregarPedido: function(pedidoId) {
				var vm = this;
				var ped = this.pedido;
				ped.loading = true;
				App.Services.detalhesPedido(pedidoId, function(loading, error, data) {
					if (loading) return;
					// setTimeout(function() {
						ped.loading = false;
						if (!error && 'string' === typeof data) {
							try {
								data = JSON.parse(data);
							} catch(parseError) {
								error = parseError;
							}
						}
						// console.log('carregandoPedido', error, data);
						ped.error = error;
						ped.data = data;
					// }, 5000);
				});
			},
			onCampoValor: function(payload) {
				this.$store.dispatch('campoValor', payload);
			},
			onCampoSelecionado: function(payload) {
				this.$store.dispatch('campoSelecionado', payload);
			},
			onCampoBlur: function(payload) {
				this.$store.dispatch('validarCampo', payload);
			},
			onClickEstorno: function(opcao) {
				this.estornoSelecionado = opcao.valor;
			},
			onClickEstornoBoleto: function() {
				this.$emit('estorno-boleto');
			},
			clickReload: function() {
				this.carregarPedido(this.pedidoId);
			}
		},
		mounted: function() {
			this.$watch('pedidoId', function(id) {
				if (id) {
					this.carregarPedido(id);
				} else {
					this.pedido.loading = false;
					this.pedido.error = null;
					this.pedido.data = null;
				}
			}, {
				immediate: true
			});
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
