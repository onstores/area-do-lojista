(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["pages-mobile/cadastro/form"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			return {
				checkboxAceito: false,
				enviandoCadastro: false,
				erroMensagem: null,
				uploadFile: null,
				campoCNPJ: {
					nome: 'usuario',
					rotulo: 'CNPJ: *',
					valor: '',
					mask: Utils.mask.cnpj,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio,
						Utils.valida.cnpj
					]
				},
				campoRazaoSocial: {
					nome: 'razaosocial',
					rotulo: 'Razão Social: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoNomeFantasia: {
					nome: 'nomefantasia',
					rotulo: 'Nome Fantasia: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoSUC: {
					nome: 'suc',
					rotulo: 'SUC: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoAndar: {
					nome: 'Piso',
					rotulo: 'Piso: *',
					selecionado: null,
					opcoes: [
						{ valor: 0, texto: 'Térreo' },
						{ valor: 1, texto: 'Piso 1' },
						{ valor: 2, texto: 'Piso 2' },
						{ valor: 3, texto: 'Piso 3' },
						{ valor: 4, texto: 'Piso 4' },
						{ valor: 5, texto: 'Piso 5' },
						{ valor: 6, texto: 'Piso 1A' },
						{ valor: 7, texto: 'Piso 2A' },
						{ valor: 8, texto: 'Piso 3A' },
						{ valor: 9, texto: '1º Subsolo' },
						{ valor: 10, texto: '2º Subsolo' },
						{ valor: 11, texto: 'Estacionamento G1' },
						{ valor: 12, texto: 'Estacionamento G2' }
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.selecionado
					]
				},
				campoCategoria: {
					nome: 'categoria',
					rotulo: 'Categoria: *',
					selecionado: null,
					opcoes: [
						{ valor: 'tecnologia', texto: 'Tecnologia' },
						{ valor: 'outros', texto: 'Outros' }
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.selecionado
					]
				},
				campoResponsavel: {
					nome: 'responsavel',
					rotulo: 'Nome do Responsável: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoTelefone: {
					nome: 'telefone',
					rotulo: 'Telefone do Responsável: *',
					valor: '',
					mask: Utils.mask.fone,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio,
						Utils.valida.fone
					]
				},
				campoEmail: {
					nome: 'email',
					rotulo: 'Email do Responsável: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio,
						Utils.valida.email
					]
				},
				campoDtRecebimento: {
					nome: 'DtRecebimento',
					rotulo: 'Data de recebimento: ',
					valor: 'Semanalmente',
					erro: null,
					falta: false,
					valido: false,
					disabled: true,
					valida: [
						Utils.valida.naoVazio,
					]
				},
				campoAntecipacao: {
					nome: 'antecipacao',
					rotulo: 'Antecipação: ',
					valor: 'Não',
					erro: null,
					falta: false,
					valido: false,
					disabled: true,
					valida: [
						Utils.valida.naoVazio,
					]
				},
				campoTaxa: {
					nome: 'taxa',
					rotulo: 'Taxa Onstore: ',
					valor: '4%',
					erro: null,
					falta: false,
					valido: false,
					disabled: true,
					valida: [
						Utils.valida.naoVazio,
					]
				},
				// Fim dos campos disable
				campoContaBanco: {
					nome: 'contaBanco',
					rotulo: 'Banco: *',
					selecionado: null,
					opcoes: [
						{ valor: '001', texto: '001 - Banco do Brasil' },
						{ valor: '237', texto: '237 - Bradesco' },
						{ valor: '104', texto: '104 - Caixa Econômica Federal' },
						{ valor: '341', texto: '341 - Itaú' },
						{ valor: '033', texto: '033 - Santander' }
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.selecionado
					]
				},
				campoContaAgencia: {
					nome: 'contaAgencia',
					rotulo: 'Agência: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoContaNumero: {
					nome: 'contaNumero',
					rotulo: 'Conta Corrente: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoDV: {
					nome: 'DV',
					rotulo: 'DV: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoAceito: {
					nome: 'aceitoTermos',
					rotulo: '',
					checked: false,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.checked
					]
				},
				campoUpload: {
					nome: 'file',
					rotulo: 'Comprovante Bancário: *',
					file: null,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.hasFile,
						Utils.valida.isFile,
						Utils.valida.fnFileTypeRegex(
							/^application\/pdf$|^image\/(?:jpe?g|png)$/i,
							'Formato inválido, por favor envie um JPG, PNG ou PDF'
						),
						Utils.valida.fnFileSizeUnder(
							1024 * 1024,
							'Arquivo muito grande, por favor envie um arquivo menor que 1 MB'
						)
					]
				}
			};
		},
		computed: {
			baseUrl: function() {
				return this.$store.state.baseUrl;
			},
			isLoja: function() {
				return this.$store.getters.isLoja;
			}
		},
		methods: {
			enviarDados: function() {
				var vm = this;
				this.enviandoCadastro = true;
				App.Services.cadastroLoja({
					"id": "csplojahavaianas",
					"cnpjfilial" : this.campoCNPJ.valor,
					"cnpjmatriz" : this.campoCNPJ.valor,
					"razaosocial" : this.campoRazaoSocial.valor,
					"nomefantasia" : this.campoNomeFantasia.valor,
					"suc" : this.campoSUC.valor,
					"andar" : (this.campoAndar.selecionado || {}).valor,
					"categoria" : (this.campoCategoria.selecionado || {}).valor,
					"nomeresponsavel" : this.campoResponsavel.valor,
					"emailresponsavel" : this.campoEmail.valor,
					"telefoneresponsavel" : this.campoTelefone.valor,
					"banco" : (this.campoContaBanco.selecionado || {}).valor,
					"agencia" : this.campoContaAgencia.valor,
					"contacorrente" : this.campoContaNumero.valor,
					"digitoverificadorconta" : this.campoDV.valor,
					"file": this.campoUpload.file,
				}, function(loading, error, data) {
					vm.enviandoCadastro = loading;
					if (loading) return;
					console.log('cadastroLoja', error, data);
					if (!error && data && !data.message) {
						vm.$emit('cadastrar');
					} else {
						vm.erroMensagem = data && data.message
							? data.message
							: (error && error.error || error) || 'Resposta vazia do servidor';
						// tratar o erro
					}
				});
			},
			clickCadastrar: function() {
				// this.$emit('cadastrar');
				var vm = this;
				this.erroMensagem = null;

				this.$store.dispatch('validarForm', {
					cnpj: this.campoCNPJ,
					razaoSocial: this.campoRazaoSocial,
					nomeFantasia: this.campoNomeFantasia,
					suc: this.campoSUC,
					andar: this.campoAndar,
					categoria: this.campoCategoria,
					responsavel: this.campoResponsavel,
					telefone: this.campoTelefone,
					email: this.campoEmail,
					contaBanco: this.campoContaBanco,
					contaAgencia: this.campoContaAgencia,
					contaNumero: this.campoContaNumero,
					digitoverificadorconta: this.campoDV,
					aceito: this.campoAceito,
					upload: this.campoUpload
				}).then(function(result) {
					vm.erroMensagem = result.erroMensagem;
					if (!result.erroMensagem) {
						vm.enviarDados();
					}
				});
			},
			clickTermos: function() {
				this.$emit('termos');
			},
			onCampoValor: function(payload) {
				this.$store.dispatch('campoValor', payload);
			},
			onCampoSelecionado: function(payload) {
				this.$store.dispatch('campoSelecionado', payload);
			},
			onCampoCheck: function(payload) {
				this.$store.dispatch('campoCheck', payload);
			},
			onCampoBlur: function(payload) {
				this.$store.dispatch('validarCampo', payload);
			},
			onChangeFileUpload: function() {
				this.campoUpload.file = this.$refs.fileUpload.files[0] || null;
				this.$store.dispatch('validarCampo', { campo: this.campoUpload });
			},
			filterClickAceito: function(evt) {
				return !Utils.isChildOf(evt.target, this.$refs.btAbrirTermos);
			},
			findSelectOpcaoValor: function(obj) {
				var valor = obj.selecionado;
				obj.selecionado = null;
				Utils.forEach(obj.campo.opcoes, function(opcao) {
					if (valor == opcao.valor) {
						obj.selecionado = opcao;
						return this._break;
					}
				});
				return obj;
			},
			fillInputsWithSessionData: function() {
				var sData = this.$store.state.session.data;
				var cValor = 'campoValor';
				var cSelecionado = 'campoSelecionado';
				var plist = [
					this.$store.dispatch(cValor, {
						campo: this.campoCNPJ,
						valor: sData.cnpjfilial || ''
					}),
					this.$store.dispatch(cValor, {
						campo: this.campoRazaoSocial,
						valor: sData.razaosocial || ''
					}),
					this.$store.dispatch(cValor, {
						campo: this.campoNomeFantasia,
						valor: sData.nomefantasia || ''
					}),
					this.$store.dispatch(cValor, {
						campo: this.campoSUC,
						valor: sData.suc || ''
					}),
					this.$store.dispatch(cSelecionado, this.findSelectOpcaoValor({
						campo: this.campoAndar,
						selecionado: sData.andar || ''
					})),
					this.$store.dispatch(cSelecionado, this.findSelectOpcaoValor({
						campo: this.campoCategoria,
						selecionado: sData.categoria || ''
					})),
					this.$store.dispatch(cValor, {
						campo: this.campoResponsavel,
						valor: sData.nomeresponsavel || ''
					}),
					this.$store.dispatch(cValor, {
						campo: this.campoTelefone,
						valor: sData.telefoneresponsavel || ''
					}),
					this.$store.dispatch(cValor, {
						campo: this.campoEmail,
						valor: sData.emailresponsavel || ''
					}),
					this.$store.dispatch(cValor, {
						campo: this.campoDtRecebimento,
						valor: sData.datadorecebimento || ''
					}),
					this.$store.dispatch(cValor, {
						campo: this.campoAntecipacao,
						valor: sData.antecipacao === true ? 'Sim' :
							sData.antecipacao === false ? 'Não' :
							String(sData.antecipacao)
					}),
					this.$store.dispatch(cValor, {
						campo: this.campoTaxa,
						valor: Number(+sData.taxaonstore).toFixed(4)
							.replace(/\.?0*$/,'')
							.replace('.',',')+' %'
					}),
					this.$store.dispatch(cSelecionado, this.findSelectOpcaoValor({
						campo: this.campoContaBanco,
						selecionado: sData.codigodobanco
					})),
					this.$store.dispatch(cValor, {
						campo: this.campoContaAgencia,
						valor: sData.agencia || ''
					}),
					this.$store.dispatch(cValor, {
						campo: this.campoContaNumero,
						valor: sData.contacorrente || ''
					}),
					this.$store.dispatch(cValor, {
						campo: this.campoDV,
						valor: sData.digitoverificadorconta || ''
					})
				];
				Promise.all(plist).then(function(){});
			},
			onChangeSession: function() {
				if (this.isLoja) {
					if (this.unwatchSession) {
						this.unwatchSession();
						this.unwatchSession = void 0;
					}
					this.fillInputsWithSessionData();
				}
			}
		},
		created: function() {
			this.$on('termosAceitar', function() {
				this.onCampoCheck({
					campo: this.campoAceito,
					checked: true
				});
			});
			if (this.$store.state.session.loading) {
				this.unwatchSession = this.$watch(function() {
					return this.$store.state.session;
				}, this.onChangeSession);
			} else {
				this.onChangeSession();
			}
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
