(function () {
  'use strict';
  var vars = window._var$;
  var App = window._app$;
  var Utils = vars.Utils;
  App.compMap["desktop/header"] = function (callback, template, match) {
    comp.template = template;
    callback(null, comp);
  };
  var comp = {
    template: null,
    data: function () {
      var rp = this.$route.params;
      return {
        notify: {
          loading: false,
          error: null,
          data: null,
        },
        mouseOnOver: false,
        buscaValue: rp && rp.busca || ''
      };
    },
    computed: {
      baseUrl: function () {
        return this.$store.state.baseUrl;
      },
      routerBaseUrl: function () {
        return App.routerBaseUrl || '';
      },
      isLoja: function () {
        return this.$store.getters.isLoja;
      },
      isLogged: function () {
        return this.$store.getters.isLogged;
      },
      userCompanyName: function () {
        return this.$store.getters.userCompanyName;
      },
      sawNotificationsAt: function () {
        return this.$store.getters.sawNotificationsAt;
      },
      isAdminOnstore: function () {
        return this.$store.getters.isAdminOnstore;
      }
    },
    methods: {
      mouseRelatorio: function () {
        this.mouseOnOver = !this.mouseOnOver;
      },
      goToOrder: function () {
        window.location.href = 'loja/detalhes-pedido/' + this.order.order_id;
      },
      sawNotif: function () {
        var vm = this;
        // this.$store.dispatch('sawNotification');
        App.Services.sawNotification(function (loading, error, data) {
          if (loading) return;

          if (!error && !data) {
            vm.notify.data = [];
          }
        });
      },
      linkScope: function (link) {
        var getters = this.$store.getters;
        // var base = getters.isAdmin ? '/admin/' :
        // 	getters.isLoja ? '/loja/' :
        // 	null;
        var base = '/loja/'
        return base ? base + link : '/';
      },
      onSubmitBusca: function () {
        var userPath = this.$store.getters.isAdmin
          ? '/admin'
          : '/loja';
        this.$router.push({
          path: userPath + '/busca/' + encodeURIComponent(this.buscaValue)
        });
        // App.Services.detalhesToken(this.buscaValue, function(loading, err, data) {
        // 	if (loading) return;
        // 	console.log('detalhesToken', err, data);
        // });
      },
      getLatestOrders: function () {
        var vm = this;
        var ped = vm.notify;
        var dateNow = new Date();

        var sawNotifAt = vm.sawNotificationsAt ?
          new Date(vm.sawNotificationsAt + " UTC") :
          new Date('2018-01-01 00:00');

        var dataInicial = sawNotifAt.getFullYear() + '-' + (sawNotifAt.getMonth() + 1) + '-' + sawNotifAt.getDate();
        var dataFinal = dateNow.getFullYear() + '-' + (dateNow.getMonth() + 1) + '-' + dateNow.getDate();
        var req = {
          //status
          status: "",

          //shopping
          shopping: "",

          //seller
          seller: "",

          //offset param
          offset: 0,

          //start_date range param
          start_date: dataInicial,

          //end_date range param
          end_date: dataFinal
        }

        App.Services.pedidos(req, function (loading, error, data) {
          if (loading) return;
          ped.loading = false;

          var notify = [];
          if (data && data.results instanceof Array) {
            Utils.forEach(data.results, function (item) {
              item.expand = false;
              item.height = 0;
              item.currentAnimation = null;

              var data = new Date(item.date_created);
              item.data = data.getDate() + '/' + data.getMonth() + '/' + data.getFullYear();
              item.data = item.data + ' - ' + data.getHours() + ':' + data.getMinutes();

              data > sawNotifAt ? notify.push(item) : "";
            });
          }
          notify.count = notify.length

          ped.error = error;
          ped.data = notify;
        });

      },
      onClickLogout: function () {
        // this.$router.push('/logout');
        window.location.href = '/logout';

      }
    },
    mounted: function () {
      this.getLatestOrders();
    }
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
  };
})();
