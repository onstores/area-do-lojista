(function () {
  'use strict';
  var vars = window._var$;
  var App = window._app$;
  var Utils = vars.Utils;
  var rePedidoDate = /(\d{1,2})\/(\d{1,2})\/(\d{1,4})(?:[\st-]+(\d{1,2}):(\d{1,2}))/i;
  App.compMap["desktop/relacao-de-pedidos"] = function (callback, template, match) {
    comp.template = template;
    callback(null, comp);
  };
  var comp = {
    template: null,
    props: {
      listTitle: {
        type: String
      },
      pedidos: {
        type: Object,
        required: true
      },
      resultados: {
        type: Number
      }
    },
    data: function () {
      var opcaoStatusTodos = { valor: null, texto: 'Todos' };
      return {
        monthNames: Utils.getMonthNames(),
        weekDaysHeader: Utils.getWeekDaysHeader(),
        itemState: [],
        // pedidos: {
        // 	loading: false,
        // 	error: null,
        // 	data: null
        // },
        campoStatus: {
          nome: 'campoStatus',
          rotulo: 'Filtrar por: ',
          selecionado: opcaoStatusTodos,
          opcoes: [
            { texto: 'Todos', valor: '' },
            { texto: 'Aguardando cancelamento', valor: 'waiting-for-seller-decision' },
            { texto: 'Aguardando pagamento', valor: 'payment-pending,waiting-for-order-authorization,waiting-ffmt-authorization' },
            { texto: 'Aguardando retirada', valor: 'approve-payment,authorize-fulfillment,handling,invoice,order-accepted,payment-approved,ready-for-handling,start-handling,window-to-cancel' },
            { texto: 'Cancelamento solicitado', valor: 'cancellation-requested,request-cancel' },
            { texto: 'Entregue', valor: 'invoiced' },
            { texto: 'Erro na criação do pedido', valor: 'order-create-error,order-creation-error' },
            { texto: 'Erro no estorno', valor: 'invoice-after-cancellation-deny' },
            { texto: 'Estornado', valor: 'cancel,canceled,replaced' },
            { texto: 'Pagamento negado', valor: 'payment-denied' },
            { texto: 'Processando', valor: 'on-order-completed,order-completed,order-created,waiting-for-seller-confirmation' }
          ],
          erro: null,
          falta: false,
          valido: false,
        },
        campoBuscaMaster: {
          nome: 'campoBuscaMaster',
          rotulo: 'Filtrar por:',
          placeholder: 'shopping ou loja',
        },

        campoDataInicial: {
          nome: 'campoDataInicial',
          tipo: 'date',
          rotulo: '',
          placeholder: 'dd/mm/aaaa',
          selecionado: null,
          dataMes: new Date(),
          dataInicial: null,
          dataFinal: null,
          erro: null,
          falta: false,
          valido: false,
          valida: [
            // Utils.valida.selecionado
          ]
        },
        campoDataFinal: {
          nome: 'campoDataFinal',
          tipo: 'date',
          rotulo: ' à ',
          placeholder: 'dd/mm/aaaa',
          selecionado: null,
          dataMes: new Date(),
          dataInicial: null,
          dataFinal: null,
          erro: null,
          falta: false,
          valido: false,
          valida: [
            // Utils.valida.selecionado
          ]
        }
      };
    },
    computed: {
      loading: function () {
        return this.pedidos.loading;
      },
      error: function () {
        return this.pedidos.error;
      },
      errorText: function () {
        var e = this.pedidos.error;
        var t = String(e && e.error || e);
        if (t === String({})) {
          t = JSON.stringify(e);
        }
        return t;
      },
      data: function () {
        return this.pedidos.data;
      },
      dataState: function () {
        var vm = this;
        var data = this.pedidos.data;
        var together = [];
        var cdi = this.campoDataInicial;
        var pInicio = cdi.dataInicial;
        var pFim = cdi.dataFinal;
        pFim = pFim && new Date(pFim.getFullYear(), pFim.getMonth(), pFim.getDate() + 1);
        var status = this.campoStatus.selecionado;
        status = status && status.valor;
        var temFiltro = status || pInicio || pFim;
        var totalFiltro = 0;
        if (data && data instanceof Array) {
          Utils.forEach(data, function (pedido, index) {
            var dc = vm.parsePedidoDate(pedido.date_created);
            var mf = (dc
              ? (pInicio ? pInicio <= dc : true)
              && (pFim ? dc < pFim : true)
              : true)
              && (status ? pedido.status == status : true);
            totalFiltro += mf ? 1 : 0;
            var state = vm.itemState[index] || {
              dateCreated: dc,
              matchFilter: true
            };
            state.matchFilter = mf;
            vm.itemState[index] = state;
            together.push({
              pedido: pedido,
              state: state
            });
          });
        }
        vm.itemState = vm.itemState.slice(0, data && data.length || 0);
        return {
          together: together,
          totalFiltro: totalFiltro,
          temFiltro: temFiltro
        };
      },
      isAdmin: function () {
        return this.$store.getters.isAdmin;
      },

      isAdminOnstore: function () {
        return this.$store.getters.isAdminOnstore;
      },

      isLoja: function () {
        return this.$store.getters.isLoja;
      },

      isShopping: function () {
        return this.$store.getters.isShopping;
      },

      exportURL: function () {
        let status = this.campoStatus.selecionado.valor;
        var q = [];
        var reqFormat = 'xlsx';
        var reqStatus = this.campoStatus.selecionado.valor;
        var reqStart = this.campoDataInicial.dataInicial;
        var reqEnd = this.campoDataFinal.dataFinal;                
        var formatDate = function(date) {
            return encodeURIComponent(date instanceof Date
                ? Utils.formatDateIso(date)
                : String(date)
            );
        }
        reqFormat && q.push('output='+reqFormat);
        reqStart && q.push('start_date='+formatDate(reqStart));
        reqEnd && q.push('end_date='+formatDate(reqEnd));
        reqStatus && q.push('status='+reqStatus);
        q = q.length ? '?'+q.join('&') : '';
        return (url) => url+q;
      }
    },
    methods: {
      parsePedidoDate: function (str) {
        var m = String(str).match(rePedidoDate);
        return m ? new Date(+m[3], +m[2] - 1, +m[1], +m[4] || 0, +m[5] || 0) : void 0;
      },
      formatStatus: function (str, pedido) {
        var statusMap = {
          'Aguardando cancelamento': ['waiting-for-seller-decision'],
          'Aguardando pagamento': ['payment-pending', 'waiting-for-order-authorization', 'waiting-ffmt-authorization'],
          'Aguardando retirada': ['approve-payment', 'authorize-fulfillment', 'handling', 'invoice', 'order-accepted', 'payment-approved', 'ready-for-handling', 'start-handling', 'window-to-cancel'],
          'Cancelamento solicitado': ['cancellation-requested', 'request-cancel'],
          'Erro na criação do pedido': ['order-create-error', 'order-creation-error'],
          'Erro no estorno': ['invoice-after-cancellation-deny'],
          'Estornado': ['cancel', 'canceled', 'replaced'],
          'Pagamento negado': ['payment-denied'],
          'Processando': ['on-order-completed', 'order-completed', 'order-created', 'waiting-for-seller-confirmation'],
          'Entregue': ['invoiced']
        }

        if (pedido.status_delivered && pedido.status_delivered == "true" && str == "handling"){
          return "Entrega Parcial"
        }

        for (var property in statusMap) {
          if (statusMap.hasOwnProperty(property)) {
            var statusIndex = statusMap[property].indexOf(str);
            // has matched
            if (statusIndex !== -1) {
              return property;
            }
          }
        }        

        // In case none matches
        return str

      },
      onCampoSelecionado: function (payload) {
        // this.$store.dispatch('campoSelecionado', payload);
        this.$emit('statusSelecionado', payload);
        this.campoStatus.selecionado = payload.selecionado;
      },
      onCampoClickDay: function (payload) {
        var campo = payload.campo;
        var day = payload.day;
        var start, end;
        var cdInicial = this.campoDataInicial;
        var cdFinal = this.campoDataFinal;

        this.$store.commit('aplicarCalendarMonth', {
          campo: cdInicial,
          dataMes: campo.dataMes
        });
        this.$store.commit('aplicarCalendarMonth', {
          campo: cdFinal,
          dataMes: campo.dataMes
        });

        if (campo === cdInicial) {
          start = day.startStart;
          end = day.startEnd;
        } else if (campo === cdFinal) {
          start = day.endStart;
          end = day.endEnd;
        } else {
          return;
        }

        var camposPromise = [
          this.$store.dispatch('campoCalendarPeriod', {
            campo: cdInicial,
            start: start,
            end: end
          }),
          this.$store.dispatch('campoCalendarPeriod', {
            campo: cdFinal,
            start: start,
            end: end
          })
        ];

        Promise.all(camposPromise).then(function () {
          cdInicial.selecionado = start
            ? Utils.formatDateUser(start)
            : null;
          cdFinal.selecionado = end
            ? Utils.formatDateUser(end)
            : null;
        });

        this.$emit('setDateFilter', cdInicial);

      },
      onCampoChangeMonth: function (payload) {
        this.$store.dispatch('campoCalendarMonth', payload);
      },
      onCampoBlur: function (payload) {
        this.$store.dispatch('validarCampo', payload);
      },
      clickReload: function () {
        this.$emit('pedidosReload');
      },
      makeLinkPedido: function (ped) {
        var userPath = this.$store.getters.isAdmin
          ? '/admin'
          : '/loja';
        return userPath + '/detalhes-pedido/' + ped.order_id;
      },
      onClickPedido: function (ped) {
        this.$emit('detalhes', ped);
      },

      formatDate: function (currDate) {
        var currDateFormated = new Date(currDate);
        var year = currDateFormated.getFullYear();
        var month = currDateFormated.getMonth() + 1;
        var dt = currDateFormated.getDate();

        var formatedDt = dt < 10 ? '0' + dt : dt;
        var formatedMonth = month < 10 ? '0' + month : month



        return (formatedDt + '-' + formatedMonth + '-' + year)

      },
    }
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
  };
})();
