(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["desktop/recebimentos"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		props: {
			shopping: {
				type: String
			},
			seller:{
				type: String
			},
		},
		data: function() {
			var opcaoStatusTodos = { valor: null, texto: 'Todos' };
			return {
				downloadRecebiveis: null,
				monthNames: Utils.getMonthNames(),
				weekDaysHeader: Utils.getWeekDaysHeader(),
				itemState: [],
				timelineMonths: this.getTimelineMonths(),
				recebiveis: {
					loading: false,
					error: null,
					data: null
				},
				status: null,
				pagination: {
					active: false,
					actual: 1,
					results: 0,
					pages: 0
				},
				campoStatus: {
					nome: 'campoStatus',
					rotulo: 'Filtrar por: ',
					selecionado: opcaoStatusTodos,
					opcoes: [
						{ texto: 'Todos', valor: ''},
						{ texto: 'A Receber'	, valor: 'waiting_funds'},
						{ texto: 'Pago'					, valor: 'paid'},
						{ texto: 'Transferência'     		, valor: 'prepaid'},
						{ texto: 'Estorno'      		, valor: 'refund'}
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				},
				queryFilterXLSX: null,
				campoDataInicial: {
					nome: 'campoDataInicial',
					tipo: 'date',
					rotulo: '',
					placeholder: 'dd/mm/aaaa',
					selecionado: null,
					dataMes: new Date(),
					dataInicial: null,
					dataFinal: null,
					erro: null,
					falta: false,
					valido: false,
					valida: [
					  // Utils.valida.selecionado
					]
				  },
				  campoDataFinal: {
					nome: 'campoDataFinal',
					tipo: 'date',
					rotulo: ' à ',
					placeholder: 'dd/mm/aaaa',
					selecionado: null,
					dataMes: new Date(),
					dataInicial: null,
					dataFinal: null,
					erro: null,
					falta: false,
					valido: false,
					valida: [
					  // Utils.valida.selecionado
					]
				  }
			};
		},
		computed: {
			baseUrl: function() {
				return this.$store.state.baseUrl;
			},
			userCompanyName: function() {
				return this.$store.getters.userCompanyName;
			},
			calMonthNames: function() {
				return Utils.getMonthNames();
			},
			monthFirstDay: function() {
				var am = this.activeMonth;
				return new Date(am.getFullYear(), am.getMonth(), 1);
			},
			monthLastDay: function() {
				var am = this.activeMonth;
				return new Date(am.getFullYear(), am.getMonth()+1, 0);
			},
			formatedMonthFirstDay: function() {
				var data = this.monthFirstDay;
				data = data.getFullYear() +'-'+ (data.getMonth() + 1) +'-'+ data.getDate();

				return data;
			},
			formatedMonthLastDay: function() {
				var data = this.monthLastDay;
				data = data.getFullYear() +'-'+ (data.getMonth() + 1) +'-'+ data.getDate();

				return data;
			},
			activeMonth: function() {
				var tm = this.timelineMonths;
				var active = Math.max(0, Math.min(tm.list.length, tm.active));
				return tm.list[active];
			},
			loading: function() {
				return this.recebiveis.loading;
			},
			error: function() {
				return this.recebiveis.error;
			},
			errorText: function() {
				var e = this.recebiveis.error;
				var t = String(e && e.error || e);
				if (t === String({})) {
					t = JSON.stringify(e);
				}
				return t;
			},
			data: function() {
				return this.recebiveis.data;
			},
			dataState: function () {
				var vm = this;
				var data = this.recebiveis.data;
				var together = [];
				var cdi = this.campoDataInicial;
				var pInicio = cdi.dataInicial;
				var pFim = cdi.dataFinal;
				pFim = pFim && new Date(pFim.getFullYear(), pFim.getMonth(), pFim.getDate() + 1);
				var status = this.campoStatus.selecionado;
				status = status && status.valor;
				var temFiltro = status || pInicio || pFim;
				var totalFiltro = 0;
				if (data && data instanceof Array) {
				  Utils.forEach(data, function (pedido, index) {
					var dc = vm.parsePedidoDate(pedido.date_created);
					var mf = (dc
					  ? (pInicio ? pInicio <= dc : true)
					  && (pFim ? dc < pFim : true)
					  : true)
					  && (status ? pedido.status == status : true);
					totalFiltro += mf ? 1 : 0;
					var state = vm.itemState[index] || {
					  dateCreated: dc,
					  matchFilter: true
					};
					state.matchFilter = mf;
					vm.itemState[index] = state;
					together.push({
					  pedido: pedido,
					  state: state
					});
				  });
				}
				vm.itemState = vm.itemState.slice(0, data && data.length || 0);
				return {
				  together: together,
				  totalFiltro: totalFiltro,
				  temFiltro: temFiltro
				};
			},
			exportURL: function () {
				let status = this.campoStatus.selecionado.valor;
				var q = [];
				var reqFormat = 'xlsx';
				var reqStatus = this.campoStatus.selecionado.valor;
				var reqStart = this.campoDataInicial.dataInicial;
				var reqEnd = this.campoDataFinal.dataFinal;                
				var formatDate = function(date) {
					return encodeURIComponent(date instanceof Date
						? Utils.formatDateIso(date)
						: String(date)
					);
				}
				reqFormat && q.push('output='+reqFormat);
				reqStart && q.push('start_date='+formatDate(reqStart));
				reqEnd && q.push('end_date='+formatDate(reqEnd));
				reqStatus && q.push('status='+reqStatus);
				q = q.length ? '?'+q.join('&') : '';
				return (url) => url+q;
			}	
		},
		methods: {
			onCampoClickDay: function (payload) {
				var campo = payload.campo;
				var day = payload.day;
				var start, end;
				var cdInicial = this.campoDataInicial;
				var cdFinal = this.campoDataFinal;
		
				this.$store.commit('aplicarCalendarMonth', {
					campo: cdInicial,
					dataMes: campo.dataMes
				});
				this.$store.commit('aplicarCalendarMonth', {
					campo: cdFinal,
					dataMes: campo.dataMes
				});
		
				if (campo === cdInicial) {
					start = day.startStart;
					end = day.startEnd;
				} else if (campo === cdFinal) {
					start = day.endStart;
					end = day.endEnd;
				} else {
					return;
				}
		
				var camposPromise = [
					this.$store.dispatch('campoCalendarPeriod', {
					campo: cdInicial,
					start: start,
					end: end
					}),
					this.$store.dispatch('campoCalendarPeriod', {
					campo: cdFinal,
					start: start,
					end: end
					})
				];
		
				Promise.all(camposPromise).then(function () {
					cdInicial.selecionado = start
					? Utils.formatDateUser(start)
					: null;
					cdFinal.selecionado = end
					? Utils.formatDateUser(end)
					: null;
				});

				this.pagination.actual = 1;
				this.carregarRecebiveis();
		
				//this.$emit('setDateFilter', cdInicial);
		
			},
			onCampoChangeMonth: function (payload) {
				this.$store.dispatch('campoCalendarMonth', payload);
			},
			onCampoSelecionado: function(payload) {
				var vm = this;
				vm.$emit('statusSelecionado', payload);
				vm.campoStatus.selecionado = payload.selecionado;
				vm.status = payload.selecionado.valor;
				vm.carregarRecebiveis();
			},
			onCampoBlur: function(payload) {
				this.$store.dispatch('validarCampo', payload);
			},
			formatStatus: function(str) {				
				var statusMap = {
					'Aguardando Pagamento'  : ['waiting_funds'],
					'Pago'  : ['paid'],
					'Pré-Pago'  : ['prepaid'],
					'Suspendido'  : ['suspended'],
				}

				for (var property in statusMap) {
					if (statusMap.hasOwnProperty(property)) {
				    	var statusIndex = statusMap[property].indexOf(str);
				    	// has matched
				    	if (statusIndex !== -1) {                        
				      	  return property;
				    	}
					}
				}

				// In case none matches
				return str

			},
			getTimelineMonths: function() {
				var now = new Date();
				var currentYear = now.getFullYear();
				var currentMonth = now.getMonth();
				var list = [];
				for (var i = 0; i < 14; i++) {
					list.push(new Date(currentYear, currentMonth-10+i, 1));
				}
				return {
					list: list,
					active: 10
				};
			},
			printDayMonthText: function(d) {
				return String(d.getDate()).concat(' de ',
					String(this.calMonthNames[d.getMonth()]).toLowerCase()
				);
			},
			onTimelineClickMonth: function(index) {
				this.timelineMonths.active = index;
				this.$nextTick(this.carregarRecebiveis);
			},
			
			clickReload: function() {},
			carregarRecebiveis: function() {
				setTimeout(() => {

				var vm = this;
				var dataInicial = "";
				var dataFinal = "";
				var pag = vm.pagination;
				var rec = vm.recebiveis;				

				if (vm.campoDataInicial.dataInicial && vm.campoDataInicial.dataFinal){
					var idata = vm.campoDataInicial.dataInicial;
					var fdata = vm.campoDataInicial.dataFinal;
	
					dataInicial = idata.getFullYear() + '-' + (idata.getMonth() + 1) + '-' + idata.getDate();
					dataFinal = fdata.getFullYear() + '-' + (fdata.getMonth() + 1) + '-' + fdata.getDate();
				}

				var req = {
					output: 'json',
					dateStart: dataInicial,
					dateEnd: dataFinal,
					offset: pag.actual * 10 - 10,
					shopping: vm.shopping,
					seller: vm.seller,
					status: vm.status
				};

				vm.queryFilterXLSX = req;

				vm.downloadRecebiveis = [
					['output', 'xlsx'],
					['start_date', dataInicial],
					['end_date', dataFinal],
					['offset', pag.actual * 10 - 10],
					['shopping', vm.shopping],
					['seller', vm.seller],
					['status', vm.status],
				].reduce((acc, [key, value]) => acc + (value ? `${ key }=${ value }&` : ''), '/receivables?')
				.replace(/[?&]$/, '');

				rec.loading = true;
				App.Services.recebiveis(req, function(loading, error, data) {
					if (loading) return;
					rec.loading = false;

					if (!error && 'string' === typeof data) {
						try {
							data = JSON.parse(data);
						} catch(parseError) {
							
							error = parseError;
						}
					}

					if(!error && data.results){
						var results = data.results;
						results.map(function(result){
							var paymentDate = new Date(result.payment_date);
							var dateCreated = new Date(result.date_created);
							var date = paymentDate.getTime() > dateCreated.getTime() ? paymentDate : dateCreated;
							date = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear(); 
							result.date = date;
							result.anticipation = result.model === 'receivable' ? result.anticipation_fee || '-' : result.fee || '-';
						});

						vm.paginate(data);
					}

					if(error && error.error.xhr.status){
						vm.$router.push({path: '/login'});
					} else if(error){
						rec.error = error.error.xhr.response ? error.error.xhr.response : error.message;;
					}
					
					rec.data = data;
				});

				}, 0);
			},
			onClickPedido: function(rec) {
				this.$emit('detalhes', rec);
			},

			nextPage: function(){
				var vm = this;
				var pag = this.pagination;
	
				if(pag.actual < pag.pages){
					pag.actual = pag.actual + 1;
					vm.carregarRecebiveis();
				}
			},
			prevPage: function(){
				var vm = this;
				var pag = this.pagination;
	
				if(pag.actual > 1){
					pag.actual = pag.actual - 1;
					vm.carregarRecebiveis();
				}
	
			},
			paginate: function(data){
				var pag = this.pagination;
				
				pag.results = data.count;
				pag.pages = Math.ceil(data.count / 10);
	
				pag.results > 10 ? pag.active = true : pag.active = false;
			}
		},
		mounted: function() {
			this.carregarRecebiveis();
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
