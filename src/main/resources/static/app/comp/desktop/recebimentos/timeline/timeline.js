(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["desktop/recebimentos/timeline"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		props: {
			monthNames: {
				type: Array,
				required: true
			},
			months: {
				type: Object,
				required: true
			},
			recebiveis: {
				type: Object,
				required: true
			}
		},
		computed: {
			currentMonthYearText: function() {
				var m = this.months;
				m = m.list[m.active];
				return [
					String(this.monthNames[m.getMonth()]),
					m.getFullYear()
				].join('/');
			}
		},
		methods: {
			onClickMonth: function() {				
				var args = Array.prototype.slice.call(arguments);
				this.$emit.apply(this, ['clickMonth'].concat(args));
			}
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
