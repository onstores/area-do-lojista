(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["select-date"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		props: {
			numImages: {
				type: Number,
				default: 3
			},
			monthNames: {
				type: Array,
				required: true
			},
			months: {
				type: Object,
				required: true
			}
		},
		data: function() {
			var count = this.months.list.length;
			var active = this.months.active;
			var numImages = Math.max(1, this.numImages);
			var pages = count / numImages - 1;
			var pageOffset = Math.floor((numImages - 1) * 0.5);
			var initialMarginLeft = Math.max(0, Math.min(pages, (active - pageOffset) / numImages));
			return {
				ident: 0,
				// activeMonthIndex: 0,
				timelineMarginLeftTarget: initialMarginLeft,
				timelineMarginLeftAnimated: initialMarginLeft,
				timelineCurrentAnimation: null
				// months: [
				// 	{ label: 'Ago/2018' },
				// 	{ label: 'Set/2018' },
				// 	{ label: 'Out/2018' },
				// 	{ label: 'Nov/2018' },
				// 	{ label: 'Dez/2018' },
				// 	{ label: 'Jan/2019' },
				// 	{ label: 'Fev/2019' }
				// ]
			};
		},
		computed: {
			activeMonthIndex: function() {
				return this.months.active;
			},
			marginPadding: function() { return 30; },
			calcTimelineWidth: function() {
				var f = this.months.list.length / Math.max(1, this.numImages);
				f = Math.floor(f * 10000) * 0.01;
				return f.toFixed(2)+'%';
			},
			calcItemWidth: function() {
				var f = 1 / (Math.max(1, this.months.list.length));
				f = Math.floor(f * 10000) * 0.01;
				return f.toFixed(2)+'%';
			}
		},
		methods: {
			getMonthLabel: function(date) {
				return [
					String(this.monthNames[date.getMonth()]).substr(0, 3),
					date.getFullYear()
				].join('/');
			},
			onClickMonth: function(item, index, $event) {				
				// console.log(item, index, $event);
				// this.activeMonthIndex = index;
				this.$emit('clickMonth', index, item, $event);
			},
			onClickMove: function(change) {
				var numImages = Math.max(1, this.numImages);
				change *= Math.max(1/numImages, (numImages - 1) / numImages);
				var vm = this;
				var from = this.timelineMarginLeftTarget;
				var pages = (this.months.list.length / numImages) - 1;
				var to = Math.max(0, Math.min(pages, from + change));
				var ease = Utils.easing.easeQuad;
				var mod = Utils.easing.modOut;
				this.timelineMarginLeftTarget = to;
				var tca = {};
				vm.timelineCurrentAnimation = tca;
				Utils.animate(from, to, 500, ease, mod, function(value, pos) {
					if (vm.timelineCurrentAnimation !== tca) return true; // break
					vm.timelineMarginLeftAnimated = value;
				});
			}
		}
		
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		
	};
})();
