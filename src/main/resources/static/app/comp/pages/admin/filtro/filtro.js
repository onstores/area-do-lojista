(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["pages/admin/filtro"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			return {
				campoSegmento: {
					nome: 'recFiltroShopping',
					rotulo: 'Shopping: ',
					selecionado: null,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				},
				shoppingReady: false,
				campoLoja: {
					nome: 'recFiltroLoja',
					rotulo: 'Loja: ',
					selecionado: null,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				},
				lojaReady: false
			};
		},
		methods: {
			onCampoSelecionado: function(payload) {
				this.$store.dispatch('campoSelecionado', payload);
				
				if(payload.campo && payload.campo.nome && payload.campo.nome == 'recFiltroShopping'){
					this.$emit('shoppingSelecionado', payload);
					this.shoppingSelecionado(payload);
				}
				if(payload.campo && payload.campo.nome && payload.campo.nome == 'recFiltroLoja'){
					this.$emit('lojaSelecionada', payload);
				}
			},
			onCampoBlur: function(payload) {
				this.$store.dispatch('validarCampo', payload);
			},
			shoppingSelecionado: function(payload){
				var self = this;
				var id = payload.selecionado ? payload.selecionado.id : payload
				var emptySelect = {
					id: "",
					texto: "Todas"
				};

				self.campoLoja.selecionado = null;
				if(id){
					App.Services.lojas(String(id), function(loading, error, data){

						if (loading){
							return
						}else if(data && data.content){
							var sellers = [emptySelect];

							var options = data.content;
							options.map(function(option, index){
								option.valor = 1 + index;
								option.texto = option.name;
							});

							options.sort(function (a, b) {
							  return a.name.localeCompare(b.name);
							});

							options.map(function(opt){
								sellers.push(opt);
							});

							self.campoLoja.opcoes = sellers;
							self.lojaReady = true;  
						}
					});
				}else{
					self.campoLoja.opcoes = [emptySelect];
					self.$emit('lojaSelecionada', {selecionado: emptySelect});
				}
			},

			carregarShoppings: function(){
				var self = this;
				App.Services.shoppings(function(loading, error, data) {
				
					if (loading) {
						return;
					}else if(data && data.content){
						var shoppings = [{
							texto: "Todos",
							id: ""
						}];

						data.content.map(function(option){
							option.texto = option.name;	
							shoppings.push(option);
						});

						self.campoSegmento.opcoes = shoppings;
						self.shoppingReady = true;
					}
				});
			}
		},
		mounted: function() {
			if(this.isAdminOnstore){
				this.carregarShoppings();
			}else if(this.isShopping){
				this.shoppingSelecionado(this.shoppingId);
			}
		},
		computed: {
			isAdminOnstore: function() {
				return this.$store.getters.isAdminOnstore;
			},
			isShopping: function() {
				return this.$store.getters.isShopping;
			},
			shoppingId: function() {
				return this.$store.getters.shoppingId;
			},
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
