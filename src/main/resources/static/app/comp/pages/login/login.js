(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["pages/login"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			return {
				modalEsqueci: false,
				campoUsuario: {
					nome: 'username',
					rotulo: 'Username',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoSenha: {
					nome: 'password',
					tipo: 'password',
					rotulo: 'Senha',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				}
				// calDate: new Date(),
				// calStart: null,
				// calEnd: null,
				// calMonthNames: [
				// 	'Janeiro',
				// 	'Fevereiro',
				// 	'Março',
				// 	'Abril',
				// 	'Maio',
				// 	'Junho',
				// 	'Julho',
				// 	'Agosto',
				// 	'Setembro',
				// 	'Outubro',
				// 	'Novembro',
				// 	'Dezembro'
				// ],
				// calWeekHeader: [
				// 	'D', 'S', 'T', 'Q', 'Q', 'S', 'S'
				// ]
			};
		},
		computed: {
			baseUrl: function() {
				return this.$store.state.baseUrl;
			},
			routerBaseUrl: function() {
				return App.routerBaseUrl || '';
			}
		},
		methods: {
			submitLogin: function() {
				// alert('clicou login');
			},
			onCampoValor: function(payload) {
				this.$store.dispatch('campoValor', payload);
			},
			onCampoBlur: function(payload) {
				this.$store.dispatch('validarCampo', payload);
			}
			// onCalClickDay: function(obj) {
			// 	// console.log(obj);
			// 	this.calStart = obj.start;
			// 	this.calEnd = obj.end;
			// },
			// onCalChangeMonth: function(d, change) {
			// 	console.log(d, change);
			// 	this.calDate = d;
			// }
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
