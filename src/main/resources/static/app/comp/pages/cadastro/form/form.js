(function() {
  'use strict';
  window._var$.Utils.valida.customEmailValidation = function (campo) {
    var myRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    var valor = campo.valor;
    if (!myRegex.test(valor)) {
      return { erro: 'Email inválido' };
    }
  };
  
  var vars = window._var$;
  var App = window._app$;
  var Utils = vars.Utils;	
  
  var reDocOrImage = /^application\/pdf$|^image\/(?:jpe?g|png)$/i;
  App.compMap["pages/cadastro/form"] = function(callback, template, match) {
    comp.template = template;
    callback(null, comp);
  };
  var comp = {
    template: null,
    data: function() {
      var currentVm = this;
      return {
        modalSenha: false,
        checkboxAceito: false,
        enviandoCadastro: false,
        erroMensagem: null,
        uploadFile: null,
        campoCNPJ: {
          nome: 'usuario',
          rotulo: 'CNPJ: *',
          valor: '',
          disabled: this.$store.getters.isLogged,
          mask: Utils.mask.cnpj,					
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio,
            Utils.valida.cnpj
          ]
        },
        campoCNPJMatriz: {
          nome: 'usuariomatriz',
          rotulo: 'CNPJ Matriz: ',
          valor: '',
          mask: Utils.mask.cnpj,
          erro: null,
          falta: false,
          valido: false,
          valida: [						
            // Utils.valida.cnpj
          ]
        },
        campoRazaoSocial: {
          nome: 'razaosocial',
          rotulo: 'Razão Social: *',	
          disabled: this.$store.getters.isLogged,				
          valor: '',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoRazaoSocialMatriz: {
          nome: 'razaosocialmatriz',
          rotulo: 'Razão Social Matriz: ',
          valor: '',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            
          ]
        },
        campoNomeFantasia: {
          nome: 'nomefantasia',
          rotulo: 'Nome Fantasia: *',
          valor: '',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoSUC: {
          nome: 'suc',
          rotulo: 'SUC: *',
          valor: '',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoAndar: {
          nome: 'Piso',
          rotulo: 'Piso: *',
          selecionado: null,
          opcoes: [
            { valor: 0, texto: 'Térreo' },
            { valor: 1, texto: 'Piso 1' },
            { valor: 2, texto: 'Piso 2' },
            { valor: 3, texto: 'Piso 3' },
            { valor: 4, texto: 'Piso 4' },
            { valor: 5, texto: 'Piso 5' },
						{ valor: 6, texto: 'Piso 1A' },
						{ valor: 7, texto: 'Piso 2A' },
						{ valor: 8, texto: 'Piso 3A' },
						{ valor: 9, texto: '1º Subsolo' },
						{ valor: 10, texto: '2º Subsolo' },
						{ valor: 11, texto: 'Estacionamento G1' },
						{ valor: 12, texto: 'Estacionamento G2' }
					],
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.selecionado
          ]
        },
        campoCategoria: {
          nome: 'categoria',
          rotulo: 'Categoria: *',
          selecionado: null,
          opcoes: [
            { valor: 'alimentacao_e_restaurantes', texto: 'Alimentação e Restaurantes' },
            { valor: 'brinquedo', texto: 'Brinquedo' },
            { valor: 'casa_e_decoracao', texto: 'Casa e Decoração' },
            { valor: 'drogaria', texto: 'Drogaria' },
            { valor: 'esporte_e_lazer', texto: 'Esporte e Lazer' },
            { valor: 'joalheria_relojoaria_e_bijuterias', texto: 'Joalheria, Relojoaria e Bijuterias' },
            { valor: 'mala_mochila_e_acessorios', texto: 'Mala, Mochila e Acessórios' },
            { valor: 'mercado', texto: 'Mercado' },
            { valor: 'moda', texto: 'Moda' },
            { valor: 'papelaria_livros_e_presentes', texto: 'Papelaria, Livros e Presentes' },
            { valor: 'perfumaria_e_cosmeticos', texto: 'Perfumaria e Cosméticos' },
            { valor: 'pet_shop', texto: 'Pet Shop'},
            { valor: 'servicos', texto: 'Serviços'},
            { valor: 'tecnologia_informatica_e_eletronicos', texto: 'Tecnologia, Informática e Eletrônicos'},
          ],
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.selecionado
          ]
        },
        campoResponsavel: {
          nome: 'responsavel',
          rotulo: 'Nome do Responsável: *',
          valor: '',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoTelefone: {
          nome: 'telefone',
          rotulo: 'Telefone do Responsável: *',
          valor: '',
          mask: Utils.mask.fone,
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio,
            Utils.valida.fone
          ]
        },
        campoEmail: {
          nome: 'email',
          rotulo: 'Email do Responsável: *',
          valor: '',
          erro: null,
          falta: false,
          valido: false,					
          valida: [
            Utils.valida.naoVazio,
            Utils.valida.customEmailValidation			
            
          ]
        },
        campoDtRecebimento: {
          nome: 'DtRecebimento',
          rotulo: 'Data de recebimento: ',
          valor: 'Semanalmente',
          erro: null,
          falta: false,
          valido: false,
          disabled: true,
          valida: [
            Utils.valida.naoVazio,
          ]
        },
        campoAntecipacao: {
          nome: 'antecipacao',
          rotulo: 'Antecipação: ',
          valor: 'Não',
          erro: null,
          falta: false,
          valido: false,
          disabled: true,
          valida: [
            Utils.valida.naoVazio,
          ]
        },
        campoTaxa: {
          nome: 'taxa',
          rotulo: 'Taxa Onstore: ',
          valor: '4%',
          erro: null,
          falta: false,
          valido: false,
          disabled: true,
          valida: [
            Utils.valida.naoVazio,
          ]
        },
        campoShopping: {
          nome: 'shopping',
          rotulo: 'Shopping: *',
          selecionado: null,
          opcoes: [
            { valor: 'grandplazashopping', texto: 'Grand Plaza Shopping' },
            { valor: 'tieteplazashopping', texto: 'Tietê Plaza Shopping' },
            { valor: 'shoppingd', texto: 'Shopping D' },
            { valor: 'shoppingmetropolitanobarra', texto: 'Shopping Metropolitano' },
            { valor: 'shoppingcerrado', texto: 'Shopping Cerrado' },
            { valor: 'shoppingcidadesp', texto: 'Shopping Cidade São Paulo' },
            { valor: 'onservicosdigitais', texto: 'ON Stores' }
          ],
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.selecionado
          ]
        },
        // Fim dos campos disable
        campoContaBanco: {
          nome: 'contaBanco',
          rotulo: 'Banco: *',
          selecionado: null,
          opcoes: [],
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.selecionado
          ]
        },
        campoContaAgencia: {
          nome: 'contaAgencia',
          rotulo: 'Agência: *',
          valor: '',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoContaNumero: {
          nome: 'contaNumero',
          rotulo: 'Conta Corrente: *',
          valor: '',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoDV: {
          nome: 'DV',
          rotulo: 'DV: *',
          valor: '',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoAgenciaDV: {
          nome: 'AgenciaDV',
          rotulo: 'Agência DV:',
          valor: '',
          erro: null,
          falta: false,
          valido: false
        },
        campoAceito: {
          nome: 'aceitoTermos',
          rotulo: '',
          checked: false,
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.checked
          ]
        },
        campoUpload: {
          nome: 'file',
          rotulo: 'Comprovante Bancário: *',
          file: null,
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.hasFile,
            Utils.valida.isFile,
            Utils.valida.fnFileTypeRegex(
              /^application\/pdf$|^image\/(?:jpe?g|png)$/i,
              'Formato inválido, por favor envie um JPG, PNG ou PDF'
              ),
              Utils.valida.fnFileSizeUnder(
                1024 * 1024,
                'Arquivo muito grande, por favor envie um arquivo menor que 1 MB'
                )
              ]
            }
          };
        },
        computed: {
          baseUrl: function() {
            return this.$store.state.baseUrl;
          },
          isLoja: function() {				
            return this.$store.getters.isLoja;
          },
          isLogged: function() {
            return this.$route.path === '/loja/configuracoes';
            // return this.$store.getters.isLogged;
          },
          
          validEmail : function (campo) {
            var myRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            
            var valor = campo.valor;
            if (!myRegex.test(valor)) {
              return { erro: 'Email inválido' };
            }
          }
        },
        methods: {
          enviarDados: function() {
            var vm = this;
            this.enviandoCadastro = true;
            App.Services.cadastroLoja({
              // "id": "csplojahavaianas",
              "cnpjfilial" : this.campoCNPJ.valor,
              "cnpjmatriz" : this.campoCNPJMatriz.valor,
              "razaosocial" : this.campoRazaoSocial.valor,
              "razaosocialmatriz" : this.campoRazaoSocialMatriz.valor,
              "nomefantasia" : this.campoNomeFantasia.valor,
              "suc" : this.campoSUC.valor,
              "andar" : (this.campoAndar.selecionado || {}).valor,
              "categoria" : (this.campoCategoria.selecionado || {}).valor,
              "shoppingid" : (this.campoShopping.selecionado || {}).valor,
              "nomeresponsavel" : this.campoResponsavel.valor,
              "emailresponsavel" : this.campoEmail.valor,
              "telefoneresponsavel" : this.campoTelefone.valor,
              // "bancoSelect" : (this.campoContaBanco.selecionado).select,
              "banco" : (this.campoContaBanco.selecionado).texto.split('-')[1].trim(),
              "codigodobanco" : (this.campoContaBanco.selecionado).valor,
              "agencia" : this.campoContaAgencia.valor,
              "digitoverificadoragencia" : this.campoAgenciaDV.valor,
              "contacorrente" : this.campoContaNumero.valor,
              "digitoverificadorconta" : this.campoDV.valor,
              "file": this.campoUpload.file,
            }, function(loading, error, data) {					
              vm.enviandoCadastro = loading;
              
              if (loading) return;
              
              if (!error && data && !data.message) {
                vm.$emit('cadastrar');						
              } else {						
                vm.erroMensagem = data && data.message
                ? data.message
                : (error && error.error || error) || 'Resposta vazia do servidor(3)';
                // tratar o erro
              }
            });
          },
          carregarBancos: function() {
            var self = this;
            App.Services.bancos(function(loading, error, data) {
              if (loading) return;

              data.map(function(banco){
                banco.valor = banco.code;
                banco.texto = banco.code + ' - ' + banco.name;
              });

              data.sort(function(a, b){
                // Compare the 2 dates
                if(a.code < b.code) return -1;
                if(a.code > b.code) return 1;
                return 0;
              });
              
              self.campoContaBanco.opcoes = data;
            })
          },
          
          clickCadastrar: function() {
            var vm = this;
            this.erroMensagem = null;				
            this.$store.dispatch('validarForm', {
              cnpj: this.campoCNPJ,
              cnpjMatriz: this.campoCNPJMatriz,
              razaoSocial: this.campoRazaoSocial,
              razaoSocialMatriz: this.campoRazaoSocialMatriz,
              nomeFantasia: this.campoNomeFantasia,
              suc: this.campoSUC,
              andar: this.campoAndar,
              categoria: this.campoCategoria,
              shopping: this.campoShopping,
              responsavel: this.campoResponsavel,
              telefone: this.campoTelefone,
              email: this.campoEmail,
              contaBanco: this.campoContaBanco,
              contaAgencia: this.campoContaAgencia,
              AgenciaDV: this.campoAgenciaDV,
              contaNumero: this.campoContaNumero,
              digitoverificadorconta: this.campoDV,
              aceito: this.campoAceito,
              upload: this.campoUpload
            }).then(function(result) {					
              vm.erroMensagem = result.erroMensagem;
              if (!result.erroMensagem) {
                vm.enviarDados();
              }
            });
          },
          clickTermos: function() {
            this.$emit('termos');
          },
          onCampoValor: function(payload) {
            this.$store.dispatch('campoValor', payload);
          },
          onCampoSelecionado: function(payload) {
            this.$store.dispatch('campoSelecionado', payload);
          },
          onCampoCheck: function(payload) {
            this.$store.dispatch('campoCheck', payload);
          },
          onCampoBlur: function(payload) {
            this.$store.dispatch('validarCampo', payload);
          },
          onChangeFileUpload: function() {
            this.campoUpload.file = this.$refs.fileUpload.files[0] || null;
            this.$store.dispatch('validarCampo', { campo: this.campoUpload });
          },
          filterClickAceito: function(evt) {
            return !Utils.isChildOf(evt.target, this.$refs.btAbrirTermos);
          },
          findSelectOpcaoValor: function(obj) {
            var valor = obj.selecionado;
            obj.selecionado = null;
            Utils.forEach(obj.campo.opcoes, function(opcao) {
              if (valor == opcao.valor) {
                obj.selecionado = opcao;
                return this._break;
              }
            });
            return obj;
          },
          fillInputsWithSessionData: function() {
            var sData = this.$store.state.session.data;
            var cValor = 'campoValor';
            var cSelecionado = 'campoSelecionado';
            var plist = [
              this.$store.dispatch(cValor, {
                campo: this.campoCNPJ,
                valor: sData.cnpj || ''
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoRazaoSocial,
                valor: sData.razaoSocial || ''
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoCNPJMatriz,
                valor: sData.cnpjMatriz || ''
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoRazaoSocialMatriz,
                valor: sData.razaoSocialMatriz || ''
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoNomeFantasia,
                valor: sData.nomeFantasia || ''
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoSUC,
                valor: sData.suc || ''
              }),
              this.$store.dispatch(cSelecionado, this.findSelectOpcaoValor({
                campo: this.campoAndar,
                selecionado: sData.andar || ''
              })),
              this.$store.dispatch(cSelecionado, this.findSelectOpcaoValor({
                campo: this.campoCategoria,
                selecionado: sData.categoria || ''
              })),
              this.$store.dispatch(cSelecionado, this.findSelectOpcaoValor({
                campo: this.campoShopping,
                selecionado: sData.shoppingId || ''
              })),
              this.$store.dispatch(cValor, {
                campo: this.campoResponsavel,
                valor: sData.nomeResponsavel || ''
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoTelefone,
                valor: sData.telefoneResponsavel || ''
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoEmail,
                valor: sData.emailResponsavel || ''
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoDtRecebimento,
                valor: sData.dataRecebimento || ''
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoAntecipacao,
                valor: sData.antecipacao === true ? 'Sim' :
                sData.antecipacao === false ? 'Não' :
                String(sData.antecipacao)
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoTaxa,
                valor: Number(+sData.taxaOnstore).toFixed(4)
                .replace(/\.?0*$/,'')
                .replace('.',',')+' %'
              }),
              this.$store.dispatch(cSelecionado, this.findSelectOpcaoValor({
                campo: this.campoContaBanco,
                selecionado: sData.bankInfo.banco
              })),
              this.$store.dispatch(cValor, {
                campo: this.campoContaAgencia,
                valor: sData.bankInfo.agencia || ''
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoAgenciaDV,
                valor: sData.bankInfo.agenciaDV || ''
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoContaNumero,
                valor: sData.bankInfo.contaCorrente || ''
              }),
              this.$store.dispatch(cValor, {
                campo: this.campoDV,
                valor: sData.bankInfo.contaDV || ''
              })
            ];
            Promise.all(plist).then(function(){});
          },
          onChangeSession: function() {
            if (this.isLoja) {
              if (this.unwatchSession) {
                this.unwatchSession();
                this.unwatchSession = void 0;
              }
            }
            this.fillInputsWithSessionData();
          }
        },
        mounted: function() {
          this.carregarBancos()
        },
        created: function() {
          this.$on('termosAceitar', function() {
            this.onCampoCheck({
              campo: this.campoAceito,
              checked: true
            });
          });
          if (this.$store.state.session.loading) {
            this.unwatchSession = this.$watch(function() {
              return this.$store.state.session;
            }, this.onChangeSession);
          } else {
            this.onChangeSession();
          }
        }
        /*
        props: {
          propA: {
            type: String
          }
        },
        data: function() {
          return {};
        },
        computed: {},
        methods: {},
        created: function() {},
        mounted: function() {},
        beforeDestroy: function() {}
        */
      };
    })();
    