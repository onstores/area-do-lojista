(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["recebimentos"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		props: {
			recebiveis: {
				type: Object,
				required: true
			}
		},
		computed: {
			loading: function() {
				return this.recebiveis.loading;
			},
			error: function() {
				return this.recebiveis.error;
			},
			errorText: function() {
				var t = String(this.recebiveis.error);
				if (t === String({})) {
					t = JSON.stringify(t);
				}
				return t;
			},
			data: function() {
				return this.recebiveis.data;
			}
		},
		methods: {
			clickReload: function() {}
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
