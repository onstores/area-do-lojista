(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["modal/detalhes-pedido"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		props: {
			detalhes: {
				type: Object
			}
		},
		computed: {
			numeroTransacao: function() {
				return this.detalhes.recebimento.transaction_id;
			},
			dataTransacao: function() {
				return this.detalhes.recebimento.creation_date;
			},
			loading: function() {
				return this.detalhes.loading;
			},
			error: function() {
				return this.detalhes.error;
			},
			errorText: function() {
				var e = this.detalhes.error;
				var t = String(e && e.error || e);
				if (t === String({})) {
					t = JSON.stringify(e);
				}
				return t;
			},
			data: function() {
				return this.detalhes.data;
			},
			client: function() {
				var data = this.detalhes.data;
				return data && data.client || {};
			},
			items: function() {
				var data = this.detalhes.data;
				return data && data.items || [];
			}
		},
		methods: {
			clickFechar: function() {
				this.$emit('fechar');
			},
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
