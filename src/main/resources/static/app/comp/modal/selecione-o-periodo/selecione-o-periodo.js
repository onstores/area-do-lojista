(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["modal/selecione-o-periodo"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		props: {
			show: {
				required: false
			},
			initialDate: {
				type: Date,
				required: false
			},
			initialStart: {
				type: Date,
				required: false
			},
			initialEnd: {
				type: Date,
				required: false
			}
		},
		data: function() {
			return {
				calDate: this.initialDate || new Date(),
				calStart: this.initialStart,
				calEnd: this.initialEnd,
				campoInicial: {
					nome: 'inicial',
					rotulo: '',
					valor: '',
					readonly: true,
					placeholder: 'dd/mm/aaaa',
					falta: false,
					erro: null,
					valido: false,
					valida: []
				},
				campoFinal: {
					nome: 'final',
					rotulo: '',
					valor: '',
					readonly: true,
					placeholder: 'dd/mm/aaaa',
					falta: false,
					erro: null,
					valido: false,
					valida: []
				}
			};
		},
		computed: {
			calMonthNames: function() {
				return Utils.getMonthNames();
			},
			calWeekHeader: function() {
				return Utils.getWeekDaysHeader();
			}
		},
		watch: {
			show: function() {
				var vm = this;
				this.$nextTick(function() {
					vm.calDate = vm.initialDate || new Date();
				});
			}
		},
		methods: {
			clickFechar: function() {
				this.$emit('fechar');
			},
			clickLimpar: function() {
				this.setarPeriodo(null, null);
				this.clickAceitar();
			},
			clickAceitar: function() {
				this.$emit('periodo', {
					start: this.calStart,
					end: this.calEnd
				});
				this.clickFechar();
			},
			setarPeriodo: function(start, end) {
				this.calStart = start;
				this.calEnd = end;
				this.$store.dispatch('campoValor', {
					campo: this.campoInicial,
					valor: start ? Utils.formatDateUser(start) : ''
				});
				this.$store.dispatch('campoValor', {
					campo: this.campoFinal,
					valor: end ? Utils.formatDateUser(end) : ''
				});
			},
			onCalClickDay: function(obj) {
				// console.log(obj);
				return this.setarPeriodo(obj.biggerStart, obj.biggerEnd);
			},
			onCalChangeMonth: function(d, change) {
				// console.log(d, change);
				this.calDate = d;
			}
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
