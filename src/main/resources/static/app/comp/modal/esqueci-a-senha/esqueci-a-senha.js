(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["modal/esqueci-a-senha"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			return {
				campoEmail: {
					nome: 'emailresponsavel',
					rotulo: 'Insira o e-mail cadastrado',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoCNPJ: {
					nome: 'cnpj',
					rotulo: 'Informe seu CNPJ',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio,
						Utils.valida.cnpj
					]
				},
				returnMessage: ""
			};
		},
		methods: {
			clickFechar: function() {
				this.returnMessage = "";
				this.$emit('fechar');
			},
			onCampoValor: function(payload) {
				this.$store.dispatch('campoValor', payload);
			},
			onCampoBlur: function(payload) {
				this.$store.dispatch('validarCampo', payload);
			},
			submitPassReset: function(event) {
				var self = this;

				event.preventDefault();

				if (this.campoCNPJ && this.campoCNPJ.valor && this.campoEmail && this.campoEmail.valor ){

					var req = JSON.stringify({
						"cnpj" : this.campoCNPJ.valor,
						"emailresponsavel" : this.campoEmail.valor
					});

					App.Services.passReset(req, function(loading, error, data) {
						if (loading) return;

						if(!error && data){
							self.returnMessage = data
						}else if(error && data){
							self.returnMessage = data
						}else{
							self.returnMessage = "Ocorreu um erro inesperado. Por favor tente novamente ou entre em contato com o responsável técnico"
						}						
					});
					
				}
			}
		},
		computed: {
			baseUrl: function() {
				return this.$store.state.baseUrl;
			},
			routerBaseUrl: function() {
				return App.routerBaseUrl || '';
			}
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
