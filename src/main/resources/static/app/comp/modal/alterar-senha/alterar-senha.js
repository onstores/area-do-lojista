(function() {
  'use strict';
  var vars = window._var$;
  var App = window._app$;
  var Utils = vars.Utils;
  App.compMap["modal/alterar-senha"] = function(callback, template, match) {
    comp.template = template;
    callback(null, comp);
  };
  var comp = {
    template: null,
    data: function() {
      return {
        campoOldPass: {
          nome: 'oldpassword',
          rotulo: 'Informe sua senha atual',
          tipo: 'password',
          valor: '',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoNewPass: {
          nome: 'newpassword',
          rotulo: 'Informe a nova senha',
          tipo: 'password',
          valor: '',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        returnMessage: ""
      };
    },
    methods: {
      clickFechar: function() {
        this.$emit('fechar');
        this.returnMessage = "";
      },
      onCampoValor: function(payload) {
        this.$store.dispatch('campoValor', payload);
      },
      onCampoBlur: function(payload) {
        this.$store.dispatch('validarCampo', payload);
      },
      submitNewPass: function(event) {
        var self = this;
        
        event.preventDefault();
        
        if (this.campoOldPass && this.campoOldPass.valor && this.campoNewPass && this.campoNewPass.valor ){

          if(this.campoOldPass.valor == this.campoNewPass.valor){
            this.returnMessage = "Os campos devem possuir valores diferentes";
            return
          }


          var req = JSON.stringify({
            "oldpassword" : this.campoOldPass.valor,
            "newpassword" : this.campoNewPass.valor
          });
          
          App.Services.newPass(req, function(loading, error, data) {
            
            if (loading) {
              return
            } else if (error == "Sucesso"){
              self.returnMessage = "Sua senha foi alterada";
            } else if (error.message != "") {
              self.returnMessage = "Verifique os dados inseridos";
            }
            
            // if (data && data.results instanceof Array) {
            // 	Utils.forEach(data.results, function(item) {
            // 		item.expand = false;
            // 		item.height = 0;
            // 		item.currentAnimation = null;
            // 	});
            // } else {
            // 	data = [];
            // }
            
          });
          
        }
        else {
          this.returnMessage = "Os campos devem ser preenchidos";
        }
      }
    },
    computed: {
      baseUrl: function() {
        return this.$store.state.baseUrl;
      },
      routerBaseUrl: function() {
        return App.routerBaseUrl || '';
      }
    }
    /*
    props: {
      propA: {
        type: String
      }
    },
    data: function() {
      return {};
    },
    computed: {},
    methods: {},
    created: function() {},
    mounted: function() {},
    beforeDestroy: function() {}
    */
  };
})();
