(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["modal/estorno-boleto"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			return {
				campoNome: {
					nome: 'campoNome',
					rotulo: 'Nome: ',
					selecionado: null,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				},
				campoCPF: {
					nome: 'campoCPF',
					rotulo: 'CPF: ',
					selecionado: null,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				},
				campoBanco: {
					nome: 'campoBanco',
					rotulo: 'Banco: ',
					selecionado: null,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				},
				campoAgencia: {
					nome: 'campoAgencia',
					rotulo: 'Agência: *',
					selecionado: null,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				},
				campoAgenciaDigito: {
					nome: 'campoAgenciaDigito',
					rotulo: 'Dígito: *',
					selecionado: null,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				},
				campoCtCorrente: {
					nome: 'campoCtCorrente',
					rotulo: 'Conta Corrente: *',
					selecionado: null,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				},
				campodv: {
					nome: 'campodv',
					rotulo: 'DV: *',
					selecionado: null,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				}
			};
		},
		methods: {
			clickFechar: function() {
				this.$emit('fechar');
			},
			onCampoValor: function(payload) {
				this.$store.dispatch('campoValor', payload);
			},
			onCampoSelecionado: function(payload) {
				this.$store.dispatch('campoSelecionado', payload);
			},
			onCampoBlur: function(payload) {
				if(payload.campo.valor && this.campoAgencia && this.campoAgencia.valor){
					this.campoAgencia.valor = this.campoAgencia.valor.replace(/\D/g,'');
				}
				if(payload.campo.valor && this.campoAgenciaDigito && this.campoAgenciaDigito.valor){
					this.campoAgenciaDigito.valor = this.campoAgenciaDigito.valor.replace(/\D/g,'');
				}
				if(payload.campo.valor && this.campoCtCorrente && this.campoCtCorrente.valor){
					this.campoCtCorrente.valor = this.campoCtCorrente.valor.replace(/\D/g,'');
				}
				if(payload.campo.valor && this.campodv && this.campodv.valor){
					this.campodv.valor = this.campodv.valor.replace(/\D/g,'');
				}
				this.$store.dispatch('validarCampo', payload);
			},
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
