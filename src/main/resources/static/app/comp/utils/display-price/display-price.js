(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["utils/display-price"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		props: {
			before: {
				type: String
			},
			after: {
				type: String
			},
			thousandsSeparator: {
				type: String,
				default: '.'
			},
			decimalSeparator: {
				type: String,
				default: ','
			},
			value: {
				// type: [Number, String],
				required: true,
				// validator: function(p) {
				// 	return p != null && !isNaN(p);
				// }
			}
		},
		computed: {
			integer: function() {
				var currValue = this.value ? (typeof this.value === 'string' ? parseFloat(this.value) : this.value) : 0;

				return currValue
					.toString()
					.replace(/\.\d*/, '')
					.split('')
						.reverse()
							.map((value, index) => ((index + 1) % 3 ? '' : this.thousandsSeparator ) + value)
							//adiciona o ponto a cada 3 caracteres
						.reverse()
					.join('')
					.replace(/^(-)?\.(-)?/, '$1$2'); //tira o ponto no começo do número mantendo o hífen
			},
			cents: function() {
				var currValue = this.value ? (typeof this.value === 'string' ? parseFloat(this.value) : this.value) : 0;

				return this.decimalSeparator+currValue.toFixed(2).substr(-2);
			}
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
