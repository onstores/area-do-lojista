(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["pages-desktop/configuracoes/form"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			return {
				checkboxAceito: true,
				enviandoCadastro: false,
				erroMensagem: null,
				arquivoSelecionado: null,
				campoCNPJ: {
					nome: 'usuario',
					rotulo: 'CNPJ: *',					
					valor: '',
					mask: Utils.mask.cnpj,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio,
						Utils.valida.cnpj
					]
				},
				campoRazaoSocial: {
					nome: 'razaosocial',
					rotulo: 'Razão Social: *',					
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoNomeFantasia: {
					nome: 'nomefantasia',
					rotulo: 'Nome Fantasia: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoSUC: {
					nome: 'suc',
					rotulo: 'SUC: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoAndar: {
					nome: 'Piso',
					rotulo: 'Piso: *',
					selecionado: null,
					opcoes: [
						{ valor: 0, texto: 'Térreo' },
						{ valor: 1, texto: 'Piso 1' },
						{ valor: 2, texto: 'Piso 2' },
						{ valor: 3, texto: 'Piso 3' },
						{ valor: 4, texto: 'Piso 4' },
						{ valor: 5, texto: 'Piso 5' },
						{ valor: 6, texto: 'Piso 1A' },
						{ valor: 7, texto: 'Piso 2A' },
						{ valor: 8, texto: 'Piso 3A' },
						{ valor: 9, texto: '1º Subsolo' },
						{ valor: 10, texto: '2º Subsolo' },
						{ valor: 11, texto: 'Estacionamento G1' },
						{ valor: 12, texto: 'Estacionamento G2' }
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.selecionado
					]
				},
				campoCategoria: {
					nome: 'categoria',
					rotulo: 'Categoria: *',
					selecionado: null,
					opcoes: [
						{ valor: 1, texto: '1' },
						{ valor: 2, texto: '2' },
						{ valor: 3, texto: '3' },
						{ valor: 4, texto: '4' },
						{ valor: 5, texto: '5' }
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.selecionado
					]
				},
				campoResponsavel: {
					nome: 'responsavel',
					rotulo: 'Nome do Responsável: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoTelefone: {
					nome: 'telefone',
					rotulo: 'Telefone do Responsável: *',
					valor: '',
					mask: Utils.mask.fone,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio,
						Utils.valida.fone
					]
				},
				campoEmail: {
					nome: 'email',
					rotulo: 'Email do Responsável: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio,
						Utils.valida.email
					]
				},
				campoDtRecebimento: {
					nome: 'DtRecebimento',
					rotulo: 'Data de recebimento: ',
					valor: 'Semanalmente',
					erro: null,
					falta: false,
					valido: false,
					disabled: true,
					valida: [
						Utils.valida.naoVazio,
					]
				},
				campoAntecipacao: {
					nome: 'antecipacao',
					rotulo: 'Antecipação: ',
					valor: 'Não',
					erro: null,
					falta: false,
					valido: false,
					disabled: true,
					valida: [
						Utils.valida.naoVazio,
					]
				},
				campoTaxa: {
					nome: 'taxa',
					rotulo: 'Taxa Onstore: ',
					valor: '4%',
					erro: null,
					falta: false,
					valido: false,
					disabled: true,
					valida: [
						Utils.valida.naoVazio,
					]
				},
				// Fim dos campos disable
				campoContaBanco: {
					nome: 'contaBanco',
					rotulo: 'Banco: *',
          selecionado: null,
					opcoes: [
						{ valor: '001', texto: '001 - Banco do Brasil' },
						{ valor: '237', texto: '237 - Bradesco' },
						{ valor: '104', texto: '104 - Caixa Econômica Federal' },
						{ valor: '341', texto: '341 - Itaú' },
						{ valor: '033', texto: '033 - Santander' }
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.selecionado
					]
				},
				campoContaAgencia: {
					nome: 'contaAgencia',
					rotulo: 'Agência: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoContaNumero: {
					nome: 'contaNumero',
					rotulo: 'Conta Corrente: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoDV: {
					nome: 'DV',
					rotulo: 'DV: *',
					valor: '',
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.naoVazio
					]
				},
				campoAceito: {
					nome: 'aceitoTermos',
					rotulo: '',
					checked: true,
					erro: null,
					falta: false,
					valido: false,
					valida: [
						Utils.valida.checked
					]
				}
			};
		},
		computed: {
			baseUrl: function() {
				return this.$store.state.baseUrl;
			}
		},
		methods: {
			enviarDados: function() {
				var vm = this;
				this.enviandoCadastro = true;
				App.Services.cadastroLoja({
					"id": "csplojahavaianas",
					"cnpjfilial" : this.campoCNPJ.valor,
					"cnpjmatriz" : this.campoCNPJ.valor,
					"razaosocial" : this.campoRazaoSocial.valor,
					"nomefantasia" : this.campoNomeFantasia.valor,
					"suc" : this.campoSUC.valor,
					"andar" : this.campoAndar.valor,
					"categoria" : this.campoCategoria.valor,
					"nomeresponsavel" : this.campoResponsavel.valor,
					"emailresponsavel" : this.campoEmail.valor,
					"telefoneresponsavel" : this.campoTelefone.valor,
					"codigodobanco" : this.campoContaBanco.codigodobanco,
					"banco" : this.campoContaBanco.texto,
					"codigodobanco" : this.campoContaBanco.valor,
					"agencia" : this.campoContaAgencia.valor,
					"contacorrente" : this.campoContaNumero.valor,
					"digitoverificadorconta" : this.campoDV.valor
					// "digitoverificadorconta" : ""
				}, function(loading, error, data) {
					vm.enviandoCadastro = loading;
					if (loading) return;
					console.log('cadastroLoja', error, data);
					if (!error && data && !data.message) {
						vm.$emit('cadastrar');
					} else {											
						vm.erroMensagem = data && data.message ? data.message : (error && error.error || error) || 'Resposta vazia do servidor(1)';
						// tratar o erro
					}
				});
			},
			clickCadastrar: function() {
				// this.$emit('cadastrar');
				var vm = this;
				this.erroMensagem = null;

				this.$store.dispatch('validarForm', {
					cnpj: this.campoCNPJ,
					razaoSocial: this.campoRazaoSocial,
					nomeFantasia: this.campoNomeFantasia,
					suc: this.campoSUC,
					andar: this.campoAndar,
					categoria: this.campoCategoria,
					responsavel: this.campoResponsavel,
					telefone: this.campoTelefone,
					email: this.campoEmail,
					contaBanco: this.campoContaBanco,
					contaAgencia: this.campoContaAgencia,
					contaNumero: this.campoContaNumero,
					digitoverificadorconta: this.campoDV,
					aceito: this.campoAceito
					
				}).then(function(result) {
					vm.erroMensagem = result.erroMensagem;
					if (!result.erroMensagem) {
						vm.enviarDados();
					}
				});
			},
			clickTermos: function() {
				this.$emit('termos');
			},
			onCampoValor: function(payload) {
				this.$store.dispatch('campoValor', payload);
			},
			onCampoSelecionado: function(payload) {
				this.$store.dispatch('campoSelecionado', payload);
			},
			onCampoCheck: function(payload) {
				this.$store.dispatch('campoCheck', payload);
			},
			onCampoBlur: function(payload) {
				this.$store.dispatch('validarCampo', payload);
			},
			filterClickAceito: function(evt) {
				return !Utils.isChildOf(evt.target, this.$refs.btAbrirTermos);
			},
			changeComprovante: function(evt) {
				this.arquivoSelecionado = evt.target.files[0];
			}
		},
		created: function() {
			this.$on('termosAceitar', function() {
				this.onCampoCheck({
					campo: this.campoAceito,
					checked: true
				});
			});
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
