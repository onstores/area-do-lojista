(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["pages-desktop/busca"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			return {
				pedidos: {
					loading: false,
					error: null,
					data: null
				}
			};
		},
		computed: {
			isAdmin: function() {
				return this.$store.getters.isAdmin;
			}
		},
		methods: {
			clickReload: function() {
				this.carregarPedidos();
			},
			carregarPedidos: function() {
				var vm = this;
				var ped = this.pedidos;
				ped.loading = true;
				App.Services.detalhesToken(this.$route.params.busca, function(loading, error, data) {
					if (loading) return;
					
					ped.loading = false;
					if (!error && 'string' === typeof data) {
						try {
							data = JSON.parse(data);
						} catch(parseError) {
							error = parseError;
						}
					}
					if (data && data instanceof Array) {
						Utils.forEach(data, function(item) {
							item.expand = false;
							item.height = 0;
							item.currentAnimation = null;
						});
					}else if(data && data instanceof Object) {
						data = [data]
					}else if(error && 'string' === typeof data){
						try {
							data = JSON.parse(data);
							data.message = data.detail;
							error.message = data.detail;
						} catch(parseError) {
							error = parseError;
						}
					}
					ped.error = error;
					ped.data = data;
				});
			},
			onClickPedido: function(ped) {
				this.$emit('detalhes', ped);
			}
		},
		mounted: function() {
			this.carregarPedidos();
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
