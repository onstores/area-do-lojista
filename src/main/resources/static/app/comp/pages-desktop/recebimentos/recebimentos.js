(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["pages-desktop/recebimentos"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			return {
				modalDetalhes: null,
				shopping: null,
				seller: null
			};
		},
		computed: {
			isAdmin: function() {
				return this.$store.getters.isAdmin;
			},
			isAdminOnstore: function() {
				return this.$store.getters.isAdminOnstore;
			},
			isShopping: function() {
				return this.$store.getters.isShopping;
			}
		},
		methods: {
			shoppingSelecionado: function(payload){
				var vm = this;

				if(payload.selecionado) vm.shopping = payload.selecionado.id;
				
				vm.lojaSelecionada({ selecionado: {
					id: "",
					texto: "Todas"
				} });
			},
			lojaSelecionada: function(payload){
				var vm = this;
				
				if(payload.selecionado) vm.seller = payload.selecionado.id;
				vm.$refs.childRecebimentos.carregarRecebiveis();
			},
			onDetalhes: function(rec) {
				var vm = this;
				App.Services.detalhesTransacao(rec.transaction_id, function(loading, error, data) {
					if (!loading && !error && null != data && 'string' === typeof data) {
						try {
							data = JSON.parse(data);
						} catch(parseError) {
							error = parseError;
						}
					}
					vm.modalDetalhes = {
						recebimento: rec,
						loading: loading,
						error: error,
						data: data
					};
					// console.log(loading, error, data);
				});
			}
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
