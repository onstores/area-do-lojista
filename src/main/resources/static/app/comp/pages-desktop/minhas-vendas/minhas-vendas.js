(function() {
'use strict';
window._var$.Utils.helpers = window._var$.Utils.helpers || {};
/*
 * defines a chunk helper 
 */
window._var$.Utils.helpers.chunk = function() {
	function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

	function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

	function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

	function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

	function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

	function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

	function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

	function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

	var denyThe = function denyThe(exp) {
	  return !exp;
	};

	var concat = function concat(list, el) {
	  return [].concat(_toConsumableArray(list), [el]);
	};

	var cleanArray = function cleanArray(arr) {
	  arr = [];
	  return arr;
	};

	var RestOfDivision = function RestOfDivision(x, y) {
	  return x % y;
	};

	var groupElements = function groupElements(acc, r) {
	  return [concat(acc, r), cleanArray(r)];
	};

	var testLength = function testLength(i, length, self) {
	  return denyThe(RestOfDivision(i + 1, length)) || i === self.length - 1;
	};

	var toChunk = function toChunk(length) {
	  var r = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
	  return function (acc, cur, i, self) {
	    r = concat(r, cur);

	    if (testLength(i, length, self)) {
	      var _groupElements = groupElements(acc, r);

	      var _groupElements2 = _slicedToArray(_groupElements, 2);

	      acc = _groupElements2[0];
	      r = _groupElements2[1];
	    }

	    return acc;
	  };
	};

	var chunk = function chunk(list, length) {
	  return list.reduce(toChunk(length), []);
	};

	return chunk
}(); 

var vars = window._var$;
var App = window._app$;
var Utils = vars.Utils;
App.compMap["pages-desktop/minhas-vendas"] = function(callback, template, match) {
	comp.template = template;
	callback(null, comp);
};
var comp = {
	template: null,
	data: function() {
		return {
			pedidos: {
				loading: false,
				error: null,
				data: null
			},
			pagination: {
				active: false,
				actual: 1,
				results: 0,
				pages: 0
			},
			selectedDateFilter: {},
			status: '',
			shopping: '',
			seller: ''
		};
	},
	computed: {
		isAdmin: function() {
			return this.$store.getters.isAdmin;
		},
		isAdminOnstore: function() {
			return this.$store.getters.isAdminOnstore;
		},
		isShopping: function() {
			return this.$store.getters.isShopping;
		},
		isLoja: function() {
			return this.$store.getters.isLoja;
		},
		isLogged: function() {
			return this.$store.getters.isLogged;
		},
		shoppingId: function() {
			return this.$store.getters.shoppingId;
		}
	},
	methods: {
		shoppingSelecionado: function(payload){
			this.shopping = payload.selecionado.id;
			this.carregarPedidos();
		},
		onStatusSelecionado: function(payload){
			this.status = payload.selecionado.valor;
			this.carregarPedidos();
		},
		lojaSelecionada: function(payload){
			this.seller = payload.selecionado.id;
			this.carregarPedidos();
		},

		clickReload: function() {
			this.carregarPedidos();
		},
		carregarPedidos: function() {
			var vm = this;
			var ped = vm.pedidos;
			var pag = vm.pagination;
			var date = vm.selectedDateFilter;
			var dataInicial = "";
			var dataFinal = "";
			var seller = vm.seller;
			var status = vm.status;

			var shopping = vm.shopping ? vm.shopping : (vm.shoppingId ? vm.shoppingId : '');

			if (date && date.dataInicial){
				var idata = date.dataInicial;
				var fdata = date.dataFinal;

				dataInicial = idata.getFullYear() + '-' + (idata.getMonth() + 1) + '-' + idata.getDate();
				dataFinal = fdata.getFullYear() + '-' + (fdata.getMonth() + 1) + '-' + fdata.getDate();
			}
			var req = {
				//status
				status: status,

				//shopping
				shopping: shopping,

				//seller
				seller: seller,

				//offset param
				offset: (pag.actual * 50 - 50),
				
				//start_date range param
				start_date: dataInicial,
				
				//end_date range param
				end_date: dataFinal
			}
			
			ped.loading = true;
			App.Services.pedidos(req, function(loading, error, data) {
				
				if (loading) return;

				ped.loading = false;					

				if (data && data.results instanceof Array) {
					Utils.forEach(data.results, function(item) {
						item.expand = false;
						item.height = 0;
						item.currentAnimation = null;
					});
				} else {
					data = [];
				}

				if(error && error.error.xhr.status == 401){
					vm.$router.push({path: '/login'});
				} else if(error){
					ped.error = error.error.xhr.response ? error.error.xhr.response : error.message;
				} else {
					ped.data = data.results;
					vm.paginate(data)
				}
				
			});
		},

		dateFilter: function(cdInicial){
			this.selectedDateFilter = cdInicial;

			this.pagination.actual = 1;
			this.carregarPedidos();
		},

		nextPage: function(){
			var vm = this;
			var pag = this.pagination;

			if(pag.actual < pag.pages){
				pag.actual = pag.actual + 1;
				vm.carregarPedidos();
			}
		},
		prevPage: function(){
			var vm = this;
			var pag = this.pagination;

			if(pag.actual > 1){
				pag.actual = pag.actual - 1;
				vm.carregarPedidos();
			}

		},
		paginate: function(data){
			var pag = this.pagination;
			
			pag.results = data.count;
			pag.pages = Math.ceil(data.count / 50);

			pag.results > 50 ? pag.active = true : pag.active = false;
		},

		onClickPedido: function(ped) {
			this.$emit('detalhes', ped);
		},

		// chunkedOrders: function (pedidos) {
		// 	console.log(this.$data)		;
		// 	var ped = pedidos;
		// 	ped.data = Utils.helpers.chunk(ped.data , 10)[0];
			

		// 	return ped
		// },
	},
	mounted: function() {
		this.carregarPedidos();
	}
	/*
	props: {
		propA: {
			type: String
		}
	},
	data: function() {
		return {};
	},
	computed: {},
	methods: {},
	created: function() {},
	mounted: function() {},
	beforeDestroy: function() {}
	*/
};
})();
