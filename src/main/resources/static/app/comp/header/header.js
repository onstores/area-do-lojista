(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["header"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		computed: {
			isLoja: function() {
				return this.$store.getters.isLoja;
			}
		},
		methods: {
			linkScope: function(link) {
				var getters = this.$store.getters;
				var base = getters.isAdmin ? '/admin/' :
					getters.isLoja ? '/loja/' :
					null;
				return base ? base + link : '/';
			}
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
