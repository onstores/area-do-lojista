(function () {
  'use strict';
  var vars = window._var$;
  var App = window._app$;
  var Utils = vars.Utils;
  App.compMap["detalhes-pedido"] = function (callback, template, match) {
    comp.template = template;
    callback(null, comp);
  };
  var comp = {
    template: null,
    props: {
      pedidoId: {
        type: String
      }
    },
    data: function () {
      return {
        erroMensagem: null,
        nothingToDeliver: false,
        confirmMessage: false,
        valida: true,
        seller_env_id: null,
        pedido: {
          loading: false,
          error: null,
          data: null
        },
        validacaoOpcoes: [],
        formattedItems: null,
        serviceEntrega: {
          loading: false,
          error: null,
          data: null
        },
        serviceEstorno: {
          loading: false,
          error: null,
          data: null
        },
        campoStatus: {
          nome: 'campoStatus',
          rotulo: 'Observação: ',
          selecionado: null,
          opcoes: [
            { valor: 1, texto: 'Todos' },
            { valor: 2, texto: 'Recebido' },
            { valor: 3, texto: 'Cancelado' }
          ],
          erro: null,
          falta: false,
          valido: false,
          valida: [
            // Utils.valida.selecionado
          ]
        },
        campoEntregar: {
          nome: 'campoEntregar',
          valor: '',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoToken: {
          nome: 'campoToken',
          rotulo: 'Token: *',
          erro: null,
          disabled: false,
          valor: '',
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoNf: {
          nome: 'campoNf',
          rotulo: 'Nota Fiscal: *',
          valor: '',
          erro: null,
          disabled: false,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },

        campoObservacoes: {
          nome: 'campoObservacoes',
          rotulo: 'Observações: ',
          erro: null,
          falta: false,
          disabled: false,
          valido: false,
          valida: [
            // Utils.valida.naoVazio
          ]
        },
        validacaoSelecionada: 'entregar',
        campoEntregarPedido: {
          nome: 'campoEntregarPedido',
          rotulo: 'Entregar Pedido',
          checked: true,
          valor: 'entregar',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.checked
          ]
        },
        campoEstornoCartao: {
          nome: 'campoEstornoCartao',
          rotulo: 'Estorno Cartão',
          checked: false,
          valor: 'estorno-cartao',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.checked
          ]
        },
        campoEstornoBoleto: {
          nome: 'campoEstornoBoleto',
          rotulo: 'Estorno Boleto',
          class: '',
          checked: false,
          disabled: false,
          valor: 'estorno-boleto',
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.checked
          ]
        },
        campoNome: {
          nome: 'campoNome',
          rotulo: 'Nome: ',
          selecionado: null,
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoCPF: {
          nome: 'campoCPF',
          rotulo: 'CPF: ',
          selecionado: null,
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        // campoBanco: {
        // 	nome: 'campoBanco',
        // 	rotulo: 'Banco: ',
        // 	selecionado: null,
        // 	erro: null,
        // 	falta: false,
        // 	valido: false,
        // 	valida: [
        // 		Utils.valida.naoVazio
        // 	]
        // },
        campoContaBanco: {
          nome: 'contaBanco',
          rotulo: 'Banco: *',
          selecionado: null,
          opcoes: [],
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.selecionado
          ]
        },
        campoAgencia: {
          nome: 'campoAgencia',
          rotulo: 'Agência: *',
          selecionado: null,
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoAgenciaDigito: {
          nome: 'campoAgenciaDigito',
          rotulo: 'Agência DV: *',
          selecionado: null,
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campoCtCorrente: {
          nome: 'campoCtCorrente',
          rotulo: 'Conta Corrente: *',
          selecionado: null,
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        },
        campodv: {
          nome: 'campodv',
          rotulo: 'DV: *',
          selecionado: null,
          erro: null,
          falta: false,
          valido: false,
          valida: [
            Utils.valida.naoVazio
          ]
        }
      };
    },
    computed: {
      isLoja: function () {
        return this.$store.getters.isLoja;
      },
      // validacaoOpcoes: function() {
      // 	return [
      // 		this.campoEntregarPedido,					
      // 		this.campoEstornoBoleto,
      // 		this.campoEstornoCartao
      // 	];
      // },
      loading: function () {
        return this.pedido.loading;
      },
      error: function () {
        return this.pedido.error;
      },
      errorText: function () {
        var e = this.pedido.error;
        var t = String(e && e.error || e);
        if (t === String({})) {
          t = JSON.stringify(e);
        }
        return t;
      },
      data: function () {
        return this.pedido.data;
      },
      client: function () {
        var data = this.pedido.data;
        return data && data.client || {};
      },
      payment: function () {
        var data = this.pedido.data;
        return data && data.payment || {};
      }
    },
    methods: {
      carregarBancos: function () {
        var self = this;
        App.Services.bancos(function (loading, error, data) {
          if (loading) return;

          data.map(function (banco) {
            banco.valor = banco.code;
            banco.texto = banco.code + ' - ' + banco.name;
          });

          data.sort(function (a, b) {
            // Compare the 2 dates
            if (a.code < b.code) return -1;
            if (a.code > b.code) return 1;
            return 0;
          });

          self.campoContaBanco.opcoes = data;
        })
      },
      onCampoSelecionado: function (payload) {
        this.$store.dispatch('campoSelecionado', payload);
      },
      onCampoValor: function (payload) {
        this.$store.dispatch('campoValor', payload);
      },
      onCampoCheck: function (payload) {
        var vm = this;
        vm.validacaoSelecionada = payload.campo.valor;
        Utils.forEach(vm.validacaoOpcoes, function (opcao) {
          var checked = opcao === payload.campo;
          var sendCheck = {
            campo: opcao,
            checked: checked
          };
          vm.$store.dispatch('campoCheck', sendCheck);
        });
      },
      onCampoBlur: function (payload) {
        if(payload.campo.valor && this.campoAgencia && this.campoAgencia.valor){
          this.campoAgencia.valor = this.campoAgencia.valor.replace(/\D/g,'');
        }
        if(payload.campo.valor && this.campoAgenciaDigito && this.campoAgenciaDigito.valor){
          this.campoAgenciaDigito.valor = this.campoAgenciaDigito.valor.replace(/\D/g,'');
        }
        if(payload.campo.valor && this.campoCtCorrente && this.campoCtCorrente.valor){
          this.campoCtCorrente.valor = this.campoCtCorrente.valor.replace(/\D/g,'');
        }
        if(payload.campo.valor && this.campodv && this.campodv.valor){
          this.campodv.valor = this.campodv.valor.replace(/\D/g,'');
        }
        this.$store.dispatch('validarCampo', payload);
      },
      serviceErrorMessage: function (svc) {
        // Refatorar => validar por status do request
        var messageValue = function (str) {
          return str == 'OK' ? 'Pedido Entregue com sucesso!' : 'Erro ao finalizar o pedido'
        }
        var e = svc.error;
        e = (e && e.message || messageValue(svc.data));
        var t = String(e);
        if (String({}) === t) {
          t = JSON.stringify(e);
        }
        return t;
      },
      serviceErrorMessageEstorno: function (error) {
        var e = error.xhr.response;
        var t = String(e);
        if (String({}) === t) {
          t = JSON.stringify(e);
        }
        console.log(error)
        return 'Transação não pode ser estornada';
      },
      serviceErrorDetail: function (error) {
        return error.error;
      },
      onClickEstornoBoleto: function () {
        this.reverseOrder();
      },
      onClickEstornoCartao: function () {
        this.reverseOrderCard();
      },
      reverseOrder: function () {
        var svc = this.serviceEstorno;
        svc.loading = true;
        this.campoAgencia.valor = this.campoAgencia.valor.replace(/\D/g,'');
        this.campoAgenciaDigito.valor = this.campoAgenciaDigito.valor.replace(/\D/g,'');
        App.Services.reverseOrder({
          'order_id': this.pedido.data.order_id,
          'bank_code': this.campoContaBanco.selecionado.valor,
          'agencia': this.campoAgencia.valor,
          'agencia_dv': this.campoAgenciaDigito.valor,
          'conta': this.campoCtCorrente.valor,
          'conta_dv': this.campodv.valor,
          'document_number': this.campoCPF.valor,
          'legal_name': this.campoNome.valor,
          'seller_env_id': this.seller_env_id

        }, function (loading, error, response) {

          if (loading) return;

          if (error && error.error.xhr.status != 200) {
            svc.error = error.error
            svc.data = null
            svc.loading = false 
          } else {
            svc.error = null;
            svc.data = response;
            svc.loading = false;
          }

        });
      },
      reverseOrderCard: function () {
        var svc = this.serviceEstorno;
        svc.loading = true;
        App.Services.reverseOrder({
          'order_id': this.pedido.data.order_id,
          'seller_env_id': this.seller_env_id
        }, function (loading, error, response) {

          if (loading) return;

          if (error && error.error.xhr.status != 200) {
            svc.error = error.error
            svc.data = null
            svc.loading = false 
          } else {
            svc.error = null;
            svc.data = response;
            svc.loading = false;
          }

        });
      },
      formatStatus: function (str) {
        var statusMap = {
          // 'processando'                          : ['order-created','order-completed', 'on-order-completed'],
          // 'aguardando confirmação do seller'     : ['waiting-for-seller-confirmation'],
          // 'aguardando autorização do pedido'     : ['waiting-for-order-authorization'],
          // 'aguardando decisão do seller'         : ['waiting-for-seller-decision'],
          // 'aguardando autorização para despachar': ['authorize-fulfillment'],
          // 'preparando entrega'                   : ['approve-payment', 'handling'],
          // 'pagamento pendente'                   : ['payment-pending'],
          // 'pagamento aprovado'                   : ['payment-approved'],
          // 'pagamento negado'                     : ['payment-denied'],
          // 'faturado'                             : ['invoiced'],
          // 'entregue'                             : ['delivered'],
          // 'solicitar cancelamento'               : ['request-cancel'],
          // 'erro na criação do pedido'            : ['order-create-error', 'order-creation-error'],
          // 'carência para cancelamento'           : ['window-to-cancel'],
          // 'pronto para o manuseio'               : ['ready-for-handling'],
          // 'iniciar manuseio'                     : ['start-handling'],
          // 'fatura pós-cancelamento negado'       : ['invoice-after-cancellation-deny'],
          // 'verificando envio'                    : ['order-accepted'],
          // 'enviando'                             : ['invoice'],
          // 'substituído'                          : ['replaced'],
          // 'cancelamento solicitado'              : ['cancellation-requested'],
          // 'cancelar'                             : ['cancel'],
          // 'cancelado'                            : ['canceled'],

          'aguardando cancelamento': ['waiting-for-seller-decision'],
          'aguardando pagamento': ['payment-pending', 'waiting-for-order-authorization', 'waiting-ffmt-authorization', ],
          'aguardando retirada': ['approve-payment', 'authorize-fulfillment', 'handling', 'invoice', 'order-accepted', 'payment-approved', 'ready-for-handling', 'start-handling', 'window-to-cancel'],
          'cancelamento solicitado': ['cancellation-requested', 'request-cancel'],
          'erro na criação do pedido': ['order-create-error', 'order-creation-error'],
          'erro no estorno': ['invoice-after-cancellation-deny'],
          'estornado': ['cancel', 'canceled', 'replaced'],
          'pagamento negado': ['payment-denied'],
          'processando': ['on-order-completed', 'order-completed', 'order-created', 'waiting-for-seller-confirmation'],
          'entregue': ['invoiced'],
          'entrega parcial': ['partially-canceled']
        }

        for (var property in statusMap) {
          if (statusMap.hasOwnProperty(property)) {
            var statusIndex = statusMap[property].indexOf(str);
            // has matched
            if (statusIndex !== -1) {
              return property;
            }
          }
        }

        // In case none matches
        return str

      },
      mustDisable: function (status) {
        // var hideArr = ['canceled', 'cancelled', 'cancellation-requested', 'cancel'];
        var hideArr = ['non-existing-status', 'partially-canceled'];

        return hideArr.indexOf(status) != -1
      },
      mustShow: function (str) {
        // var hideArr = ['canceled', 'cancelled', 'cancellation-requested'];
        var hideArr = ['non-existing-status', 'partially-canceled'];
        return hideArr.indexOf(str) != -1
      },
      clickReload: function () {
        this.carregarPedido(this.pedidoId);
      },
      carregarPedido: function (pedidoId) {
        var vm = this;
        var ped = this.pedido;
        ped.loading = true;

        App.Services.detalhesPedido(pedidoId, function (loading, error, data) {
          if (loading) return;

          ped.loading = false;
          ped.error = error;
          ped.data = data;

          var invalida = [
            'payment-pending',
            'waiting-for-order-authorization',
            'waiting-for-seller-confirmation',
            'order-created',
            'order-completed',
            'on-order-completed',
            'waiting-for-seller-decision',
            'waiting-ffmt-authorization',
            'request-cancel',
            'cancellation-requested',
            'order-create-error',
            'order-creation-error',
            'replaced',
            'cancel',
            'canceled',
            'payment-denied',
            'invoiced'
          ];

          data.status && invalida.indexOf(data.status) == -1 ?
            vm.valida = true :
            vm.valida = false;

          if(data.status == "handling" && data.status_delivered == "true"){
            vm.valida = false
          }

          if (data.seller_code) vm.seller_env_id = data.seller_code;

          var formattedItems = data.items.map(function (item, index) {
            item.toDeliver = 1;
            return item
          });

          vm.formattedItems = formattedItems;

          vm.validacaoOpcoes.push(vm.campoEntregarPedido);
          if (data.payment && data.payment.payment_method == "credit_card") {
            vm.validacaoOpcoes.push(vm.campoEstornoCartao);
          } else {
            vm.validacaoOpcoes.push(vm.campoEstornoBoleto);
          }

          vm.campoToken.disabled = vm.mustDisable(ped.data.status);
          vm.campoNf.disabled = vm.mustDisable(ped.data.status);
          vm.campoObservacoes.disabled = vm.mustDisable(ped.data.status);
          vm.campoEstornoBoleto.class = vm.mustDisable(ped.data.status) ? 'is--inactive' : '';
        });
      },
      // onClickEntregaPedido: function() {	
      // 	this.deliveryOrder();	
      // 	// App.Services.entregaProdutos(pacote, function(loading, error, data) {
      // 	// 	if (loading) return;
      // 	// 	svc.loading = false;					
      // 	// 	svc.error = error;
      // 	// 	svc.data = data;
      // 	// });
      // },
      onClickEntregaPedido: function () {
        var vm = this;
        vm.erroMensagem = null;

        vm.nothingToDeliver = false;
        this.formattedItems.map(function (item) {
          if (item.toDeliver == 0) {
            vm.nothingToDeliver = true;
          }
        });
        var i = vm.nothingToDeliver;

        var valida = [];
        i ? valida = {
          token: vm.campoToken
        } : valida = {
          token: vm.campoToken,
          invoice_number: vm.campoNf
        };

        vm.$store.dispatch('validarForm', valida).then(function (result) {
          vm.erroMensagem = result.erroMensagem;

          if (!result.erroMensagem) {
            vm.deliveryOrder();
            vm.confirmMessage = true;
          }
        });
      },
      confirmEmptyCheckout: function () {
        var vm = this;

        vm.nothingToDeliver = false;
        // vm.confirmMessage = false;
        vm.deliveryOrder();
      },
      resetEmptyCheckout: function () {
        var vm = this;

        vm.nothingToDeliver = false;
        vm.confirmMessage = false;
      },
      deliveryOrder: function () {
        var vm = this;
        var i = vm.nothingToDeliver;

        var svc = this.serviceEntrega;
        svc.loading = true;

        var items = this.formattedItems
          .map(function (item, index) {
            return ({
              id: item.item_id,
              quantity: item.toDeliver
            })
          });

        !i && App.Services.entregaProdutos({
          'order_id': this.pedido.data.order_id,
          'token': this.campoToken.valor,
          'invoice_number': this.campoNf.valor,
          'items': items,
          'seller_env_id': vm.seller_env_id,
          'reason': this.campoObservacoes.valor ? this.campoObservacoes.valor : null
        }, function (loading, error, data) {
          if (loading) return;

          if(error && error.error.xhr.status != 200) {
            if(error.error.xhr.response.includes("reason")){
              svc.error = 'O campo observações é obrigatório.'
            } else {
              svc.error = error.error.xhr.response;
            }
            svc.data = null;
            svc.loading = false;
          } 
          else {			
            svc.error = null;
            svc.data = data;
            svc.loading = false;
          }
        });

        i ? svc.loading = false : '';
      },
      onlyNumber(campo) {
        console.log(campo)
        return { erro: 'Somente número' };
      }
    },
    mounted: function () {
      this.carregarBancos();
      this.$watch('pedidoId', function (id) {
        if (id) {
          this.carregarPedido(id);
        } else {
          this.pedido.loading = false;
          this.pedido.error = null;
          this.pedido.data = null;
        }
      }, {
          immediate: true
        });
      this.carregarBancos();
    }
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
  };
})();
