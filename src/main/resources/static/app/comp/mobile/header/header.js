(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["mobile/header"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			var rp = this.$route.params;
			return {
				openMenu: null,
				buscaValue: rp && rp.busca || ''
			};
		},
		computed: {
			baseUrl: function() {
				return this.$store.state.baseUrl;
			},
			isLoja: function() {
				return this.$store.getters.isLoja;
			},
			isLogged: function() {
				return this.$store.getters.isLogged;
			}
		},
		methods: {
			linkScope: function(link) {
				var getters = this.$store.getters;
				var base = getters.isAdmin ? '/admin/' :
					getters.isLoja ? '/loja/' :
					null;
				return base ? base + link : '/';
			},
			onSubmitBusca: function() {
				App.Services.detalhesToken(this.buscaValue, function(loading, err, data) {
					if (loading) return;
					console.log('detalhesToken', err, data);
				});
			},
			documentClick: function(ev) {
				var anyBt = Utils.isChildOf(ev.target, this.$refs.btMenu)
					|| Utils.isChildOf(ev.target, this.$refs.btNotificacoes)
					|| Utils.isChildOf(ev.target, this.$refs.btBusca);
				if (!anyBt) {
					this.openMenu = null;
				}
			}
		},
		mounted: function() {
			var self = this;
			document.documentElement.addEventListener('click', this.documentClick, false);
		},
		beforeDestroy: function() {
			document.documentElement.removeEventListener('click', this.documentClick, false);
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
