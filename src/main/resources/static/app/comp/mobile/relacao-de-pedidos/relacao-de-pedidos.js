(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	var rePedidoDate = /(\d{1,2})\/(\d{1,2})\/(\d{1,4})(?:[\st-]+(\d{1,2}):(\d{1,2}))/i;
	App.compMap["mobile/relacao-de-pedidos"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		props: {
			listTitle: {
				type: String
			},
			pedidos: {
				type: Object,
				required: true
			}
		},
		data: function() {
			var opcaoStatusTodos = { valor: null, texto: 'Todos' };
			return {
				modalPeriodo: false,
				periodoInicio: null,
				periodoFim: null,
				itemState: [],
				heightClosed: 0,
				heightOpened: 172,
				heightUnit: 'px',
				// pedidos: {
				// 	loading: false,
				// 	error: null,
				// 	data: null
				// },
				campoStatus: {
					nome: 'campoStatus',
					rotulo: '',
					selecionado: opcaoStatusTodos,
					opcoes: [
						opcaoStatusTodos,
						{ valor: 'delivered', texto: 'Entregue' },
						{ valor: 'cancelled', texto: 'Cancelado' },
						{ valor: 'refunded', texto: 'Estornado' }
					],
					erro: null,
					falta: false,
					valido: false,
					valida: [
						// Utils.valida.selecionado
					]
				}
			};
		},
		computed: {
			isAdmin: function() {
				return this.$store.getters.isAdmin;
			},
			baseUrl: function() {
				return this.$store.state.baseUrl;
			},
			loading: function() {
				return this.pedidos.loading;
			},
			error: function() {
				return this.pedidos.error;
			},
			errorText: function() {
				var e = this.pedidos.error;
				var t = String(e && e.error || e);
				if (t === String({})) {
					t = JSON.stringify(e);
				}
				return t;
			},
			data: function() {
				return this.pedidos.data;
			},
			dataState: function() {
				var vm = this;
				var data = this.pedidos.data;
				var together = [];
				var pInicio = this.periodoInicio;
				var pFim = this.periodoFim;
				pFim = pFim && new Date(pFim.getFullYear(), pFim.getMonth(), pFim.getDate()+1);
				var status = this.campoStatus.selecionado;
				status = status && status.valor;
				var temFiltro = pInicio || pFim || status;
				var totalFiltro = 0;
				if (data && data instanceof Array) {
					Utils.forEach(data, function(pedido, index) {
						var dc = vm.parsePedidoDate(pedido.date_created);
						var mf = (dc
							? (pInicio ? pInicio <= dc : true)
								&& (pFim ? dc < pFim : true)
							: true)
							&& (status ? pedido.status == status : true);
						totalFiltro += mf ? 1 : 0;
						var state = vm.itemState[index] || {
							dateCreated: dc,
							expand: false,
							height: 0,
							currentAnimation: null,
							matchFilter: true
						};
						state.matchFilter = mf;
						vm.itemState[index] = state;
						together.push({
							pedido: pedido,
							state: state
						});
					});
				}
				vm.itemState = vm.itemState.slice(0, data && data.length || 0);
				return {
					together: together,
					totalFiltro: totalFiltro,
					temFiltro: temFiltro
				};
			},
			periodoText: function() {
				var inicio = this.periodoInicio;
				var fim = this.periodoFim;
				if (!inicio || !fim) {
					return 'Selecionar período';
				}
				if (+inicio == +fim) {
					return Utils.formatDateUser(inicio);
				} else {
					return [
						Utils.formatDateUser(inicio),
						Utils.formatDateUser(fim)
					].join(' a ');
				}
			}
		},
		methods: {
			parsePedidoDate: function(str) {
				var m = String(str).match(rePedidoDate);
				return m ? new Date(+m[3], +m[2]-1, +m[1], +m[4] || 0, +m[5] || 0) : void 0;
			},
			setPeriodo: function(obj) {
				this.periodoInicio = obj.start;
				this.periodoFim = obj.end;
			},
			onCampoSelecionado: function(payload) {
				this.$store.dispatch('campoSelecionado', payload);
			},
			onCampoBlur: function(payload) {
				this.$store.dispatch('validarCampo', payload);
			},
			onClickExpand: function(item) {
				var isp = item.state;
				var open = isp.expand;
				var from = open ? this.heightOpened : this.heightClosed;
				var to = open ? this.heightClosed : this.heightOpened;
				var unit = this.heightUnit;
				var ease = Utils.easing.easeQuad;
				var mod = Utils.easing.modOut;
				var rca = {};
				isp.currentAnimation = rca;
				isp.expand = !open;
				Utils.animate(from, to, 500, ease, mod, function(value, pos) {
					if (isp.currentAnimation !== rca) return true; // break
					isp.height = value.toFixed(0)+unit;
				});
			},
			clickReload: function() {
				this.$emit('pedidosReload');
			},
			makeLinkPedido: function(ped) {
				var userPath = this.$store.getters.isAdmin
					? '/admin'
					: '/loja';
				return userPath+'/detalhes-pedido/'+ped.order_id;
			},
			onClickPedido: function(ped) {
				this.$emit('detalhes', ped);
			}
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
