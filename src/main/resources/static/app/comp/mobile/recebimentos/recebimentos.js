(function() {
	'use strict';
	var vars = window._var$;
	var App = window._app$;
	var Utils = vars.Utils;
	App.compMap["mobile/recebimentos"] = function(callback, template, match) {
		comp.template = template;
		callback(null, comp);
	};
	var comp = {
		template: null,
		data: function() {
			return {
				heightClosed: 0,
				heightOpened: 170,
				heightUnit: 'px',
				timelineMonths: this.getTimelineMonths(),
				recebiveis: {
					loading: false,
					error: null,
					data: null
				}
			};
		},
		computed: {
			baseUrl: function() {
				return this.$store.state.baseUrl;
			},
			userCompanyName: function() {
				return this.$store.getters.userCompanyName;
			},
			calMonthNames: function() {
				return Utils.getMonthNames();
			},
			monthFirstDay: function() {
				var am = this.activeMonth;
				return new Date(am.getFullYear(), am.getMonth(), 1);
			},
			monthLastDay: function() {
				var am = this.activeMonth;
				return new Date(am.getFullYear(), am.getMonth()+1, 0);
			},
			activeMonth: function() {
				var tm = this.timelineMonths;
				var active = Math.max(0, Math.min(tm.list.length, tm.active));
				return tm.list[active];
			},
			loading: function() {
				return this.recebiveis.loading;
			},
			error: function() {
				return this.recebiveis.error;
			},
			errorText: function() {
				var e = this.recebiveis.error;
				var t = String(e && e.error || e);
				if (t === String({})) {
					t = JSON.stringify(e);
				}
				return t;
			},
			data: function() {
				return this.recebiveis.data;
			},
			dataReceivables: function() {
				var data = this.recebiveis.data;
				return data && data.receivables || [];
			}
		},
		methods: {
			getTimelineMonths: function() {
				var now = new Date();
				var currentYear = now.getFullYear();
				var currentMonth = now.getMonth();
				var list = [];
				for (var i = 0; i < 7; i++) {
					list.push(new Date(currentYear, currentMonth-3+i, 1));
				}
				return {
					list: list,
					active: 3
				};
			},
			printDayMonthText: function(d) {
				return String(d.getDate()).concat(' de ',
					String(this.calMonthNames[d.getMonth()]).toLowerCase()
				);
			},
			onTimelineClickMonth: function(index) {
				this.timelineMonths.active = index;
				this.$nextTick(this.carregarRecebiveis);
			},
			onClickExpand: function(rec) {
				var open = rec.expand;
				var from = open ? this.heightOpened : this.heightClosed;
				var to = open ? this.heightClosed : this.heightOpened;
				var unit = this.heightUnit;
				var ease = Utils.easing.easeQuad;
				var mod = Utils.easing.modOut;
				var rca = {};
				rec.currentAnimation = rca;
				rec.expand = !open;
				Utils.animate(from, to, 500, ease, mod, function(value, pos) {
					if (rec.currentAnimation !== rca) return true; // break
					rec.height = value.toFixed(0)+unit;
				});
			},
			clickReload: function() {},
			carregarRecebiveis: function() {
				var vm = this;
				var rec = this.recebiveis;
				var req = {
					dateStart: this.monthFirstDay,
					dateEnd: this.monthLastDay
				};
				rec.loading = true;
				App.Services.recebiveis(req, function(loading, error, data) {
					if (loading) return;
					rec.loading = false;
					// if (!error && 'string' === typeof data) {
					// 	try {
					// 		data = JSON.parse(data);
					// 	} catch(parseError) {
					// 		error = parseError;
					// 	}
					// }
					var list = data.receivables;
					if (list && list instanceof Array) {
						Utils.forEach(list, function(item) {
							item.expand = false;
							item.height = 0;
							item.currentAnimation = null;
						});
					} else {
						data.receivables = list = [];
					}
					// console.log('carregandoRecebiveis', error, data);
					rec.error = error;
					rec.data = data;
				});
			},
			makeLinkPedido: function(order_id) {
				var userPath = this.$store.getters.isAdmin
					? '/admin'
					: '/loja';
				return userPath+'/detalhes-pedido/'+order_id;
			},
			onClickPedido: function(rec) {},
			onClickDetalhes: function(rec) {
				this.$emit('detalhes', rec);
			}
		},
		mounted: function() {
			this.carregarRecebiveis();
		}
		/*
		props: {
			propA: {
				type: String
			}
		},
		data: function() {
			return {};
		},
		computed: {},
		methods: {},
		created: function() {},
		mounted: function() {},
		beforeDestroy: function() {}
		*/
	};
})();
