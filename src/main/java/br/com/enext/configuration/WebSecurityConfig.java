package br.com.enext.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import br.com.enext.services.SellerDetailService;

/*
	Classe responsável por validar a autenticação do user no sistema
	ao efetuar chamada de endpoint que comessem com /resource/*, /mock*, /login e /logout*.
*/

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private SellerDetailService userDetailsService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired 
	private AuthenticationSuccessHandler successfullLoginHandler;
	
	@Autowired 
	private LogoutSuccessHandler successfullLogoutHandler;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		logger.info("Requesting authentication");
		http.authorizeRequests()
				.antMatchers("/mock**").permitAll()
				.and()
			.authorizeRequests()
				.antMatchers("/login*").anonymous()
				.antMatchers("/resource/*").permitAll()
				//.anyRequest().authenticated()
				.and().csrf().disable()
			.formLogin()
				.loginPage("/login")
				.permitAll()
				.successHandler(successfullLoginHandler)
		        .failureUrl("/login?error=true")
				.and()			
			.logout()
				.permitAll()
				.logoutSuccessHandler(successfullLogoutHandler);
	}
	@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
    }
}
