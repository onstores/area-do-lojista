package br.com.enext.configuration;

import br.com.enext.enums.RoleType;
import br.com.enext.model.BankInfo;
import br.com.enext.model.Seller;
import br.com.enext.services.SellerService;
import br.com.enext.utils.Enviroment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/*
        Classe responsável por criar as contas onstores e dos shoppings ao iniciar o servidor.
        Essas contas devem ser criadas separadas pois possuem configurações distintas das padrões dispostas no sistema.

        OBS: Não está sendo chamada atualmente pois as contas já foram criadas.
*/

//TODO: Descomentar @Configuration caso necessite que os usuários sejam criados novamente.
//@Configuration
@Slf4j
public class InitOnStoreAndShoppingAccountsConfig {

    @Autowired
    SellerService sellerService;

    /*
        Criando as contas onstores e dos shoppings
    */
    @PostConstruct
    public void initAccounts() {
        // TODO: Utilizar a vtexAccountId de OnStores
        createSeller(
                Enviroment.getEnv("ONSTORES_CNPJ"),
                RoleType.ADMIN_ONSTORES,
                "",
                "onstores",
                Enviroment.getEnv("ONSTORES_EMAIL"),
                "onstores"
        );

        // Grand Plaza Shopping
        createSeller(
                Enviroment.getEnv("GRANDPLAZA_CNPJ"),
                RoleType.ADMIN_SHOPPING,
                "grandplazashopping",
                "grandplazashopping",
                Enviroment.getEnv("GRANDPLAZA_EMAIL"),
                "grandplazashopping"
        );

        // Cerrado Shopping
        createSeller(
                Enviroment.getEnv("CERRADO_CNPJ"),
                RoleType.ADMIN_SHOPPING,
                "shoppingcerrado",
                "shoppingcerrado",
                Enviroment.getEnv("CERRADO_EMAIL"),
                "shoppingcerrado"
        );

        // Shopping Cidade São Paulo
        createSeller(
                Enviroment.getEnv("CIDADESP_CNPJ"),
                RoleType.ADMIN_SHOPPING,
                "shoppingcidadesp",
                "shoppingcidadesp",
                Enviroment.getEnv("CIDADESP_EMAIL"),
                "shoppingcidadesp"
        );

        // Shopping D
        createSeller(
                Enviroment.getEnv("D_CNPJ"),
                RoleType.ADMIN_SHOPPING,
                "shoppingd",
                "shoppingd",
                Enviroment.getEnv("D_EMAIL"),
                "shoppingd"
        );

        // Shopping Metropolitano Barra
        createSeller(
                Enviroment.getEnv("METROPOLITANOBARRA_CNPJ"),
                RoleType.ADMIN_SHOPPING,
                "shoppingmetropolitanobarra",
                "shoppingmetropolitanobarra",
                Enviroment.getEnv("METROPOLITANOBARRA_EMAIL"),
                "shoppingmetropolitanobarra"
        );

        // Tiete Plaza Shopping
        createSeller(
                Enviroment.getEnv("PLAZASHOPPING_CNPJ"),
                RoleType.ADMIN_SHOPPING,
                "tieteplazashopping",
                "tieteplazashopping",
                Enviroment.getEnv("PLAZASHOPPING_EMAIL"),
                "tieteplazashopping"
        );
    }

    private Seller createSeller(String cnpj, RoleType roleType, String shoppingId, String vtexAccountId, String emailResponsavel, String idVtex) {
        Seller seller = new Seller();

        seller.setId(cnpj + "_" + roleType);
        seller.setCnpj(cnpj);
        seller.setRazaoSocial("");
        seller.setNomeFantasia("");
        seller.setSUC("");
        seller.setAndar("");
        seller.setCategoria("");
        seller.setNomeResponsavel("");
        seller.setTelefoneResponsavel("1111111111");
        seller.setEmailResponsavel(emailResponsavel);
        seller.setDataRecebimento("");

        BankInfo bankInfo = new BankInfo();
        bankInfo.setAgencia("");
        bankInfo.setBanco("");
        bankInfo.setComprovanteBancario("");
        bankInfo.setContaCorrente("");
        bankInfo.setContaDV("");

        seller.setBankInfo(bankInfo);
        seller.setRoleType(roleType);
        seller.setShoppingId(shoppingId);
        seller.setIdVtex(idVtex);
        seller.setTaxaOnstore("");

        seller.setEnabled(true);
        return sellerService.saveSeller(seller, vtexAccountId);
    }
}
