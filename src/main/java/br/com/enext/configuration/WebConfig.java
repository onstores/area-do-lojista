package br.com.enext.configuration;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import br.com.enext.filters.ResourceHeaderFilter;
import br.com.enext.handlers.SuccessfullLoginHandler;
import br.com.enext.handlers.SuccessfullLogoutHandler;
import br.com.enext.utils.Enviroment;

/*
	Spring Bean com dados de configuração para filtro (ResourceHeaderFilter class) dos endpoins que comessem com /resources/.
*/

@Configuration
@Slf4j
public class WebConfig {
	
	@Bean
	public FilterRegistrationBean<ResourceHeaderFilter> headerFilter() {
		log.info("Inicializando os filtros");
		FilterRegistrationBean<ResourceHeaderFilter> filterRegBean = new FilterRegistrationBean<>();
		filterRegBean.setFilter(new ResourceHeaderFilter());
		filterRegBean.addInitParameter("X-API-KEY", Enviroment.getEnv("X_API_KEY"));
		filterRegBean.addInitParameter("X-API-TOKEN", Enviroment.getEnv("X_API_TOKEN"));
		String urlPattern = "/resource/*";
		log.info(String.format("O filtro ResourceHeaderFilter irá atuar em endpoints do tipo: %s", urlPattern));
		filterRegBean.addUrlPatterns(urlPattern);
		filterRegBean.setOrder(Ordered.LOWEST_PRECEDENCE - 1);
		return filterRegBean;
	}
	
	@Bean
	public ValidatingMongoEventListener validatingMongoEventListener() {
	    return new ValidatingMongoEventListener(validator());
	}

	@Bean
	public LocalValidatorFactoryBean validator() {
	    return new LocalValidatorFactoryBean();
	}
	@Bean
	public AuthenticationSuccessHandler successfullLoginHandler() {
		return new SuccessfullLoginHandler();
	}
	@Bean
	public LogoutSuccessHandler successfullLogoutHandler() {
		return new SuccessfullLogoutHandler();
	}
	@Bean
	public SecurityContext context() {
		return SecurityContextHolder.getContext();
	}
}
