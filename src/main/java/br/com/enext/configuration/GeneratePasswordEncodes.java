package br.com.enext.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;

@Configuration
@Slf4j
public class GeneratePasswordEncodes {
    @Autowired
    private PasswordEncoder encoder;

    @PostConstruct
    public void showPasswordEncodes() {
        log.debug("--------------------------------------------------- Password encodes BEGIN");

//        log.debug("18180472000139_ADMIN_ONSTORES");
//        log.debug(encoder.encode("senhadeexemplo"));

        log.debug("--------------------------------------------------- Password encodes END");
    }
}
