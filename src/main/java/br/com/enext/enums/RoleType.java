package br.com.enext.enums;

/*
    Clçsse responsável por disponibilizar todos os Role Types possíveis.
*/

public enum RoleType {
    ADMIN_ONSTORES,
    ADMIN_SHOPPING,
    ADMIN_LOJISTA,
    FUNCIONARIO_LOJISTA
}
