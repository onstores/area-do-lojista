package br.com.enext.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.enext.enums.RoleType;
import br.com.enext.model.Seller;

import java.util.List;

/*
	Interface da tabela Seller no MongoDB.
*/

public interface SellerRepository extends MongoRepository<Seller, String>{
	public Seller findFirstByCnpj(String cnpj);
	Seller findByIdAndEnabled(String id, boolean enabled);

	public List<Seller> findByCnpjMatriz(String cnpjMatriz);
	
	public Seller findByRoleTypeAndCnpj(RoleType roleType, String Cnpj);

	public List<Seller> findByShoppingIdOrderByNomeFantasia (String shoppingId);

}
