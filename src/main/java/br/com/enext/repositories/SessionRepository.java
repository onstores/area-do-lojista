package br.com.enext.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.enext.model.Session;

/*
	Interface da tabela Session no MongoDB.
*/

public interface SessionRepository extends MongoRepository<Session, String>{
	Session findBySessionToken(String token);
	Session findBySellerId(String cnpj);
//	long deleteBySellerId(String token);
	long deleteBySessionToken(String token);
	
	
    List<Session> findAll();
    
}
