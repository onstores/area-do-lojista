package br.com.enext.utils;

/*
    Classe responsável por retirar toda pontuação padrão do CNPJ (12.345.678/0000-00 para 12345678000000)
*/

public class StringUtils {
    public static String getCleanCnpj(String originalCnpj) {
        String cleanCnpj = originalCnpj.replace("-", "");
        cleanCnpj = cleanCnpj.replace(".", "");
        cleanCnpj = cleanCnpj.replace("/", "");
        return cleanCnpj;
    }
}
