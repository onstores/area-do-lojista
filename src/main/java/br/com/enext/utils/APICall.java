package br.com.enext.utils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

/*
	Classe responsável por construir uma chamada de API de acordo com o caso necessário.
*/

@Slf4j
public class APICall {
	private HttpRequestBase method;

	public APICall(String url, String token, Class<?> m) {
        log.debug("Criando um objeto do tipo APICall com token");
        log.debug(String.format("url: %s", url));
        log.debug(String.format("token: %s", token));
        log.debug(String.format("m: %s", m));

		try {
			method = (HttpRequestBase) m.getConstructor(String.class).newInstance(url);
		} catch (Exception e) {
			e.printStackTrace();
		}

			method.setHeader("X-API-TOKEN", token);
	}

	public APICall(String url, Class<?> m) {
        log.debug("Criando um objeto do tipo APICall sem token");
        log.debug(String.format("url: %s", url));
        log.debug(String.format("m: %s", m));

		try {
			method = (HttpRequestBase) m.getConstructor(String.class).newInstance(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public <T> ResponseEntity<T> get(Class<T> c) throws ApiCallErrorException {
		// HttpHost proxy = new HttpHost("localhost", 8888);
        log.debug("--- BEGIN --- Chamada GET");
        log.debug(String.format("Iniciando o APICall.get(%s)", c));
        log.debug(String.format("request: %s", method));
        log.debug(String.format("request.getHeaders():"));
        for(Header h : method.getAllHeaders()) {
            log.debug(String.format("%s", h));
        }

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			HttpResponse response = httpClient.execute((HttpGet) method);
			HttpEntity entity = response.getEntity();

			String respAsString = EntityUtils.toString(entity, "UTF-8");
			log.debug(String.format("respAsString: %s", respAsString));

			if(response.getStatusLine().getStatusCode()/100 != 2) {
				throw ApiCallErrorException.builder()
						.httpStatus(HttpStatus.valueOf(response.getStatusLine().getStatusCode()))
						.respBody(respAsString)
						.build();
			}

			ResponseEntity<T> responseEntity = new ResponseEntity<T>(c.cast(new Gson().fromJson(respAsString, c)), HttpStatus.OK);

            log.debug(String.format("httpResponse: %s", response));
            log.debug(String.format("responseEntity: %s", responseEntity.toString()));
            log.debug("--- END --- Chamada GET");
            return responseEntity;
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		log.debug("Retornando ResponseEntity<>(null, HttpStatus.BAD_REQUEST)");
        log.debug("--- END --- Chamada GET");
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}

	public <T> ResponseEntity<List<T>> get(Class<T> c, Type t) throws ApiCallErrorException {
		// HttpHost proxy = new HttpHost("localhost", 8888);
        log.debug("--- BEGIN --- Chamada GET");
        log.debug(String.format("Iniciando o APICall.get(%s, %s)", c, t));
        log.debug(String.format("request: %s", method));
        log.debug(String.format("request.getHeaders():"));
        for(Header h : method.getAllHeaders()) {
            log.debug(String.format("%s", h));
        }

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			HttpResponse response = httpClient.execute((HttpGet) method);

			log.debug(String.format("httpResponse: %s", response));

			HttpEntity entity = response.getEntity();

			String respAsString = EntityUtils.toString(entity, "UTF-8");
			log.debug(String.format("respAsString: %s", respAsString));

			if(response.getStatusLine().getStatusCode()/100 != 2) {
				throw ApiCallErrorException.builder()
						.httpStatus(HttpStatus.valueOf(response.getStatusLine().getStatusCode()))
						.respBody(respAsString)
						.build();
			}

			List<T> l = new Gson().fromJson(respAsString, t);
            ResponseEntity<List<T>> responseEntity = new ResponseEntity<List<T>>(l, HttpStatus.OK);

            log.debug(String.format("responseEntity: %s", responseEntity.toString()));
            log.debug("--- END --- Chamada GET");
            return responseEntity;
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		log.debug("Retornando ResponseEntity<>(null, HttpStatus.BAD_REQUEST)");
        log.debug("--- END --- Chamada GET");
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}

	public <T> ResponseEntity<T> put(Class<T> c, Class<?> r, Object body) throws ApiCallErrorException {
        log.debug("--- BEGIN --- Chamada PUT");
        log.debug(String.format("Iniciando o APICall.put(%s, %s)", c, body));
        log.debug(String.format("request: %s", method));
        log.debug(String.format("request.getHeaders():"));
        for(Header h : method.getAllHeaders()) {
            log.debug(String.format("%s", h));
        }

	    try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			HttpPut put = (HttpPut) method;
			put.setEntity(new StringEntity(new Gson().toJson(r.cast(body))));
			HttpResponse response = httpClient.execute(put);
			HttpEntity entity = response.getEntity();

			String respAsString = EntityUtils.toString(entity, "UTF-8");
			log.debug(String.format("respAsString: %s", respAsString));

			if(response.getStatusLine().getStatusCode()/100 != 2) {
				throw ApiCallErrorException.builder()
						.httpStatus(HttpStatus.valueOf(response.getStatusLine().getStatusCode()))
						.respBody(respAsString)
						.build();
			}

            ResponseEntity<T> responseEntity = new ResponseEntity<T>(c.cast(new Gson().fromJson(respAsString, c)), HttpStatus.OK);

            log.debug(String.format("httpResponse: %s", response));
            log.debug(String.format("responseEntity: %s", responseEntity.toString()));
            log.debug("--- END --- Chamada PUT");
            return responseEntity;
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		log.debug("Retornando ResponseEntity<>(null, HttpStatus.BAD_REQUEST)");
        log.debug("--- END --- Chamada PUT");
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}

	public <T> ResponseEntity<T> put(Class<T> c, Class<?> r, Object body, Map<String, String> headers) throws ApiCallErrorException {
        log.debug("--- BEGIN --- Chamada PUT");
        log.debug(String.format("Iniciando o APICall.put(%s, %s, %s)", c, body, headers));
        log.debug(String.format("request: %s", method));
        log.debug(String.format("request.getHeaders():"));
        for(Header h : method.getAllHeaders()) {
            log.debug(String.format("%s", h));
        }

	    try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			/*ttpHost proxy = new HttpHost("127.0.0.1", 8888, "http");
			RequestConfig config = RequestConfig.custom()
                    .setProxy(proxy)
                    .build();*/
			HttpPut put = (HttpPut) method;
			//put.setConfig(config);
			if (headers != null) {
				for (String header : headers.keySet()) {
					put.addHeader(header, headers.get(header));
					log.debug(header + ":" + headers.get(header));
				}
			}
			String bodyAsString = new Gson().toJson(r.cast(body));
			log.debug("body: " + bodyAsString);
			put.setEntity(new StringEntity(bodyAsString, "UTF-8"));
			//log.info(put.getAllHeaders()[0].getName() + ":" + put.getAllHeaders()[0].getValue());
			HttpResponse response = httpClient.execute(put);
			HttpEntity entity = response.getEntity();

			String respAsString = EntityUtils.toString(entity, "UTF-8");
			log.debug(String.format("respAsString: %s", respAsString));

			if(response.getStatusLine().getStatusCode()/100 != 2) {
				throw ApiCallErrorException.builder()
						.httpStatus(HttpStatus.valueOf(response.getStatusLine().getStatusCode()))
						.respBody(respAsString)
						.build();
			}

            ResponseEntity<T> responseEntity = new ResponseEntity<T>(c.cast(new Gson().fromJson(respAsString, c)), HttpStatus.OK);

            log.debug(String.format("httpResponse: %s", response));
            log.debug(String.format("responseEntity: %s", responseEntity.toString()));
            log.debug("--- END --- Chamada PUT");
            return responseEntity;
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
        log.debug("Retornando ResponseEntity<>(null, HttpStatus.BAD_REQUEST)");
        log.debug("--- END --- Chamada PUT");
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}

	public <T> ResponseEntity<T> post(Class<T> c, Class<?> r, Object body, Map<String, String> headers) throws ApiCallErrorException {
        log.debug("--- BEGIN --- Chamada POST");
        log.debug(String.format("Iniciando o APICall.post(%s, %s, %s)", c, body, headers));
        log.debug(String.format("request: %s", method));
        log.debug(String.format("request.getHeaders():"));

	    try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {

			HttpPost post = (HttpPost) method;
			if (headers != null) {
				for (String header : headers.keySet()) {
					post.addHeader(header, headers.get(header));
					log.debug(header + ":" + headers.get(header));
				}
			}

			post.setEntity(new StringEntity(new Gson().toJson(r.cast(body))));
			HttpResponse response = httpClient.execute(post);
			HttpEntity entity = response.getEntity();

			String respAsString = EntityUtils.toString(entity, "UTF-8");
			log.debug(String.format("respAsString: %s", respAsString));

			if(response.getStatusLine().getStatusCode()/100 != 2) {
				throw ApiCallErrorException.builder()
						.httpStatus(HttpStatus.valueOf(response.getStatusLine().getStatusCode()))
						.respBody(respAsString)
						.build();
			}

            ResponseEntity<T> responseEntity = new ResponseEntity<T>(c.cast(new Gson().fromJson(EntityUtils.toString(entity, "UTF-8"), c)),
					HttpStatus.OK);

            log.debug(String.format("httpResponse: %s", response));
            log.debug(String.format("responseEntity: %s", responseEntity.toString()));
            log.debug("--- END --- Chamada POST");
            return responseEntity;
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		log.debug("Retornando ResponseEntity<>(null, HttpStatus.BAD_REQUEST)");
        log.debug("--- END --- Chamada POST");
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}

	public int post(MultipartFile file, String field) {
        log.debug("--- BEGIN --- Chamada POST file");
        log.debug(String.format("Iniciando o APICall.post(%s, %s)", file, field));
        log.debug(String.format("request: %s", method));
        log.debug(String.format("request.getHeaders():"));

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			try {
				builder.addBinaryBody(field, file.getInputStream(), ContentType.MULTIPART_FORM_DATA, file.getOriginalFilename());
			} catch (IOException e1) {
				log.error(e1.getMessage(), e1);
			}

			HttpPost post = (HttpPost) method;
			HttpEntity multipart = builder.build();
			post.setEntity(multipart);
			HttpResponse response = httpClient.execute(post);

			int responseStatusCode = response.getStatusLine().getStatusCode();

            log.debug(String.format("httpResponse: %s", response));
            log.debug(String.format("responseStatusCode: %s", responseStatusCode));
            log.debug("--- END --- Chamada POST");
			return responseStatusCode;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
        log.debug("Retornando 500");
        log.debug("--- END --- Chamada POST");
		return 500;
	}

	@Getter
	@Setter
	@Builder
	@ToString
	public static class ApiCallErrorException extends Exception {
		private HttpStatus httpStatus;
		private String respBody;

		public ResponseEntity getResponseEntity() {
			log.debug("getResponseEntity chamado de uma ApiCallErrorException");
			log.debug(String.format("httpStatus: %s", httpStatus));
			log.debug(String.format("respBody: %s", respBody));

			return new ResponseEntity<>(respBody, httpStatus);
		}
	}
}
