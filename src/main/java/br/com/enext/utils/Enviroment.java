package br.com.enext.utils;

/*
	Classe responsável por automatizar a escolha de chamada entre System.getenv() e System.getProperty() para evitar erros.
*/

public class Enviroment {
	public static String getEnv(String variable){
		return System.getenv(variable) != null ? System.getenv(variable) : System.getProperty(variable);
	}
}
