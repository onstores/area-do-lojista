package br.com.enext.utils;

/*
	Classe responsável por definir os campos da URL da API da Vtex.
*/

public class Static {
	public final static String PREFFIXENDPOINT = "https://";
	public final static String SUFFIXENDPOINT = ".vtexcommercestable.com.br/api/dataentities/%s/documents";
	public final static String PASSWORDSENDENTITY = "SP";
	public final static String SELLERENTITY = "AL";
}
