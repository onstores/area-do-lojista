package br.com.enext.model;

import lombok.*;

/*
    Classe modelo da resposta do endpoint /checkout (CheckoutController class).
*/

@Setter
@Getter
@ToString
public class CheckoutResponse {

	private String message;
	private String status;
	private String order_status;

	public String getFullMessage() {
		return String.format("%s. Status: %s. Status do pedido: %s", message, status, order_status);
	}
}
