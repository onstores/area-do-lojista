package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/*
    Classe modelo do cliente [Comprador]).
*/


@Getter
@Setter
@ToString
public class Client {
	private String name;
	private String cpf;
	private String email;
	private String phone;
	private String birth_date;
	private String gender;
	private String address;
}
