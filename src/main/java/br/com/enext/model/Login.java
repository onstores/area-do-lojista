package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

/*
	Classe modelo de login do usuário.
*/

@Getter
@Setter
public class Login {
	private String login;
	private String password;
}
