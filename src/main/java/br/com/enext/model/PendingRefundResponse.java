package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

/*
    TODO:APAGAR CLASSE?.
*/

@Getter
@Setter
public class PendingRefundResponse {
	
	private String orderId;
	private String clientCpf;
	private String clientName;
	private String dateCreated;
	private String status;
	private String paymentMethod;
	private int installments;	

}
