package br.com.enext.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/*
    Classe modelo utilizada como base para criação de shoppings no sistema.
*/

@Getter
@Setter
@AllArgsConstructor
public class ShoppingEntity {
    private String id;
    private String name;
}
