package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.List;

/*
    Classe modelo do Json necessário no Body do endpoint /checkout (CheckoutController class).
*/


@Getter
@Setter
@ToString
public class CheckoutBody {
    @NotNull
    private String order_id;
    @NotNull
    private String seller_env_id;
    @NotNull
    private String token;
    @NotNull
    private String invoice_number;
    private String reason;
    @NotNull
    private List<CheckoutBodyItem> items;
    private String bank_code;
    private String agencia;
    private String agencia_dv;
    private String conta;
    private String conta_dv;
    private String document_number;
    private String legal_name;
}
