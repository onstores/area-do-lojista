package br.com.enext.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/*
    Classe modelo de item de um select (Utilizado como array)
*/

@Getter
@Setter
@Builder
public class SelectOptionDto {
    private String id;
    private String name;
}
