package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/*
    Classe modelo da resposta para endpoint /refund_order (RefundOrderController class).
*/

@Setter
@Getter
@ToString
public class RefundOrderResponse {
	private String detail;
}
