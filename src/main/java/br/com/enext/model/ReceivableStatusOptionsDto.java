package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

/*
    Classe modelo de resposta para o endpoint /receivable-options (ReceivableOptionsController class)
*/

@Getter
@Setter
public class ReceivableStatusOptionsDto {
    List<SelectOptionDto> content = new LinkedList<>();

    public void add(String id, String name) {
        content.add(SelectOptionDto.builder().id(id).name(name).build());
    }



}
