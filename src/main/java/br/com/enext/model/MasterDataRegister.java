package br.com.enext.model;

import br.com.enext.enums.RoleType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CNPJ;

/*
    Classe modelo do Json necessário no Body do endpoint /masterdata (MasterdataController class).
*/

@Getter
@Setter
public class MasterDataRegister {
	private String id;

	@NotNull
	@CNPJ (message = "CNPJ inválido")
	private String cnpjfilial;
	
	private String cnpjmatriz;

	@NotNull
	private String razaosocial;

	@NotNull
	private String razaosocialmatriz;

	@NotNull
	private String nomefantasia;

	private String suc;

	@NotNull
	private Integer andar;

	@NotNull
	private String categoria;

	@NotNull
	private String nomeresponsavel;
	
	@NotNull
	@Email(message = "Email inválido")
	private String emailresponsavel;

	@NotNull
	private String telefoneresponsavel;

	@NotNull
	private String banco;

	private Integer codigodobanco;

	@NotNull
	private String agencia;

	private Integer digitoverificadoragencia;

	@NotNull
	private String contacorrente;

	@NotNull
	private String digitoverificadorconta;

	@NotNull
	private String shoppingid;

	private RoleType roletype;

	private Boolean cadastroaprovado;

	private Boolean isnew;

	private String idvtex;

	private String datarecebimento;

	private boolean antecipacao;

	private String taxaonstore;

	private boolean termwasaccepted;
}
