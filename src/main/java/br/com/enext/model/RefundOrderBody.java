package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/*
    Classe modelo do Json necessário no Body do endpoint /refund_order (RefundOrderController class).
*/

@Getter
@Setter
@ToString
public class RefundOrderBody {
	private String order_id;
	private String bank_code;
	private String agencia;
	private String agencia_dv;
	private String conta;
	private String conta_dv;
	private String document_number;
	private String legal_name;
	private String seller_env_id;
}
