package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

/*
    Classe modelo da resposta de erro do endpoint /checkout (CheckoutController class).
*/


@Getter
@Setter
public class CheckoutError {
    private String detail;
    private String status;
}
