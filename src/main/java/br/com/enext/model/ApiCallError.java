package br.com.enext.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/*
    Classe modelo da mensagem de retorno das chamadas de API.
*/

@Builder
@Getter
@Setter
public class ApiCallError {
    private String Message;
}
