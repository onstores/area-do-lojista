package  br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/*
    Classe modelo de um seller em um select.
*/

@Getter
@Setter
public class SellerOptionsDto {
    List<SelectOptionDto> content;
}
