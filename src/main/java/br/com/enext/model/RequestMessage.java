package br.com.enext.model;

/*
	Classe modelo responsável por guardar mensagem de resposta customizada.
*/

public class RequestMessage {
	public String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
