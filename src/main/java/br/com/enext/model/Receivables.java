package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/*
	Classe modelo da resposta do endpoint /receivables (ReceivablesResource class)
*/

@Getter
@Setter
public class Receivables {
	private Integer count;
	private String next;
	private String previous;
	private List<Receivable> results;
	private Integer num_pages;
	private Double total_received;
	private Double total_to_receive;
	private String last_update;
}
