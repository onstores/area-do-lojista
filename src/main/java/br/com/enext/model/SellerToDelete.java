package br.com.enext.model;

import br.com.enext.enums.RoleType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Field;

/*
    Classe modelo do Json necessário no Body do endpoint /resource/seller/delete (SellerResource class).
*/

@Setter
@Getter
public class SellerToDelete {

    @Field("cnpj")
    private String cnpj;

    @Field("roletype")
    private RoleType roleType;
}
