package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/*
    Classe modelo para paginação dos pedidos retornados pelo /orders (OrderResource class).
*/

@Getter
@Setter
public class PageOfOrders {
    private Integer count;
    private String next;
    private String previous;
    List<OrderData> results;
    private Integer num_pages;

    @Getter
    @Setter
    public static class OrderData {
        private String order_id;
        private String client_cpf;
        private String client_name;
        private String date_created;
        private String total;
        private String status;
        private String status_delivered;
        private String payment_method;
        private String installments;
    }
}
