package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

/*
    Classe modelo para criação de uma lista de shopings. (Utilizada para filtro)
*/

@Getter
@Setter
public class ShoppingOptionsDto {
    List<SelectOptionDto> content = new LinkedList<>();

    public void add(String id, String name) {
        content.add(SelectOptionDto.builder().id(id).name(name).build());
    }
}
