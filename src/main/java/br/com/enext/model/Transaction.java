package br.com.enext.model;

import java.util.List;

/*
	Classe modelo utilizada com resposta do endpoint /transaction/{id} (TransactionResource class)
*/

public class Transaction {
	private String transaction_id;
	private String status;
	private double total;
	private double seller_fee;
	private Client client;
	private List<Item> items;

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getSeller_fee() {
		return seller_fee;
	}

	public void setSeller_fee(double seller_fee) {
		this.seller_fee = seller_fee;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

}
