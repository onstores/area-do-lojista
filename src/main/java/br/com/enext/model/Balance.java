package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Balance {
    private String object;
    private WaitingFunds waiting_funds;
    private Transferred transferred;
    private Available available;
}

@Getter
@Setter
class Transferred {
    private String amount;
}
