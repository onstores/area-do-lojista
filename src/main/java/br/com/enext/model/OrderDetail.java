package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/*
	Classe modelo de resposta para os endpoints /order/{id} & /order/by_token (OrderResource class)

*/

@ToString
@Getter
@Setter
public class OrderDetail {
	private String order_id;
	private String date_created;
	private String status;
	private String quantity_total;
	private String quantity_delivered;
	private String status_delivered;
	private Double total;
	private Double seller_fee;
	private Client client;
	private Payment payment;
	private String seller_code;
	private List<Item> items;
}
