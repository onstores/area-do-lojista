package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/*
    Classe modelo da resposta para toda chamada feita para Vtex.
*/

@Getter
@Setter
@ToString
public class MasterDataResponse {
	private String Id;
	private String Href;
	private String DocumentId;
	private String Message;
}
