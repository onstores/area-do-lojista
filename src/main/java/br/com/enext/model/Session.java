package br.com.enext.model;

import java.util.Date;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
/*
	Classe modelo para Session do seller que será guardada no MongoDB
*/

@Document(collection = "session")
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Session {
	private String sellerId;
	private String sessionToken;
	private Date loggedIn;
}
