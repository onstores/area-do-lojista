package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/*
    TODO:APAGAR CLASSE?
*/

@ToString
@Getter
@Setter
public class OrderBasicData {
    private String order_id;
    private String client_cpf;
    private String client_name;
    private String date_created;
    private String status;
    private String payment_method;
    private Integer installments;
}
