package br.com.enext.model;

import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Field;

/*
    Classe modelo do Json necessário
    - No Body do endpoint /configurations/password/reset (SetPasswordResource class).
    - Como modelo de envio para API CALL "savePassword" (SavePasswordInVtexService class)
*/

public class SellerPassMessage {
	private String id;
	@Field("cnpj")
	private String cnpj;
	@Field("emailresponsavel")
	private String emailresponsavel;
	@Field("password")
	private String password;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmailresponsavel() {
		return emailresponsavel;
	}
	public void setEmailresponsavel(String emailresponsavel) {
		this.emailresponsavel = emailresponsavel;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
}
