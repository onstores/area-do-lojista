package br.com.enext.model;

import br.com.enext.enums.RoleType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/*
	Classe modelo para criação do token de sessão do seller.
	- Utilizada tambem como base para reconhecer filiais de uma matriz.
*/

@Getter
@Setter
public class TokenData {
	private String seller_env_id;
	private RoleType role;
	private String shoppingId;
	private List<String> sellerEnvIds;
}
