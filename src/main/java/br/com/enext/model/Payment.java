package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/*
    Classe modelo de pagamento utilizada na classe modelo OrderDetail.
*/

@Getter
@Setter
@ToString
public class Payment {
    private String payment_method;
    private String card_brand;
    private String installments;
}
