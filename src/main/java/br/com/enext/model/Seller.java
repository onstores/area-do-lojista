package br.com.enext.model;

import java.security.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import br.com.enext.enums.RoleType;
import lombok.ToString;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/*
	Classe modelo do seller
	Utilizado em geral para:
	* Autenticação
	* Salvar no banco
	* Busca no banco
	* Obter informações do security context.
*/

@Document(collection = "seller")
@ToString
public class Seller implements UserDetails, CredentialsContainer {

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Id
	@Field("id")
	private String id;

	@Field("isoldseller")
	private boolean isOldSeller;

	@NotNull(message = "Campo Seller CNPJ é obrigatório")
	@Size(min = 1)
	private String cnpj;

	@Field("cnpjmatriz")
	private String cnpjMatriz;

	private String password;

	@Field("razaosocial")
	@NotNull(message = "Campo Razão Social é obrigatório")
	private String razaoSocial;

	@Field("razaosocialmatriz")
	private String razaoSocialMatriz;

	@Field("nomefantasia")
	@NotNull(message = "Campo Nome Fantisa é obrigatório")
	private String nomeFantasia;

	@NotNull(message = "Campo SUC é obrigatório")
	private String SUC;

	@NotNull(message = "Campo Andar é obrigatório")
	private String andar;

	@NotNull(message = "Campo Categoria é obrigatório")
	private String categoria;

	@Field("nomeresponsavel")
	@NotNull(message = "Campo Nome Responsável é obrigatório")
	private String nomeResponsavel;

	@Field("telefoneresponsavel")
	@NotNull(message = "Campo Telefone Responsável é obrigatório")
	private String telefoneResponsavel;

	@Field("emailresponsavel")
	@NotNull(message = "Campo Enviar Responsável é obrigatório")
	@Email(message = "Email inválido")
	@NotEmpty
	@NotBlank
	private String emailResponsavel;

	@Field("datarecebimento")
	@NotNull(message = "Campo Data de Recebimento é obrigatório")
	private String dataRecebimento;

	@NotNull(message = "Campo Antecipação é obrigatório")
	private boolean antecipacao;

	@Field("taxaonstore")
	@NotNull(message = "Campo Taxa Onstore é obrigatório")
	private String taxaOnstore;

	@Field("bankinfo")
	@NotNull(message = "Campo Informações bancárias são obrigatórias")
	private BankInfo bankInfo;

	@Field("roletype")
	@NotNull(message = "Role é obrigatório")
	private RoleType roleType;

	@Field("shoppingid")
	@NotNull(message = "ShoppingId é obrigatório")
	private String shoppingId;

	@Field("idvtex")
	@NotNull(message = "IdVTex é obrigatório")
	private String idVtex;
	
	@Field("isnew")
	private boolean isNew;

	@Field("termwasaccepted")
	@NotNull(message = "Aceite de termos é obrigatório")
	private  boolean termwasaccepted;

	public String getLastLoginAt() {
		return lastLoginAt;
	}

	public void setLastLoginAt(String lastLoginAt) {
		this.lastLoginAt = lastLoginAt;
	}

	public String getCurrentLoginAt() {
		return currentLoginAt;
	}

	public void setCurrentLoginAt(String currentLoginAt) {
		this.currentLoginAt = currentLoginAt;
	}
	
	public String getSawNotificationsAt() {
		return sawNotificationsAt;
	}

	public void setSawNotificationsAt(String sawNotificationsAt) {
		this.sawNotificationsAt = sawNotificationsAt;
	}

	private String lastLoginAt;

	private String currentLoginAt;
	
	private String sawNotificationsAt;
	
	private String iv;

	private boolean enabled;

	transient private String token;

	public String getCnpjMatriz() {
		return cnpjMatriz;
	}

	public void setCnpjMatriz(String cnpjMatriz) {
		this.cnpjMatriz = cnpjMatriz;
	}

	public String getRazaoSocialMatriz() {
		return razaoSocialMatriz;
	}

	public void setRazaoSocialMatriz(String razaoSocialMatriz) {
		this.razaoSocialMatriz = razaoSocialMatriz;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getSUC() {
		return SUC;
	}

	public void setSUC(String SUC) {
		this.SUC = SUC;
	}

	public String getAndar() {
		return andar;
	}

	public void setAndar(String andar) {
		this.andar = andar;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getNomeResponsavel() {
		return nomeResponsavel;
	}

	public void setNomeResponsavel(String nomeResponsavel) {
		this.nomeResponsavel = nomeResponsavel;
	}

	public String getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(String dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public boolean isAntecipacao() {
		return antecipacao;
	}

	public void setAntecipacao(boolean antecipacao) {
		this.antecipacao = antecipacao;
	}

	public String getTaxaOnstore() {
		return taxaOnstore;
	}

	public void setTaxaOnstore(String taxaOnstore) {
		this.taxaOnstore = taxaOnstore;
	}

	public BankInfo getBankInfo() {
		return bankInfo;
	}

	public void setBankInfo(BankInfo bankInfo) {
		this.bankInfo = bankInfo;
	}

	public String getEmailResponsavel() {
		return emailResponsavel;
	}

	public void setEmailResponsavel(String emailResponsavel) {
		this.emailResponsavel = emailResponsavel;
	}

//	@Override
//	public Collection<? extends GrantedAuthority> getAuthorities() {
//		return null;
//	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTelefoneResponsavel() {
		return telefoneResponsavel;
	}

	public void setTelefoneResponsavel(String telefoneResponsavel) {
		this.telefoneResponsavel = telefoneResponsavel;
	}

	public String getIv() {
		return iv;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public RoleType getRoleType() {
		return roleType;
	}

	public void setRoleType(RoleType roleType) {
		this.roleType = roleType;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<SimpleGrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority("user"));

		return authorities;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return this.getCnpj();
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

//	@Override
//	public void eraseCredentials() {
//		password = null;
//	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getShoppingId() {
		return shoppingId;
	}

	public String getVtexAccountName() {
		if(this.roleType == RoleType.ADMIN_ONSTORES) {
			return "onstores";
		}
		else {
			return shoppingId;
		}
	}

	public void setShoppingId(String shoppingId) {
		this.shoppingId = shoppingId;
	}

	@Override
	public void eraseCredentials() {
		password = null;
	}

	public String getIdVtex() {
		return idVtex;
	}

	public void setIdVtex(String idVtex) {
		this.idVtex = idVtex;
	}

	public boolean isOldSeller() {
		return isOldSeller;
	}

	public void setOldSeller(boolean oldSeller) {
		isOldSeller = oldSeller;
	}

	public boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(boolean isNew) {
		this.isNew = isNew;
	}

	public boolean hasAccessToShoppingData(String shoppingId) {
		if(this.roleType == RoleType.ADMIN_ONSTORES) {
			return true;
		}
		else if(this.roleType == RoleType.ADMIN_SHOPPING && shoppingId.equals(this.shoppingId)){
			return true;
		}
		else {
			return false;
		}
	}

	public boolean isTermwasaccepted() {
		return termwasaccepted;
	}

	public void setTermwasaccepted(boolean termwasaccepted) {
		this.termwasaccepted = termwasaccepted;
	}
}
