package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/*
    Classe modelo de informações de checkout sobre um item.
    Utilizidado pela classe CheckoutBody como parte do Json necessário no Body do endpoint /checkout (CheckoutController class).
*/

@Getter
@Setter
@ToString
public class CheckoutBodyItem {
    @NotNull
    private String id;
    @NotNull
    private Integer quantity;
}
