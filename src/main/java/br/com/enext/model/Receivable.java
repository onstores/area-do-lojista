package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

/*
	Classe modelo de um recebível.
	Utilizada na classe modelo Receivables.
*/

@Getter
@Setter
public class Receivable {
	private Long id;
	private String pagarme_id;
	private String foreign_id;
	private String model;
	private double net;
	private double amount;
	private String installment;
	private String status;
	private String fee;
	private String anticipation_fee;
	private String split_rule_id;
	private String bulk_anticipation_id;
	private String type;
	private String payment_method;
	private String date_created;
	private String date_updated;
	private String payment_date;
	private String original_payment_date;
	private String accrual_date;
	private Integer transaction;
	private Integer seller;
	private ExtraInfo extra_info;
	private String remaining_installments;
}
