package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

/*
	Classe modelo da resposta da conexão com o MongoDB.
*/

@Getter
@Setter
public class MongoDBResponse {
	private int ok;
	private String errmsg;
	private int code;
	private String codeName;
}
