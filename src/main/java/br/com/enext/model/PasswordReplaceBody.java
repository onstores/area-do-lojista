package br.com.enext.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.mongodb.core.mapping.Field;
import lombok.Setter;
import lombok.Getter;

/*
    Classe modelo do Json necessário no Body do endpoint /configurations/password/replace (SetPasswordResource class).
*/

@Getter
@Setter
public class PasswordReplaceBody {

	@NotNull
	@NotBlank
	@Field("newpassword")
	private String newpassword;

	@NotNull
	@Field("oldpassword")
	private String oldpassword;

}
