package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BankAccount {

    private int id;
    private String type;
    private String conta;
    private String object;
    private String agencia;
    private String conta_dv;
    private String bank_code;
    private String agencia_dv;
    private String legal_name;
    private String date_created;
    private String document_type;
    private String document_number;
    private String charge_transfer_fees;
}
