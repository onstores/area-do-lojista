package br.com.enext.model;

/*
    TODO:APAGAR CLASSE?
*/

public class UserSession {
	private String id;
	private boolean isadmin;
	private String agencia;
	private boolean antecipacao;
	private String datadorecebimento;
	private String andar;
	private String categoria;
	private String cnpjfilial;
	private String codigodobanco;
	private String contacorrente;
	private String digitoverificadorconta;
	private String nomefantasia;
	private String razaosocial;
	private String suc;
	private double taxaonstore;
	private String nomeresponsavel;
	private String telefoneresponsavel;
	private String emailresponsavel;
	private boolean aceite;
	
	public UserSession(String id, boolean isadmin, String agencia, boolean antecipacao, String datadorecebimento,
			String andar, String categoria, String cnpjfilial, String codigodobanco, String contacorrente,
			String digitoverificadorconta, String nomefantasia, String razaosocial, String suc,
			double taxaonstore, String nomeresponsavel, String telefoneresponsavel, String emailresponsavel,
			boolean aceite) {
		super();
		this.id = id;
		this.isadmin = isadmin;
		this.agencia = agencia;
		this.antecipacao = antecipacao;
		this.datadorecebimento = datadorecebimento;
		this.andar = andar;
		this.categoria = categoria;
		this.cnpjfilial = cnpjfilial;
		this.codigodobanco = codigodobanco;
		this.contacorrente = contacorrente;
		this.digitoverificadorconta = digitoverificadorconta;
		this.nomefantasia = nomefantasia;
		this.razaosocial = razaosocial;
		this.suc = suc;
		this.taxaonstore = taxaonstore;
		this.nomeresponsavel = nomeresponsavel;
		this.telefoneresponsavel = telefoneresponsavel;
		this.emailresponsavel = emailresponsavel;
		this.aceite = aceite;
	}

	public UserSession() {}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public boolean isIsadmin() {
		return isadmin;
	}

	public void setIsadmin(boolean isadmin) {
		this.isadmin = isadmin;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public boolean getAntecipacao() {
		return antecipacao;
	}

	public void setAntecipacao(boolean antecipacao) {
		this.antecipacao = antecipacao;
	}

	public String getDatadorecebimento() {
		return datadorecebimento;
	}

	public void setDatadorecebimento(String datadorecebimento) {
		this.datadorecebimento = datadorecebimento;
	}

	public String getAndar() {
		return andar;
	}

	public void setAndar(String andar) {
		this.andar = andar;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getCnpjfilial() {
		return cnpjfilial;
	}

	public void setCnpjfilial(String cnpjfilial) {
		this.cnpjfilial = cnpjfilial;
	}

	public String getCodigodobanco() {
		return codigodobanco;
	}

	public void setCodigodobanco(String codigodobanco) {
		this.codigodobanco = codigodobanco;
	}

	public String getContacorrente() {
		return contacorrente;
	}

	public void setContacorrente(String contacorrente) {
		this.contacorrente = contacorrente;
	}

	public String getDigitoverificadorconta() {
		return digitoverificadorconta;
	}

	public void setDigitoverificadorconta(String digitoverificadorconta) {
		this.digitoverificadorconta = digitoverificadorconta;
	}

	public String getNomefantasia() {
		return nomefantasia;
	}

	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}

	public String getRazaosocial() {
		return razaosocial;
	}

	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}

	public String getSuc() {
		return suc;
	}

	public void setSuc(String suc) {
		this.suc = suc;
	}

	public double getTaxaonstore() {
		return taxaonstore;
	}

	public void setTaxaonstore(double taxaonstore) {
		this.taxaonstore = taxaonstore;
	}

	public String getNomeresponsavel() {
		return nomeresponsavel;
	}

	public void setNomeresponsavel(String nomeresponsavel) {
		this.nomeresponsavel = nomeresponsavel;
	}

	public String getTelefoneresponsavel() {
		return telefoneresponsavel;
	}

	public void setTelefoneresponsavel(String telefoneresponsavel) {
		this.telefoneresponsavel = telefoneresponsavel;
	}

	public String getEmailresponsavel() {
		return emailresponsavel;
	}

	public void setEmailresponsavel(String emailresponsavel) {
		this.emailresponsavel = emailresponsavel;
	}

	public boolean getAceite() {
		return aceite;
	}

	public void setAceite(boolean aceite) {
		this.aceite = aceite;
	}
}
