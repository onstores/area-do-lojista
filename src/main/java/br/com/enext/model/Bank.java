package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

/*
    Classe modelo do banco para ser utilizado nos possíveis bancos (Ex: Bradesco 237, Santander 033...).
*/

@Getter
@Setter
public class Bank {
    private String code;
    private String name;
}
