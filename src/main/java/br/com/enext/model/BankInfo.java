package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

/*
    Classe modelo do banco para ser utilizado pela classe Seller.
*/

@Getter
@Setter
public class BankInfo {
	public String banco;
	public String agencia;
	public String contaCorrente;
	public String contaDV;
	public String comprovanteBancario;

}