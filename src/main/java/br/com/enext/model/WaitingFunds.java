package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WaitingFunds {
    private double amount;
}
