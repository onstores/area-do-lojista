package br.com.enext.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Field;

/*
    Classe modelo de informações sobre um item.
*/

@Getter
@Setter
public class Item {
	@Field("id")
	private String item_id;
	private String description;
	private String variation;
	private String quantity;
	private String quantity_delivered;
	private String unit_price;
	private String total_price;
	private String salesperson;
	private String status;
	private String cancellation_date;
	private String deliver_date;
}
