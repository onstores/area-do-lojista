package br.com.enext.resources;

import br.com.enext.model.*;
import br.com.enext.services.ShoppingsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
    Classe responsável por devolver uma lista de shoppings de acordo com o definido na shoppingsService class
    atraves do endpoint /shopping-options.
*/


@Slf4j
@RestController
@RequestMapping("/shopping-options")
public class ShoppingOptionsController {
    @Autowired
    ShoppingsService shoppingsService;

    @GetMapping(path= {"", "/"})
    public ResponseEntity getShoppingOptions(){
        ShoppingOptionsDto shoppingsDto = new ShoppingOptionsDto();
        shoppingsDto.setContent(shoppingsService.getSelectOptionsDto());
        return ResponseEntity.ok(shoppingsDto);
    }
}
