package br.com.enext.resources;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import br.com.enext.enums.RoleType;
import br.com.enext.model.SellerToDelete;
import br.com.enext.repositories.SellerRepository;
import br.com.enext.services.SellerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import br.com.enext.model.Seller;

import java.util.Optional;

/*
	Classe responsável por salvar e deletar o seller no repositorio Seller no MongoDB.
*/

@RestController
@RequestMapping("/resource/seller")
@Slf4j
public class SellerResource {
	@Autowired
	SellerRepository sellerRepository;
	@Autowired
	SellerService sellerService;
	
	@PostMapping(path={"","/"})
	public ResponseEntity<Seller> save(HttpServletRequest req, HttpServletResponse resp, @Valid @RequestBody Seller sellerInMasterData){
		// Criando o seller ADMIN_LOJISTA
		Seller adminPersistedSeller = sellerService.saveSeller(sellerInMasterData, sellerInMasterData.getShoppingId());
		
		if(adminPersistedSeller == null) {
			return new ResponseEntity <> (HttpStatus.BAD_REQUEST);
		}

		// Criando o seller FUNCIONARIO_LOJISTA
		sellerInMasterData.setId(sellerInMasterData.getCnpj() + "_" + RoleType.FUNCIONARIO_LOJISTA);
		sellerInMasterData.setRoleType(RoleType.FUNCIONARIO_LOJISTA);
		Seller staffPersistedSeller = sellerService.saveSeller(sellerInMasterData, sellerInMasterData.getShoppingId());

		if(staffPersistedSeller != null) {
			return new ResponseEntity <> (HttpStatus.OK);
		}
		else {
			return new ResponseEntity <> (HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/delete")
	public ResponseEntity<SellerToDelete> delete(@Valid @RequestBody SellerToDelete sellerInMasterData){



		Seller adminSellerToDelete = sellerRepository.findByRoleTypeAndCnpj(RoleType.ADMIN_LOJISTA, sellerInMasterData.getCnpj());

		if(adminSellerToDelete != null) {
			log.debug("Deletando admin Seller");
			sellerRepository.delete(adminSellerToDelete);
		}
		else {
			return new ResponseEntity ("Seller não encontrado",HttpStatus.BAD_REQUEST);
		}

		Seller FuncionarioSellerToDelete = sellerRepository.findByRoleTypeAndCnpj(RoleType.FUNCIONARIO_LOJISTA, sellerInMasterData.getCnpj());

		if(FuncionarioSellerToDelete != null) {
			log.debug("Deletando Funcionario Seller");
			sellerRepository.delete(FuncionarioSellerToDelete);
			return new ResponseEntity <>(HttpStatus.OK);
		}
		else {
			return new ResponseEntity ("Seller não encontrado",HttpStatus.OK);
		}

	}

}
