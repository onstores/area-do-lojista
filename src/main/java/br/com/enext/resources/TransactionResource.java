package br.com.enext.resources;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.enext.model.Seller;
import br.com.enext.services.SellerService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpGet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.enext.model.Transaction;
import br.com.enext.utils.APICall;
import br.com.enext.utils.Enviroment;

/*
    Classe responsavel por efetuar chamada do enpoint de /transaction da Napp.
*/

@RestController
@Slf4j
@RequestMapping("/transaction")
public class TransactionResource {
	@Autowired
	SellerService sellerService;

	@GetMapping(path= {"/{id}"})
	public ResponseEntity<Transaction> getTransaction(HttpServletRequest req, HttpServletResponse res, 
			@PathVariable("id") String id){
		log.debug("getTransaction chamado");
		
		try {
			Seller s = sellerService.getSellerFromFromSecurityContext();

			StringBuilder apiUrl = new StringBuilder();
			apiUrl.append(Enviroment.getEnv("TRANSACTION_ENDPOINT"));
			apiUrl.append("/");
			apiUrl.append(s.getIdVtex());
			apiUrl.append("/");
			apiUrl.append(id);

			log.info(apiUrl.toString());

			APICall call = new APICall(apiUrl.toString(), s.getToken(),HttpGet.class);
			try {
				return call.get(Transaction.class);
			} catch (APICall.ApiCallErrorException e) {
				return e.getResponseEntity();
			}
		} catch (SellerService.SellerNotFoundException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
	}
}
