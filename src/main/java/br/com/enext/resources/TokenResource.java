package br.com.enext.resources;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.enext.services.SellerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.enext.model.RequestMessage;
import br.com.enext.model.Seller;
import br.com.enext.model.TokenData;
import br.com.enext.model.Session;
import br.com.enext.repositories.SellerRepository;
import br.com.enext.repositories.SessionRepository;

/*
	Classe responsável por retornar o Token de sessão de um seller a partir do endpoint /resource/token.
*/

@RestController
@Slf4j
public class TokenResource {
	@Autowired
	private SessionRepository repository;

	@Autowired
	private SellerRepository sellerRepository;
	@Autowired
	SellerService sellerService;

	// TODO: Adicionar se o seller está ativo e se os filhos também estão ativos
	@GetMapping({ "/resource/token"})
	private ResponseEntity<Object> getTokenData(HttpServletRequest req, HttpServletResponse res, @RequestParam String token) {
		log.debug("getTokenData chamado");

		Session session = repository.findBySessionToken(token);
		if (session != null) {
			Seller seller = sellerRepository.findByIdAndEnabled(session.getSellerId(), true);

			if (seller != null) {
				TokenData tokenData = new TokenData();

				log.debug(String.format("seller: %s", seller));

				tokenData.setSeller_env_id(seller.getIdVtex());
				tokenData.setRole(seller.getRoleType());

				switch (seller.getRoleType()) {
					case ADMIN_ONSTORES:
						break;
					case ADMIN_SHOPPING:
						tokenData.setShoppingId(seller.getShoppingId());
						break;
					case ADMIN_LOJISTA:
						tokenData.setSellerEnvIds(sellerService.getAllDependentSellerEnvIds(seller.getCnpj()));
						break;
					case FUNCIONARIO_LOJISTA:
						tokenData.setSellerEnvIds(sellerService.getAllDependentSellerEnvIds(seller.getCnpj()));
						break;
				}

				return new ResponseEntity<>(tokenData, HttpStatus.OK);
			}
		}
		RequestMessage m = new RequestMessage();
		m.setMessage("user not found or token expired");
		return new ResponseEntity<>(m, HttpStatus.NOT_FOUND);
	}
}
