package br.com.enext.resources;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import br.com.enext.enums.RoleType;
import org.apache.http.client.methods.HttpGet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import br.com.enext.model.OrderDetail;
import br.com.enext.model.PageOfOrders;
import br.com.enext.model.Seller;
import br.com.enext.services.SellerService;
import br.com.enext.utils.APICall;
import br.com.enext.utils.DebugPrettyPrinter;
import br.com.enext.utils.Enviroment;
import lombok.extern.slf4j.Slf4j;

/*
	Classe responsável pela gerencia dos endpoints de Orders da Napp.
*/

@RestController
@RequestMapping("/order")
@Slf4j
public class OrderResource {
	@Autowired
	SellerService sellerService;

	@GetMapping(path= {"", "/"})
	public ResponseEntity getOrders(HttpServletRequest req, HttpServletResponse res,
									@RequestParam(required = false) String start_date,
									@RequestParam(required = false) String end_date,
									@RequestParam(required = false) String shopping,
									@RequestParam(required = false) String seller,
									@RequestParam(required = false) String status,
									@RequestParam(required = false) Long limit,
									@RequestParam(required = false) Long offset
	){
		try {
			log.debug("getOrders chamado");
			DebugPrettyPrinter.log(req);

			Seller s = sellerService.getSellerFromFromSecurityContext();

			StringBuilder apiUrl = new StringBuilder();
			apiUrl.append(Enviroment.getEnv("ORDER_ENDPOINT"));
			apiUrl.append("?");


			if(start_date != null) {
				apiUrl.append("&");
				apiUrl.append("start_date=");
				apiUrl.append(start_date);
			}

			if(end_date != null) {
				apiUrl.append("&");
				apiUrl.append("end_date=");
				apiUrl.append(end_date);
			}

			if(shopping != null) {
				apiUrl.append("&");
				apiUrl.append("shopping=");
				if(s.getRoleType()==RoleType.ADMIN_SHOPPING || s.getRoleType()==RoleType.ADMIN_ONSTORES){
					apiUrl.append(shopping);
				}
			}

			if(s.getRoleType() == RoleType.FUNCIONARIO_LOJISTA){
				apiUrl.append("&");
				apiUrl.append("seller_env_id=");
				apiUrl.append(s.getIdVtex());
			}
			else {
				apiUrl.append("&");
				apiUrl.append("seller_env_id=");
				apiUrl.append(seller);
			}

			if(limit != null) {
				apiUrl.append("&");
				apiUrl.append("limit=");
				apiUrl.append(limit);
			}

			if(offset != null) {
				apiUrl.append("&");
				apiUrl.append("offset=");
				apiUrl.append(offset);
			}

			if(status != null) {
				apiUrl.append("&");
				apiUrl.append("status=");
				apiUrl.append(status);
			}

			APICall call = new APICall(apiUrl.toString(), s.getToken(), HttpGet.class);

			try {
				return call.get(PageOfOrders.class);
			} catch (APICall.ApiCallErrorException e) {
				return e.getResponseEntity();
			}
		} catch (SellerService.SellerNotFoundException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<OrderDetail> getOrderById(HttpServletRequest req, HttpServletResponse res, @PathVariable("id") String id){
		try {
			log.debug("getOrderById chamado");
			DebugPrettyPrinter.log(req);

			Seller s = sellerService.getSellerFromFromSecurityContext();

			StringBuilder apiUrl = new StringBuilder();
			apiUrl.append(Enviroment.getEnv("ORDER_ENDPOINT"));
			apiUrl.append("/");
			apiUrl.append(s.getIdVtex());
			apiUrl.append("/");
			apiUrl.append(id);

			APICall call = new APICall(apiUrl.toString(), s.getToken(), HttpGet.class);

			try {
				return call.get(OrderDetail.class);
			} catch (APICall.ApiCallErrorException e) {
				return e.getResponseEntity();
			}
		} catch (SellerService.SellerNotFoundException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
	}

	@GetMapping("/by-token")
	public ResponseEntity<OrderDetail> getOrderByToken(
			HttpServletRequest req,
			HttpServletResponse res,
			@RequestParam String token){
		try {
			log.debug("getOrderByToken chamado");
			log.debug(String.format("token: %s", token));
			DebugPrettyPrinter.log(req);

			Seller s = sellerService.getSellerFromFromSecurityContext();

			StringBuilder apiUrl = new StringBuilder();
			apiUrl.append(Enviroment.getEnv("ORDER_TOKEN_ENDPOINT"));
			apiUrl.append("token/");
			apiUrl.append(s.getIdVtex());
			apiUrl.append("/");
			apiUrl.append(token);
			APICall call = new APICall(apiUrl.toString(), s.getToken(), HttpGet.class);

			try {
				ResponseEntity<OrderDetail> resp = call.get(OrderDetail.class);
				log.debug(String.format("resp.getBody().toString(): %s", resp.getBody().toString()));
				return resp;
			} catch (APICall.ApiCallErrorException e) {
				return e.getResponseEntity();
			}
		} catch (SellerService.SellerNotFoundException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
	}
}
