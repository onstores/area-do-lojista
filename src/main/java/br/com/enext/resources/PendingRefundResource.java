package br.com.enext.resources;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.methods.HttpGet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;

import br.com.enext.model.Bank;
import br.com.enext.model.Seller;
import br.com.enext.model.Transaction;
import br.com.enext.services.SellerService;
import br.com.enext.services.SellerService.SellerNotFoundException;
import br.com.enext.utils.APICall;
import br.com.enext.utils.Enviroment;
import lombok.extern.slf4j.Slf4j;

/*
	Classe responsável por chamar o endpoint /api/pending_refund da Napp.

	OBS: Não está sendo utilizado.
*/

@RestController
@RequestMapping("/pending-refund")
@Slf4j
public class PendingRefundResource {
	
	@Autowired
	SellerService sellerService;
	
	@GetMapping({"/",""})
	public ResponseEntity<List<PendingRefundResource>> getPendingRefund(HttpServletRequest req, HttpServletResponse res) {
		log.debug("getPendingRefund chamado");
		try {
			Seller seller = sellerService.getSellerFromFromSecurityContext();
			
			StringBuilder apiUrl = new StringBuilder();
			apiUrl.append(Enviroment.getEnv("PENDING_REFUND_ENDPOINT"));
			apiUrl.append("?");
			apiUrl.append("seller_env_id=");
			apiUrl.append(seller.getIdVtex());
			
			APICall call = new APICall(apiUrl.toString(), seller.getToken(),HttpGet.class);
			try {
				return call.get(PendingRefundResource.class, new TypeToken<List<Bank>>(){}.getType());
			} catch (APICall.ApiCallErrorException e) {
				return e.getResponseEntity();
			}
		} catch (SellerService.SellerNotFoundException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}		
	}

}
