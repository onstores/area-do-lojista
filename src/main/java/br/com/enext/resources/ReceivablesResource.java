package br.com.enext.resources;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.enext.controllers.DownloadController;
import br.com.enext.model.Balance;
import br.com.enext.services.SellerService;
import br.com.enext.utils.Enviroment;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpGet;
import org.bouncycastle.jcajce.provider.asymmetric.ec.KeyFactorySpi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.enext.model.Receivables;
import br.com.enext.model.Seller;
import br.com.enext.utils.APICall;

import java.io.IOException;
import java.util.List;

/*
	Classe responsável por efetuar a chamada do endpoint /api/receivables da Napp.
*/

@RestController
@Slf4j
@RequestMapping("/receivables")
public class ReceivablesResource {
	@Autowired
	SellerService sellerService;
	@Autowired
	DownloadController downloadController;

	@GetMapping(path = { "/", "" })
	public ResponseEntity<Receivables> getReceivables(
			HttpServletRequest req,
			HttpServletResponse res,
			@RequestParam(name="seller_env_id", required = false) String seller_env_id,
			@RequestParam(name="start_date", required=false) String start_date,
			@RequestParam(name="end_date", required=false) String end_date,
			@RequestParam(name="payment_start_date", required=false) String payment_start_date,
			@RequestParam(name="payment_end_date", required=false) String payment_end_date,
			@RequestParam(name="payment_date", required=false) String payment_date,
			@RequestParam(name="shopping", required=false) String shopping,
			@RequestParam(name="seller", required=false) String seller,
			@RequestParam(name="status", required=false) String status,
			@RequestParam(name="limit", required=false) String limit,
			@RequestParam(name="offset", required=false) String offset,
			@RequestParam(name="output") String output

	)throws IOException {
		try {

			Seller s = sellerService.getSellerFromFromSecurityContext();
			StringBuilder apiUrl = new StringBuilder();

			String url = Enviroment.getEnv("RECEIVABLES_URL");

			apiUrl.append(url);
			apiUrl.append("?");

			if (start_date != null) {
				apiUrl.append("&start_date=");
				apiUrl.append(start_date);
			}

			if (end_date != null) {
				apiUrl.append("&end_date=");
				apiUrl.append(end_date);
			}

			if (payment_start_date != null) {
				apiUrl.append("&payment_start_date=");
				apiUrl.append(payment_start_date);
			}

			if (payment_end_date != null) {
				apiUrl.append("&payment_end_date=");
				apiUrl.append(payment_end_date);
			}

			if (payment_date != null) {
				apiUrl.append("&payment_date=");
				apiUrl.append(payment_date);
			}

			if(shopping != null) {
				apiUrl.append("&");
				apiUrl.append("shopping=");
				apiUrl.append(shopping);
			}

			if(seller != null) {
				apiUrl.append("&");
				apiUrl.append("seller=");
				apiUrl.append(seller);
			}

			if(status != null) {
				apiUrl.append("&");
				apiUrl.append("status=");
				apiUrl.append(status);
			}

			if(limit != null) {
				apiUrl.append("&");
				apiUrl.append("limit=");
				apiUrl.append(limit);
			}

			if(offset != null) {
				apiUrl.append("&");
				apiUrl.append("offset=");
				apiUrl.append(offset);
			}

			apiUrl.append("&");
			apiUrl.append("output=");
			apiUrl.append(output);

			if(output.equals("xlsx")){
				String s3Url = downloadController.generateReceivableS3Link(apiUrl.toString(), s, start_date, end_date, output);
				res.sendRedirect(s3Url);
				return null;
			}
			else{

				APICall call = new APICall(apiUrl.toString(), s.getToken(), HttpGet.class);

				log.info(apiUrl.toString());

				try {
					System.out.println("Fazendo a chamada.");
					ResponseEntity<Receivables> receivablesToReturn = call.get(Receivables.class);

					StringBuilder apiBalanceUrl = new StringBuilder();
					apiBalanceUrl.append(Enviroment.getEnv("BALANCE_ENDPOINT"));
					if(seller != null)
					{
						apiBalanceUrl.append(seller);

					}else if(shopping != null){
						apiBalanceUrl.append(shopping);

					}
					else {
						apiBalanceUrl.append(s.getIdVtex());
					}
					apiBalanceUrl.append("/balances");

					APICall balanceCall = new APICall(apiBalanceUrl.toString(), s.getToken(), HttpGet.class);

					try{
						Balance balanceResponse = balanceCall.get(Balance.class, new TypeToken<List<Balance>>(){}.getType()).getBody().get(0);
						receivablesToReturn.getBody().setTotal_received(balanceResponse.getAvailable().getAmount()/100);
						receivablesToReturn.getBody().setTotal_to_receive(balanceResponse.getWaiting_funds().getAmount()/100);
					}
					catch (Exception e){
						receivablesToReturn.getBody().setTotal_received(0.00);
						receivablesToReturn.getBody().setTotal_to_receive(0.00);
					}

					return receivablesToReturn;
				} catch (APICall.ApiCallErrorException e) {
					return e.getResponseEntity();
				}
			}

		} catch (SellerService.SellerNotFoundException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
	}
}
