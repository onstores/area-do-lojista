package br.com.enext.resources;


import br.com.enext.model.Bank;
import br.com.enext.utils.APICall;
import br.com.enext.utils.DebugPrettyPrinter;
import br.com.enext.utils.Enviroment;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpGet;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import com.google.gson.reflect.TypeToken;

/*
    Classe responsavel por efetuar chamada do enpoint Banks da Napp.
*/

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/banks")
@Slf4j
public class BanksController {
    @GetMapping(path = { "/", "" })
    public ResponseEntity<List<Bank>> getBanks(
            HttpServletRequest req,
            HttpServletResponse res
    ) {
        log.debug("getBanks chamado");
        DebugPrettyPrinter.log(req);

        StringBuilder apiUrl = new StringBuilder();
        apiUrl.append(Enviroment.getEnv("BANKS_ENDPOINT"));

        APICall call = new APICall(apiUrl.toString(), HttpGet.class);

        try {
            return call.get(Bank.class, new TypeToken<List<Bank>>(){}.getType());
        } catch (APICall.ApiCallErrorException e) {
            return e.getResponseEntity();
        }
    }
}
