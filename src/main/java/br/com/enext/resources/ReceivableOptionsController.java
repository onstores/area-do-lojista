package br.com.enext.resources;

import br.com.enext.model.ReceivableStatusOptionsDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
    Classe responsável por criar uma lista de possíveis status para os recebíveis.
*/

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/receivable-options")
@Slf4j
public class ReceivableOptionsController {
    @GetMapping(path = { "/", "" })
    public ResponseEntity getReceivableOptions(
            HttpServletRequest req,
            HttpServletResponse res
    ) {
        ReceivableStatusOptionsDto receivableStatusOptionsDto = new ReceivableStatusOptionsDto();

        receivableStatusOptionsDto.add("waiting_funds", "A Receber");
        receivableStatusOptionsDto.add("paid", "Pago");
        receivableStatusOptionsDto.add("prepaid", "Transferência");
        receivableStatusOptionsDto.add("refund", "Estorno");

        return ResponseEntity.ok(receivableStatusOptionsDto);
    }
}
