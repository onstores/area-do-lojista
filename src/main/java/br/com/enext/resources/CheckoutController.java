package br.com.enext.resources;

import br.com.enext.model.*;
import br.com.enext.services.SellerService;
import br.com.enext.utils.APICall;
import br.com.enext.utils.Enviroment;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpPut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/*
    Classe responsavel por efetuar chamada do enpoint de Checkout da Napp.
*/

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/checkout")
@Slf4j
public class CheckoutController {
    @Autowired
    SellerService sellerService;

    // TODO: Alinhar com o front como esse meétodo será chamado
    @PutMapping(path = { "", "/" })
    @CrossOrigin(origins = "*")
    public ResponseEntity checkout(@RequestBody CheckoutBody checkout) {
        try {
            log.debug("checkout chamado");
            log.debug(String.format("checkout: %s", checkout.toString()));

            Seller seller = sellerService.getSellerFromFromSecurityContext();

            StringBuilder apiUrl = new StringBuilder();
            apiUrl.append(Enviroment.getEnv("CHECKOUT_ENDPOINT"));

            Map<String, String> headers = new HashMap<String, String>();
            headers.put("Content-Type", "application/json");

//            checkout.setSeller_env_id(seller.getIdVtex());
            APICall call = new APICall(apiUrl.toString(), seller.getToken(), HttpPut.class);

            try {
               ResponseEntity<CheckoutResponse> responseEntity = call.put(CheckoutResponse.class, CheckoutBody.class, checkout, headers);
               System.out.println("RESPONSE: ====> " + responseEntity.getBody());
               return new ResponseEntity<>(responseEntity.getBody().getFullMessage(), responseEntity.getStatusCode());
            }
            catch(APICall.ApiCallErrorException e) {
                try {
                    Gson gson = new Gson();
                    CheckoutError checkoutError = gson.fromJson(e.getRespBody(), CheckoutError.class);
                    return new ResponseEntity<>(checkoutError.getDetail(), e.getHttpStatus());
                } catch(Exception e2) {
                    return e.getResponseEntity();
                }
            }

//            return new ResponseEntity<>("Faltando campo 'reason'", HttpStatus.BAD_REQUEST);

        } catch (SellerService.SellerNotFoundException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
