package br.com.enext.resources;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.enext.model.Seller;
import br.com.enext.repositories.SellerRepository;
import br.com.enext.services.SellerService;
import br.com.enext.services.SellerService.SellerNotFoundException;
import lombok.extern.slf4j.Slf4j;

/*
    Classe responsavel por devolver os dados de um seller pela chamada do enpoint /loja/configurations.
*/

@RestController
@RequestMapping("/loja/configurations")
@Slf4j
public class ConfigurationsResource {
	@Autowired
	SellerService sellerService;
	
	@Autowired
	SellerRepository sellerRepository;
	
	@Autowired
	ModelMapper modelMapper;
	
	@GetMapping(path = {"","/"})
	public ResponseEntity <Seller> getConfigurations(HttpServletRequest req, HttpServletResponse res){
		try{
			log.debug("getConfigurations chamado");
			Seller s = sellerService.getSellerFromFromSecurityContext();

			Seller sellerToReturn = sellerRepository.findByRoleTypeAndCnpj(s.getRoleType(),s.getCnpj());

			log.debug(sellerToReturn.toString());
			return new ResponseEntity<>(sellerToReturn, HttpStatus.OK);

		}
		catch (SellerNotFoundException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
	}

}
