package br.com.enext.resources;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.enext.model.Seller;
import br.com.enext.repositories.SellerRepository;
import br.com.enext.services.SellerService;
import br.com.enext.services.SellerService.SellerNotFoundException;
import lombok.extern.slf4j.Slf4j;

/*
	Classe responsável por guardar no MongoDB a ultima hora em que o seller visualizou as notificações
	a partir da chamada do endpoint /api/saw-notifications.
*/

@RestController
@RequestMapping("/api/saw-notifications")
@Slf4j
public class SawNotificationsController {
	
	@Autowired
	private SellerService sellerService;
	@Autowired
	private SellerRepository sellerRepository;
	
	@GetMapping({"","/"})
	public ResponseEntity sawNotifications(HttpServletRequest req, HttpServletResponse resp) {
		
		log.debug("sawNotifications chamado");
		
		try {
			Seller loggedSeller = sellerService.getSellerFromFromSecurityContext();
						
			Seller sellerToSave = sellerRepository.findByRoleTypeAndCnpj(loggedSeller.getRoleType(),
					loggedSeller.getCnpj());
			
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			
			sellerToSave.setSawNotificationsAt(timestamp.toString());
			log.debug(timestamp.toString());
			
			Seller persistedSeller = sellerService.saveSeller(sellerToSave, sellerToSave.getIdVtex());

			if (persistedSeller == null) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			} 
			else {
				return ResponseEntity.status(HttpStatus.OK).build();
			}
		} catch (SellerNotFoundException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

}
