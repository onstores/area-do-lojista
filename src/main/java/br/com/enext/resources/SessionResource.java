package br.com.enext.resources;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.enext.model.Session;
import br.com.enext.repositories.SellerRepository;
import br.com.enext.repositories.SessionRepository;
import lombok.extern.slf4j.Slf4j;

/*
	Classe responsável por retornar as sessions ativas na Tabela Session no MongoDB
	através do endpoint /resource/sessions.
*/

@RestController
@Slf4j
public class SessionResource {
	
	@Autowired
    SessionRepository sessionRepository;
	
	@GetMapping({ "/resource/sessions"})
	private ResponseEntity<Object> getActiveSessions(HttpServletRequest req, HttpServletResponse res){
		log.debug("getActiveSessions chamado");
		
		return new ResponseEntity<>(sessionRepository.findAll(), HttpStatus.OK);
	}

}
