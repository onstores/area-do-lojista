package br.com.enext.resources;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import br.com.enext.enums.RoleType;
import br.com.enext.model.Seller;
import br.com.enext.model.ApiCallError;
import br.com.enext.repositories.SellerRepository;
import br.com.enext.services.SellerService;
import br.com.enext.services.VtexKeysService;
import br.com.enext.utils.StringUtils;
import com.google.gson.Gson;
import com.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import br.com.enext.model.MasterDataRegister;
import br.com.enext.model.MasterDataResponse;
import br.com.enext.utils.APICall;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/*
    Classe responsavel por efetuar a comunicação com o Masterdata e funções afim.
*/

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/masterdata")
@Slf4j
public class MasterdataController {

	private static String PREFFIXENDPOINT = "https://";
	private static String SUFFIXENDPOINT = ".vtexcommercestable.com.br/api/dataentities/AL/documents";
	private static String CAMPOCOMPROVANTE = "comprovantebancario";
	private static String ATTACHMENTS = "/attachments";

	@Autowired
    SellerRepository sellerRepository;

	@Autowired
	VtexKeysService vtexKeysService;

	@Autowired
	SellerService sellerService;

	@PostMapping(path = { "", "/" })
	@CrossOrigin(origins = "*")
	public ResponseEntity<MasterDataResponse> register(@Valid MasterDataRegister masterDataRegister,
			@RequestParam(name="file", required=true) MultipartFile file) {
		log.debug("register chamado");
		log.debug(String.format("masterDataRegister: %s", masterDataRegister));

		ResponseEntity<MasterDataResponse> respAdminSeller = null;

		masterDataRegister.setTermwasaccepted(true);

		int nTriesAdmin = 0;
		boolean addedAdmin = false;
		while(nTriesAdmin < 10) {
			respAdminSeller = registerOnMasterData(masterDataRegister, RoleType.ADMIN_LOJISTA, file, true);

			if(respAdminSeller.getStatusCode() == HttpStatus.BAD_REQUEST) {
				return respAdminSeller;
			} else if(respAdminSeller.getStatusCode() == HttpStatus.FORBIDDEN) {
				return respAdminSeller;
			} else if(respAdminSeller.getStatusCode() == HttpStatus.UNAUTHORIZED) {
				return respAdminSeller;
			}
			else if (respAdminSeller.getStatusCode().is2xxSuccessful()){ // Retorna OK
				addedAdmin = true;
				break;
			}
			else {
//				// TODO: Remover a linha abaixo quando o front enviar o nome do banco e o código do banco
//				masterDataRegister.setBanco(masterDataRegister.getCodigodobanco().toString());
				nTriesAdmin++;				
			}			
		}
		
		if(!addedAdmin) {
			log.debug(String.format("Retornando HttpStatus.BAD_REQUEST após todas as tentativas de adição no masterdata com role ADMIN_SELLER"));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

		return ResponseEntity.ok().build();
	}

	private ResponseEntity registerOnMasterData(MasterDataRegister masterDataRegister, RoleType roleType, MultipartFile file, boolean enableExistentCnpjValidation) {
		String sellerId = StringUtils.getCleanCnpj(masterDataRegister.getCnpjfilial()) + "_" + roleType;
		String adminSellerId = StringUtils.getCleanCnpj(masterDataRegister.getCnpjfilial()) + "_" + RoleType.ADMIN_LOJISTA;
		String staffSellerId = StringUtils.getCleanCnpj(masterDataRegister.getCnpjfilial()) + "_" + RoleType.FUNCIONARIO_LOJISTA;

		log.debug(String.format("sellerId: %s", sellerId));

		masterDataRegister.setRoletype(roleType);
		masterDataRegister.setId(sellerId);
		masterDataRegister.setCadastroaprovado(false);
//		// TODO: Remover as linhas abaixo quando o front enviar o nome do banco e o código do banco
//		log.debug(String.format("masterDataRegister.getBanco(): %s", masterDataRegister.getBanco()));
//		masterDataRegister.setCodigodobanco(Integer.parseInt(masterDataRegister.getBanco()));
//		masterDataRegister.setBanco(getBankName(Integer.parseInt(masterDataRegister.getBanco())));

		if(enableExistentCnpjValidation) {
			// Caso o seller já exista em nosso repositório mongodb, será enviado:
			// - idvtex
			// - cadastroaprovado
			Optional<Seller> sellerOptional = sellerRepository.findById(sellerId);
			if(sellerOptional.isPresent()) {
				log.debug("Já existe um usuário no banco de dados com o id em questão");
				try {
					Seller logedSeller = sellerService.getSellerFromFromSecurityContext();

					log.debug(String.format("sellerId: %s", sellerId));
					log.debug(String.format("O usuário logado é o %s", logedSeller));
					if(!logedSeller.getId().equals(adminSellerId) && !logedSeller.getId().equals(staffSellerId)) {
						log.debug("Retornando HttpStatus.FORBIDDEN pois o cnpj em questão não pertence ao usuário logado");
						return new ResponseEntity<>(ApiCallError.builder().Message("Cnpj já cadastrado e ele não pertence ao usuário logado").build(), HttpStatus.FORBIDDEN);
					}
				} catch (SellerService.SellerNotFoundException e) {
					log.debug("Para alterar um cnpj previamente cadastrado você deve estar logado com o usuário desse cnpj. Retornando HttpStatus.UNAUTHORIZED");
					return new ResponseEntity<>(ApiCallError.builder().Message("Para alterar um cnpj previamente cadastrado, você deve estar logado com o usuário desse cnpj").build(), HttpStatus.UNAUTHORIZED);
				}

				Seller seller = sellerOptional.get();
				masterDataRegister.setCnpjfilial(seller.getCnpj());
				masterDataRegister.setDatarecebimento(seller.getDataRecebimento());
				masterDataRegister.setAntecipacao(seller.isAntecipacao());
				masterDataRegister.setTaxaonstore(seller.getTaxaOnstore());
				masterDataRegister.setShoppingid(seller.getShoppingId());
				masterDataRegister.setIdvtex(seller.getIdVtex());
			}
		}

		APICall call = new APICall(getEndpointStringBuilder(vtexKeysService.getUrl(masterDataRegister.getShoppingid())).toString(), HttpPut.class);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("X-VTEX-API-AppKey", vtexKeysService.getAppKey(masterDataRegister.getShoppingid()));
		headers.put("X-VTEX-API-AppToken", vtexKeysService.getAppToken(masterDataRegister.getShoppingid()));
		ResponseEntity<MasterDataResponse> resp = null;
		try {
			resp = call.put(MasterDataResponse.class, MasterDataRegister.class, masterDataRegister,
					headers);
		} catch (APICall.ApiCallErrorException e) {
			Gson gson = new Gson();
			ApiCallError apiCallError = gson.fromJson(e.getRespBody(), ApiCallError.class);
			return new ResponseEntity<>(apiCallError, e.getHttpStatus());
		}

		if (file != null) {
			log.info("FILE não nula");
			log.info(String.format("file.getOriginalFilename(): %s", file.getOriginalFilename()));
			log.info(String.format("file.getSize(): %s", file.getSize()));

			StringBuilder uploadURL = new StringBuilder();
			uploadURL.append(getEndpointStringBuilder(masterDataRegister.getShoppingid()).toString());
			uploadURL.append("/");
			uploadURL.append(masterDataRegister.getId());
			uploadURL.append("/");
			uploadURL.append(CAMPOCOMPROVANTE);
			uploadURL.append(ATTACHMENTS);
			log.info(String.format("uploadURL.toString(): %s", uploadURL.toString()));
			call = new APICall(uploadURL.toString(), HttpPost.class);
			int upload = call.post(file, CAMPOCOMPROVANTE);
			log.info(String.format("uploadStatusCode: %s", String.valueOf(upload)));
		}else {
			log.info("FILE nula");
		}

		return resp;
	}

	public StringBuilder getEndpointStringBuilder(String shoppingId) {
		StringBuilder endpoint = new StringBuilder();
		endpoint.append(PREFFIXENDPOINT);
		endpoint.append(shoppingId);
		endpoint.append(SUFFIXENDPOINT);
		return endpoint;
	}

//	// Alinhar com o front que esse parâmetro é setado no backend
//	private String getBankName(int bankCode) {
//		switch(bankCode) {
//			case 1:
//				return "Banco do Brasil";
//			case 237:
//				return "Bradesco";
//			case 104:
//				return "Caixa Econômica Federal";
//			case 341:
//				return "Itaú Unibanco";
//			case 33:
//				return "Santander";
//			default:
//				return "";
//		}
//	}

	// TODO: Comentar ao colocar em produção
	@GetMapping(path = {"/generate-users" })
	public ResponseEntity<String> generateUsers(HttpServletRequest req, HttpServletResponse res) throws IOException {
		log.debug("Generate users called");

		boolean firstLine = true;

		int rowCounter = 0;

		List<List<String>> records = new ArrayList<List<String>>();
		try (CSVReader csvReader = new CSVReader(new FileReader("/home/joao/Downloads/users.csv"))) {
			String[] values = null;
			while ((values = csvReader.readNext()) != null) {
				log.debug(values.toString());
//				records.add(Arrays.asList(values));
				if(firstLine) {
					firstLine = false;
				}
				else {
					rowCounter++;

					log.debug(String.format("rowCounter: %s", rowCounter));

					// Comentar para enviar todos os sellers
//					if(rowCounter == 20) {
//						break;
//					}

					List<String> row = Arrays.asList(values);

					if(row.get(1).equals("Admin_Lojista")) {
						MasterDataRegister masterDataRegister = new MasterDataRegister();

//					for(String val : row) {
//						log.debug(val);
//					}

						masterDataRegister.setNomeresponsavel(row.get(0));
						masterDataRegister.setEmailresponsavel(row.get(2));
						masterDataRegister.setTelefoneresponsavel(row.get(3));
						masterDataRegister.setNomefantasia(row.get(4));
						masterDataRegister.setRazaosocial(row.get(5));
						masterDataRegister.setCnpjfilial(normalizeCnpj(row.get(6)));
						masterDataRegister.setCnpjmatriz(normalizeCnpj(row.get(7)));
						masterDataRegister.setAndar(getAndar(row.get(8)));
						masterDataRegister.setCategoria(row.get(9));
						masterDataRegister.setShoppingid(getShoppingId(row.get(11)));
						masterDataRegister.setTaxaonstore(row.get(12));
						masterDataRegister.setIdvtex(row.get(13));
						masterDataRegister.setSuc("");
						masterDataRegister.setBanco("1");
						masterDataRegister.setAgencia("1");

						log.debug(masterDataRegister.toString());

						ResponseEntity<MasterDataResponse> respAdminSeller = null;

						int nTriesAdmin = 0;
						boolean addedAdmin = false;
						while(nTriesAdmin < 10) {
							respAdminSeller = registerOnMasterData(masterDataRegister, RoleType.ADMIN_LOJISTA, null, false);

							if(respAdminSeller.getStatusCode() == HttpStatus.BAD_REQUEST ||
									respAdminSeller.getStatusCode() == HttpStatus.FORBIDDEN ||
									respAdminSeller.getStatusCode() == HttpStatus.UNAUTHORIZED
									) {
								// Consertando mismatch entre banco e código do banco
								masterDataRegister.setBanco(masterDataRegister.getCodigodobanco().toString());
							}
							else { // Retorna OK
								addedAdmin = true;
								break;
							}

							nTriesAdmin++;
						}
					}
				}
			}
		}

		return ResponseEntity.ok().build();
	}

	private Integer getAndar(String s) {
		switch (s) {
			case "G2":
				return -2;
			case "1S":
				return -1;
			case "T":
				return 0;
			case "1A":
				return 1;
			case "2A":
				return 2;
			case "3A":
				return 3;
			case "4A":
				return 4;
			case "5A":
				return 5;
			default:
				return 0;
		}
	}

	private String getShoppingId(String s) {
		switch(s) {
			case "CSP":
				return "shoppingcidadesp";
			case "GPS":
				return "grandplazashopping";
			case "SHC":
				return "shoppingcerrado";
			case "SHD":
				return "shoppingd";
			case "SMB":
				return "shoppingmetropolitanobarra";
			case "TPS":
				return "tieteplazashopping";
			default:
				return null;
		}
	}

	private String normalizeCnpj(String originalCnpj) {
		if(originalCnpj == null || originalCnpj.equals("")) {
			return originalCnpj;
		}
		String finalCnpj = new String(new char[14-originalCnpj.length()]).replace('\0', '0') + originalCnpj;
		log.debug(String.format("finalCnpj: %s", finalCnpj));
		return finalCnpj;
	}
}
