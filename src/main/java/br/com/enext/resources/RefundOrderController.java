package br.com.enext.resources;

import br.com.enext.model.RefundOrderBody;
import br.com.enext.model.RefundOrderResponse;
import br.com.enext.model.Seller;
import br.com.enext.services.SellerService;
import br.com.enext.utils.APICall;
import br.com.enext.utils.Enviroment;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpPut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/*
	Classe responsável por efetuar a chamada do endpoint /api/refund_order da Napp.
*/

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/refund_order")
@Slf4j
public class RefundOrderController {
    @Autowired
    SellerService sellerService;

    // TODO: Mudar os requests para recer Post e remover o @Request
    @PutMapping(path = { "", "/" })
    @CrossOrigin(origins = "*")
    public ResponseEntity<String> refundOrder(@RequestBody RefundOrderBody refundOrderBody) {
        try {
            log.debug("refundOrder chamado");
            log.debug(String.format("refundOrderBody: %s", refundOrderBody.toString()));

            Seller s = sellerService.getSellerFromFromSecurityContext();

            StringBuilder apiUrl = new StringBuilder();
            apiUrl.append(Enviroment.getEnv("REFUND_ORDER_ENDPOINT"));

            Map<String, String> headers = new HashMap<String, String>();
            headers.put("Content-Type", "application/json");

//          refundOrderBody.setSeller_env_id(s.getIdVtex());

          //TODO: Descomentar Chamada API NAPP para refund
            APICall call = new APICall(apiUrl.toString(), s.getToken(), HttpPut.class);
            try {
                ResponseEntity<RefundOrderResponse> responseEntity = call.put(RefundOrderResponse.class, RefundOrderBody.class, refundOrderBody, headers);
                return new ResponseEntity<>(responseEntity.getBody().getDetail(), responseEntity.getStatusCode());
            } catch (APICall.ApiCallErrorException e) {
                try {
                    Gson gson = new Gson();
                    RefundOrderResponse reundOrderResponse = gson.fromJson(e.getRespBody(), RefundOrderResponse.class);
                    return new ResponseEntity<>(reundOrderResponse.getDetail(), e.getHttpStatus());
                } catch(Exception e2) {
                    return e.getResponseEntity();
                }
            }
            
            //return new ResponseEntity("oi alef", HttpStatus.OK);
            
        } catch (SellerService.SellerNotFoundException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
