package br.com.enext.resources;

import br.com.enext.model.ApiCallError;
import br.com.enext.model.SelectOptionDto;
import br.com.enext.model.Seller;
import br.com.enext.model.SellerOptionsDto;
import br.com.enext.services.SellerService;
import br.com.enext.services.ShoppingsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/*
    Classe responsável por retornar uma lista de sellers de
    um shopping especifico através do endpoint /shoppings/{shoppingId}/seller-options.
*/

@Slf4j
@RestController
@RequestMapping("/shoppings")
public class SellerOptionsController {
    @Autowired
    SellerService sellerService;
    @Autowired
    ShoppingsService shoppingsService;

    @GetMapping(path= {"/{shoppingId}/seller-options"})
    public ResponseEntity getSellers(@PathVariable String shoppingId){
        log.debug("getSellers chamado");

        try {
            if(!shoppingsService.contains(shoppingId)) {
                return new ResponseEntity<>(
                        ApiCallError
                                .builder()
                                .Message(String.format("O shopping %s não foi encontrado", shoppingId))
                                .build(),
                        HttpStatus.NOT_FOUND
                );
            }

            Seller seller = sellerService.getSellerFromFromSecurityContext();

            if(seller.hasAccessToShoppingData(shoppingId)) {
                List<Seller> sellersOfShopping = sellerService.getSellersOfShopping(shoppingId);
                List<SelectOptionDto> selectOptionDtoList = new LinkedList<>();
                Set<String> idVtexSet = new HashSet<>();
                if(sellersOfShopping != null && sellersOfShopping.size() > 0) {
                    for(Seller s : sellersOfShopping) {
                        // Só sera retornado se o idvtex estiver definido e não pertencer a um shopping
                        // Valores repetidos não serão retornados
                        if(s.getIdVtex() != null && !"".equals(s.getIdVtex()) && !shoppingsService.contains(s.getIdVtex()) && !idVtexSet.contains(s.getIdVtex())) {
                            idVtexSet.add(s.getIdVtex());
                            selectOptionDtoList.add(SelectOptionDto.builder().id(s.getIdVtex()).name(s.getNomeFantasia()).build());
                        }
                    }
                }
                SellerOptionsDto sellerOptionsDto = new SellerOptionsDto();
                sellerOptionsDto.setContent(selectOptionDtoList);
                return ResponseEntity.ok(sellerOptionsDto);
            } else {
                return new ResponseEntity<>(
                        ApiCallError
                                .builder()
                                .Message(String.format("Usuário não possui acesso para obter os sellers do shopping %s", shoppingId))
                                .build(),
                        HttpStatus.UNAUTHORIZED
                );
            }
        } catch (SellerService.SellerNotFoundException e) {
            return new ResponseEntity<>(
                    ApiCallError
                            .builder()
                            .Message(String.format("Para obter os sellers do shopping %s você deve estar logado", shoppingId))
                            .build(),
                    HttpStatus.UNAUTHORIZED
            );
        }
    }
}
