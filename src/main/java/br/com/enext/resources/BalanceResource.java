package br.com.enext.resources;


import br.com.enext.model.Balance;
import br.com.enext.model.Bank;
import br.com.enext.model.Seller;
import br.com.enext.services.SellerService;
import br.com.enext.utils.APICall;
import br.com.enext.utils.DebugPrettyPrinter;
import br.com.enext.utils.Enviroment;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpGet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/*
    ******DEPRECIADA******

    Classe responsavel por efetuar chamada do enpoint Balance da Napp.
*/

@RestController
@RequestMapping("/balance")
@Slf4j
public class BalanceResource {

    @Autowired
    SellerService sellerService;

    @GetMapping(path = { "/{seller_env_id}"})
    public ResponseEntity<List<Balance>> getBalance(
            HttpServletRequest req,
            HttpServletResponse res,
            @PathVariable String seller_env_id
    )  {

        try{
            //Modificação Staging
            //Deixar somente conteudo do if
            Seller s = new Seller();
            s.setToken("StagingToken");
            if(false){
                s = sellerService.getSellerFromFromSecurityContext();
            }
            //********

            StringBuilder apiUrl = new StringBuilder();
            apiUrl.append("http://onstore-qa.nappsolutions.com:8001/api/sellers/");
            apiUrl.append(seller_env_id);
            apiUrl.append("/balances");

            APICall call = new APICall(apiUrl.toString(), s.getToken(), HttpGet.class);

            try {
                return call.get(Balance.class, new TypeToken<List<Balance>>(){}.getType());
            } catch (APICall.ApiCallErrorException e) {
                return e.getResponseEntity();
            }

        } catch (SellerService.SellerNotFoundException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
