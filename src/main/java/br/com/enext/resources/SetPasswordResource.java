package br.com.enext.resources;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.enext.enums.RoleType;
import br.com.enext.model.PasswordReplaceBody;
import br.com.enext.model.Seller;
import br.com.enext.model.SellerPassMessage;
import br.com.enext.repositories.SellerRepository;
import br.com.enext.services.SavePasswordInVtexService;
import br.com.enext.services.SellerService;
import br.com.enext.services.SellerService.SellerNotFoundException;
import lombok.extern.slf4j.Slf4j;

/*
	Classe responsável por gerenciar os endpoints de reset e replace [Esqueci minha senha] de senha do seller.
*/

@RestController
@RequestMapping("/configurations/password")
@Slf4j

public class SetPasswordResource {
	@Autowired
	SellerService sellerService;
	@Autowired
	SellerRepository sellerRepository;
	@Autowired
	private PasswordEncoder encoder;
	@Autowired
	SavePasswordInVtexService savePasswordInVtexService;

	@PostMapping(path = { "/replace" })
	public ResponseEntity<Seller> replacePassword(HttpServletRequest req, HttpServletResponse resp,
			@RequestBody @Valid PasswordReplaceBody newPass) {
		try {
			log.debug("replacePassword chamado");
			log.debug(newPass.getNewpassword());

			Seller loggedSeller = sellerService.getSellerFromFromSecurityContext();
			Seller sellerToSave = sellerRepository.findByRoleTypeAndCnpj(loggedSeller.getRoleType(),
					loggedSeller.getCnpj());

			boolean passIsEqual = encoder.matches(newPass.getOldpassword(), sellerToSave.getPassword());

			if (!passIsEqual) {
				log.debug("Senha atual incorreta");
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			} 
			else if (newPass.getNewpassword().equals(newPass.getOldpassword())) {
				log.debug("A nova senha é igual a antiga");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}

			sellerToSave.setPassword(encoder.encode(newPass.getNewpassword()));
			log.debug(sellerToSave.getCnpj());

			// Toda vez que um user define sua propria senha podemos considerar que a conta
			// nao é mais nova.
			log.debug("isNew setado para false");
			sellerToSave.setIsNew(false);

			Seller persistedSeller = sellerService.saveSeller(sellerToSave, sellerToSave.getIdVtex());

			if (persistedSeller == null) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			} 
			else {
				return ResponseEntity.status(HttpStatus.OK).build();
			}

		} catch (SellerNotFoundException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

	}

	// O ideal é o reset de senha receber quatro parâmetros: cnpj, email, tipo de usuário e shopping caso o tipo de usuário for ADMIN_SHOPPING
	@PostMapping(path = { "/reset" })
	public ResponseEntity<Seller> resetPassword(HttpServletRequest req, HttpServletResponse resp,
			@RequestBody SellerPassMessage newPass) {
		// Verificando se o Seller do cnpj informado existe.
		log.debug("resetPassword chamado");
		log.debug("Searching for seller");
		
		String formattedCnpj = "";
		
		for(char c : newPass.getCnpj().toCharArray()){
			if(Character.isDigit(c)) {
				formattedCnpj+=c;
			}
		}
		
		log.debug(formattedCnpj);
		
		Seller sellerToSave = sellerRepository.findFirstByCnpj(formattedCnpj);
		// Para diferenciar os shoppings, o cnpj deles foi cadastrado como cnpj de CCP + '_' + shoppingId
		// Portanto a validação de CNPJ não é necessária
		if(sellerToSave == null) {
			sellerToSave = sellerRepository.findFirstByCnpj(newPass.getCnpj());
		}
		if (sellerToSave == null) {
			log.debug("Seller not found.");
			return new ResponseEntity("Nenhum usuário associado ao cnpj em questão foi encontrado.", HttpStatus.BAD_REQUEST);
		} // Verificando se o email informado pertence ao cnpj informado.
		else if (!sellerToSave.getEmailResponsavel().equalsIgnoreCase(newPass.getEmailresponsavel())) {
			log.debug("Email does not belong to the CNPJ available.");
			return new ResponseEntity("Email não pertence ao CNPJ informado.", HttpStatus.BAD_REQUEST);
		}
		else if (sellerToSave.getRoleType() == RoleType.ADMIN_ONSTORES || sellerToSave.getRoleType() == RoleType.ADMIN_SHOPPING) {
			log.debug(String.format("sellerToSave.getRoleType(): %s", sellerToSave.getRoleType()));
			log.debug("Generating Password");
			String generatedPassword = RandomStringUtils.randomAlphanumeric(10);
			log.info(generatedPassword);

			boolean createdPasswordOnMasterdata = savePasswordInVtexService.savePassword(sellerToSave,
					sellerToSave.getVtexAccountName(), generatedPassword);
			Seller persistedSeller = null;

			if (createdPasswordOnMasterdata) {
				log.debug("Updating password in MongoDB");
				sellerToSave.setPassword(encoder.encode(generatedPassword));
				persistedSeller = sellerRepository.save(sellerToSave);
			} else {
				log.error("Senha não cadastrada no masterdata");
			}

			if (persistedSeller == null) {
				return new ResponseEntity("Ocorreu uma falha no processo de recuperação de senha, tente novamente.", HttpStatus.BAD_REQUEST);
			}
			else {
				return new ResponseEntity(String.format("Uma nova senha foi enviada para o email %s. Utilize-a para fazer o login. Por questões de segurança, altera-a assim que possível.", sellerToSave.getEmailResponsavel()), HttpStatus.OK);
			}
		}
		else {
			// Gerando e salvando password do ADMIN
			Seller adminSeller = sellerRepository.findByRoleTypeAndCnpj(RoleType.ADMIN_LOJISTA, sellerToSave.getCnpj());

			log.debug("Generating Admin Password");
			String generatedPassword = RandomStringUtils.randomAlphanumeric(10);
			log.info(generatedPassword);

			boolean createdPasswordOnMasterdata = savePasswordInVtexService.savePassword(adminSeller,
					adminSeller.getVtexAccountName(), generatedPassword);
			Seller persistedSellerAdmin = null;

			if (createdPasswordOnMasterdata) {
				log.debug("Updating admin password in MongoDB");
				adminSeller.setPassword(encoder.encode(generatedPassword));
				persistedSellerAdmin = sellerRepository.save(adminSeller);
			} else {
				log.error("Senha não cadastrada no masterdata");
			}

			if (persistedSellerAdmin == null) {
				return new ResponseEntity("Ocorreu uma falha no processo de recuperação de senha, tente novamente.", HttpStatus.BAD_REQUEST);
			}

			// Gerando e salvando password do STAFF

			Seller staffSeller = sellerRepository.findByRoleTypeAndCnpj(RoleType.FUNCIONARIO_LOJISTA,
					sellerToSave.getCnpj());
			log.debug("Generating Staff Password");
			generatedPassword = RandomStringUtils.randomAlphanumeric(10);
			log.info(generatedPassword);

			createdPasswordOnMasterdata = savePasswordInVtexService.savePassword(staffSeller,
					staffSeller.getVtexAccountName(), generatedPassword);
			Seller persistedSellerStaff = null;
			if (createdPasswordOnMasterdata) {
				log.debug("Updating staff password in MongoDB");
				staffSeller.setPassword(encoder.encode(generatedPassword));
				persistedSellerStaff = sellerRepository.save(staffSeller);
			} else {
				log.error("Senha não cadastrada no masterdata");
			}
			if (persistedSellerAdmin == null) {
				return new ResponseEntity("Ocorreu uma falha no processo de recuperação de senha, tente novamente.", HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity(String.format("Uma nova senha foi enviada para o email %s. Utilize-a para fazer o login. Por questões de segurança, altera-a assim que possível.", sellerToSave.getEmailResponsavel()), HttpStatus.OK);
		}
	}
}
