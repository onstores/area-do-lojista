package br.com.enext.listener;

import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Component;

import br.com.enext.model.Seller;
import br.com.enext.repositories.SessionRepository;

/*
	Classe responsável por deslogar um user quando um evento x for enviado.
*/

@Component
@Slf4j
public class LogoutListener implements ApplicationListener<SessionDestroyedEvent> {

	@Autowired
	private SessionRepository repository;

	@Override
	public void onApplicationEvent(SessionDestroyedEvent event) {
		log.debug(String.format("LogoutListener.onApplicationEvent foi chamado"));
		List<SecurityContext> lstSecurityContext = event.getSecurityContexts();
		Seller seller;
		for (SecurityContext securityContext : lstSecurityContext) {
			seller = (Seller) securityContext.getAuthentication().getPrincipal();
			if(seller != null) {
				repository.deleteBySessionToken(seller.getToken());
				log.info(String.format("Seller com cnpj %s do repositório após logout", seller.getCnpj()));
			}
		}
	}
}
