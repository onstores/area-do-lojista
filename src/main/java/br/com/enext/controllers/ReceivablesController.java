package br.com.enext.controllers;

import br.com.enext.model.Seller;
import br.com.enext.repositories.SellerRepository;
import br.com.enext.services.SellerService;
import br.com.enext.utils.Enviroment;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.UUID;

/*
    Classe responsável pelo gerenciamento geral das chamadas dos endpoints de Download
    [/api/receivables/(to_receive || received || analytical || seller)]

    Utiliza principalmente os métodos da classe DownloadController
*/

@Controller
@Slf4j
@RequestMapping("/api/receivables")
public class ReceivablesController extends BaseController {

    @Autowired
    SellerService sellerService;
    @Autowired
    DownloadController downloadController;

    //Depreciado
    @GetMapping("/to_receive")
    public void downloadToReceiveFile(
            HttpServletRequest req,
            HttpServletResponse res,
            @RequestParam(required = false) String start_date,
            @RequestParam(required = false) String end_date,
            @RequestParam(required = false, defaultValue = "json") String output
    ) throws IOException {
        HashMap<String, String> queryParametersHashMap = new HashMap<>();
        queryParametersHashMap.put("start_date", start_date);
        queryParametersHashMap.put("end_date", end_date);
        queryParametersHashMap.put("output", output);

        String nappUrl = downloadController.nappURLBuilder(queryParametersHashMap, "to_receive");

        Seller seller = null;

        try {
            seller = sellerService.getSellerFromFromSecurityContext();
        } catch (SellerService.SellerNotFoundException e) {
            res.sendError(401, "Você não está autenticado");
            return;
        }

        String s3Url = downloadController.generateReceivableS3Link(nappUrl, seller, start_date, end_date, output);
        res.sendRedirect(s3Url);
    }

    //Depreciado
    @GetMapping("/received")
    public void downloadReceivedFile(
            HttpServletRequest req,
            HttpServletResponse res,
            @RequestParam(required = false) String start_date,
            @RequestParam(required = false) String end_date,
            @RequestParam(required = false, defaultValue = "json") String output
    ) throws IOException {
        HashMap<String, String> queryParametersHashMap = new HashMap<>();
        queryParametersHashMap.put("start_date", start_date);
        queryParametersHashMap.put("end_date", end_date);
        queryParametersHashMap.put("output", output);

        String nappUrl = downloadController.nappURLBuilder(queryParametersHashMap, "received");

        Seller seller = null;

        try {
            seller = sellerService.getSellerFromFromSecurityContext();
        } catch (SellerService.SellerNotFoundException e) {
            res.sendError(401, "Você não está autenticado");
            return;
        }

        String s3Url = downloadController.generateReceivableS3Link(nappUrl, seller, start_date, end_date, output);
        res.sendRedirect(s3Url);
    }

    //Relatório analítico utilizado para os Roletypes ONSTORES e Shoppings
    @GetMapping("/analytical")
    public void downloadAnalyticalFile(
            HttpServletRequest req,
            HttpServletResponse res,
            @RequestParam(required = false) String seller_env_id,
            @RequestParam(required = false) String start_date,
            @RequestParam(required = false) String end_date,
            @RequestParam(required = false, defaultValue = "json") String output
    ) throws IOException {
        HashMap<String, String> queryParametersHashMap = new HashMap<>();
        queryParametersHashMap.put("seller_env_id", seller_env_id);
        queryParametersHashMap.put("start_date", start_date);
        queryParametersHashMap.put("end_date", end_date);
        queryParametersHashMap.put("output", output);
        String nappUrl = downloadController.nappURLBuilder(queryParametersHashMap, "analytical");

        Seller seller = null;

        try {
            seller = sellerService.getSellerFromFromSecurityContext();
        } catch (SellerService.SellerNotFoundException e) {
            res.sendError(401, "Você não está autenticado");
            return;
        }

        String s3Url = downloadController.generateReceivableS3Link(nappUrl, seller, start_date, end_date, output);
        res.sendRedirect(s3Url);
    }

    //Relatório analítico utilizado para o Roletype ADMIN_LOJISTA
    @GetMapping("/seller")
    public void downloadSellerAnalyticalFile(
            HttpServletRequest req,
            HttpServletResponse res,
            @RequestParam(required = false) String seller_env_id,
            @RequestParam(required = false) String start_date,
            @RequestParam(required = false) String end_date,
            @RequestParam(required = false, defaultValue = "json") String output
    ) throws IOException {
        HashMap<String, String> queryParametersHashMap = new HashMap<>();
        queryParametersHashMap.put("seller_env_id", seller_env_id);
        queryParametersHashMap.put("start_date", start_date);
        queryParametersHashMap.put("end_date", end_date);
        queryParametersHashMap.put("output", output);

        String nappUrl = downloadController.nappURLBuilder(queryParametersHashMap, "seller");

        Seller seller = null;

        try {
            seller = sellerService.getSellerFromFromSecurityContext();
        } catch (SellerService.SellerNotFoundException e) {
            res.sendError(401, "Você não está autenticado");
            return;
        }

        String s3Url = downloadController.generateReceivableS3Link(nappUrl, seller, start_date, end_date, output);
        res.sendRedirect(s3Url);
    }
}
