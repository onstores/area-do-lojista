package br.com.enext.controllers;

import br.com.enext.services.SellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/*
	Endpoint para retornar os arquivos estáticos de nossa Single Page Application
*/

@Controller
public class DashController {
	@Autowired
	SellerService sellerService;

	@RequestMapping(path= {"", "/admin/*", "/loja/**"})
	public ModelAndView dash() {
		try {
			sellerService.getSellerFromFromSecurityContext();
			return new ModelAndView("index2");
		} catch (SellerService.SellerNotFoundException e) {
			return new ModelAndView("redirect:/login");
		}
	}
}
