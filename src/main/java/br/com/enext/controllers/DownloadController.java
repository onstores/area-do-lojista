package br.com.enext.controllers;

import br.com.enext.model.Seller;
import br.com.enext.repositories.SellerRepository;
import br.com.enext.services.SellerService;
import br.com.enext.utils.Enviroment;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import software.amazon.ion.IonException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.UUID;

/*
    Classe responsável por:
    - Construi a URL de Download da Napp (RECEIVABLES_URL) de acordo com a solicitação [/to_receive, /received, /analytical & /seller]
    - Gerar link de Download a partir do Upload do arquivo no S3.
    - Criar conexão com a Napp e efetuar o download do arquivo para máquina local.
*/


@Controller
@Slf4j
public class DownloadController extends BaseController{

    @Autowired
    SellerService sellerService;
    @Autowired
    SellerRepository sellerRepository;
    @Autowired
    AmazonS3 s3Client;

    public String nappURLBuilder(
            HashMap<String, String> queryParametersHashMap,
            String urlDistinguisher
    ) throws IonException{
        String nappUrl = String.format(
                "%s/%s?%s",
                Enviroment.getEnv("RECEIVABLES_URL"),
                urlDistinguisher,
                getQueryParams(queryParametersHashMap)
        );

        log.debug(String.format("nappUrl: %s", nappUrl));

        return nappUrl;
    }

    public String generateReceivableS3Link(String nappUrl, Seller seller,
                                            String start_date, String end_date, String output) throws IOException {
        log.debug("--------------------------------------------");
        log.debug("--------------------------------------------");
        log.debug("--------------------------------------------");
        log.debug(start_date);
        log.debug(end_date);

        // Criando um nome aleatorio para o arquivo
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        String filename = randomUUIDString + "." + output;
        log.debug(String.format("filename: %s", filename));

        // Obtendo o arquivo da Napp
        File file = getXlsxFile(seller, nappUrl, filename, output);

        // Upload para o S3
        String s3ObjectKey = String.format("%s/%s", Enviroment.getEnv("AWS_S3_PROJ_FOLDER"), filename);
        log.info(String.format("Fazendo o upload do arquivo para o S3: {file: %s, s3ObjectKey: %s}", file, s3ObjectKey));
        PutObjectResult putObjectResult = s3Client.putObject(
                Enviroment.getEnv("AWS_S3_BUCKET_NAME"),
                s3ObjectKey,
                file
        );
        s3Client.setObjectAcl(Enviroment.getEnv("AWS_S3_BUCKET_NAME"), s3ObjectKey, CannedAccessControlList.PublicRead);

        // Deletando o arquivo local
        boolean result = Files.deleteIfExists(file.toPath()); //surround it in try catch block
        log.debug(String.format("delete file result: %s", result));

        // Redirecionando para o S3
        log.debug(String.format("%s/%s/%s", Enviroment.getEnv("AWS_S3_BASE_URL"), Enviroment.getEnv("AWS_S3_BUCKET_NAME"), s3ObjectKey));
        return String.format("%s/%s/%s", Enviroment.getEnv("AWS_S3_BASE_URL"), Enviroment.getEnv("AWS_S3_BUCKET_NAME"), s3ObjectKey);
    }

    private File getXlsxFile(Seller s, String urlAsString, String filename, String output) throws IOException {
        String fileFullPath = String.format("/tmp/%s", filename);

        URL url = new URL(urlAsString);

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        urlConnection.setRequestProperty("X-API-TOKEN", s.getToken());

        if("json".equals(output)) {
            urlConnection.setRequestProperty("Accept", "application/json");
        }

        try
        {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            OutputStream os = new FileOutputStream(fileFullPath);

            byte[] buffer = new byte[1024];
            int bytesRead;
            //read from is to buffer
            while((bytesRead = in.read(buffer)) !=-1){
                os.write(buffer, 0, bytesRead);
            }
            in.close();
            //flush OutputStream to write any buffered data to file
            os.flush();
            os.close();
        }
        finally
        {
            urlConnection.disconnect();
        }

        return new File(fileFullPath);
    }

}
