package br.com.enext.controllers;

import br.com.enext.services.SellerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/*
	Classe responsável pela lógica de redirect caso o login seja efetuado com sucesso.
*/

@Controller
@RequestMapping({"/",""})
@Slf4j
public class HomeController {
	@Autowired
	SellerService sellerService;

	@RequestMapping({"/",""})
	public ModelAndView index() {
		try {
			sellerService.getSellerFromFromSecurityContext();
			return new ModelAndView("redirect:/loja/minhas-vendas");
		} catch (SellerService.SellerNotFoundException e) {
			return new ModelAndView("redirect:/login");
		}
	}
}
