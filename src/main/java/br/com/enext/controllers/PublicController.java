package br.com.enext.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PublicController {
    @RequestMapping(path= {"/cadastro"})
    public ModelAndView publicEndpoints() {
        return new ModelAndView("index2");
    }
}
