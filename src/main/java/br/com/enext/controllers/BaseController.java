package br.com.enext.controllers;

import java.util.HashMap;

/*
    Classe base com método responsavel por formatar os query parameters na URL.
*/

public class BaseController {
    String getQueryParams(HashMap<String, String> queryParamHashMap) {
        String s = "";
        boolean isFirst = true;
        if(queryParamHashMap != null && queryParamHashMap.size() > 0) {
            for(String key : queryParamHashMap.keySet()) {
                String value = queryParamHashMap.get(key);
                if(value != null) {
                    if(isFirst) {
                        s += String.format("%s=%s", key, value);
                        isFirst = false;
                    }
                    else {
                        s += String.format("&%s=%s", key, value);
                    }
                }
            }
        }
        return s;
    }
}
