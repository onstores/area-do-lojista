package br.com.enext.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.enext.model.Login;

/*
	Classe responsável pelo direcionamento da chamada do endpoint /login para o index2.
*/

@Controller
@Slf4j
public class LoginController {
	@RequestMapping("/login")
	public String login(HttpServletRequest req, HttpServletResponse resp, Login login) {
		log.debug("Login foi chamado");
		return "index2";
	}
}
