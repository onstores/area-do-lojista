package br.com.enext.services;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.methods.HttpPut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.enext.model.MasterDataResponse;
import br.com.enext.model.Seller;
import br.com.enext.model.SellerPassMessage;
import br.com.enext.utils.APICall;
import br.com.enext.utils.Static;
import lombok.extern.slf4j.Slf4j;

/*
    Classe responsável por salvar uma password dada na tabela de senhas do Masterdata.
*/

@Service
@Slf4j

public class SavePasswordInVtexService {

    @Autowired
    VtexKeysService vtexKeysService;
	
	
	public boolean savePassword(Seller sellerInMasterData, String vtexAccountName, String password){
		log.debug("savePassword chamado.");
		
		StringBuilder endpoint = new StringBuilder();
        endpoint.append(Static.PREFFIXENDPOINT);
        endpoint.append(vtexKeysService.getUrl(vtexAccountName));
        endpoint.append(String.format(Static.SUFFIXENDPOINT, Static.PASSWORDSENDENTITY));

        APICall call = new APICall(endpoint.toString(), HttpPut.class);
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/vnd.vtex.ds.v10+json");
        headers.put("X-VTEX-API-AppKey", vtexKeysService.getAppKey(vtexAccountName));
        headers.put("X-VTEX-API-AppToken", vtexKeysService.getAppToken(vtexAccountName));
        log.info(endpoint.toString());
        SellerPassMessage sellerPassMessage = new SellerPassMessage();
        sellerPassMessage.setId(sellerInMasterData.getId());
        sellerPassMessage.setEmailresponsavel(sellerInMasterData.getEmailResponsavel());
        sellerPassMessage.setPassword(password);
        int count = 0;
        boolean createdPasswordOnMasterdata = false;
        while (count < 3) {
            ResponseEntity<MasterDataResponse> res = null;
            try {
                res = call.put(MasterDataResponse.class, SellerPassMessage.class, sellerPassMessage,
                        headers);
            } catch (APICall.ApiCallErrorException e) {
                res = e.getResponseEntity();
            }
            if(res.getStatusCode().is2xxSuccessful()) {
                createdPasswordOnMasterdata = true;
                break;
            }
            count++;
        }
		
		return createdPasswordOnMasterdata;
	}
 
}
