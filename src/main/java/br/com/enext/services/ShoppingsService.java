package br.com.enext.services;

import br.com.enext.model.SelectOptionDto;
import br.com.enext.model.ShoppingEntity;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
/*
    Classe responsável por
    - criar uma lista de shoppings de acordo com o idvtex retornando a lista.
    - Verificar se um shopping de um dado idvtex está presente na lista.
*/

@Service
public class ShoppingsService {
    private List<ShoppingEntity> shoppings;

    public ShoppingsService() {
        shoppings = new LinkedList<>();
        shoppings.add(new ShoppingEntity("grandplazashopping", "Grand Plaza Shopping"));
        shoppings.add(new ShoppingEntity("shoppingcerrado", "Shopping Cerrado"));
        shoppings.add(new ShoppingEntity("shoppingcidadesp", "Shopping Cidade SP"));
        shoppings.add(new ShoppingEntity("shoppingd", "Shopping D"));
        shoppings.add(new ShoppingEntity("shoppingmetropolitanobarra", "Shopping Metropolitano Barra"));
        shoppings.add(new ShoppingEntity("tieteplazashopping", "Tietê Plaza Shopping"));
    }

    public List<SelectOptionDto> getSelectOptionsDto() {
        List<SelectOptionDto> selectOptionDtos = new LinkedList<>();
        for(ShoppingEntity shoppingEntity : shoppings) {
            selectOptionDtos.add(SelectOptionDto
                    .builder()
                    .id(shoppingEntity.getId())
                    .name(shoppingEntity.getName())
                    .build()
            );
        }
        return selectOptionDtos;
    }

    public boolean contains(String idVtex) {
        for(ShoppingEntity shoppingEntity : shoppings) {
            if(shoppingEntity.getId().equals(idVtex)) {
                return true;
            }
        }
        return false;
    }
}
