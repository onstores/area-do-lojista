package br.com.enext.services;

import br.com.enext.utils.Enviroment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/*
    Classe responsável por definir encapsulamento para o X_VTEX_API_KEY E X_VTEX_API_TOKEN da Vtex dos shoppings e Onstores.
*/

@Service
@Slf4j
public class VtexKeysService {

    public String getAppKey(String accountName) {
        switch(accountName){
            case "onstores":
                return Enviroment.getEnv("X_VTEX_API_KEY_ONSTORES");
            case "shoppingcidadesp":
                return Enviroment.getEnv("X_VTEX_API_KEY_ONSERVICOSDIGITAIS");
            case "shoppingcerrado":
                return Enviroment.getEnv("X_VTEX_API_KEY_ONSERVICOSDIGITAIS");
            case "shoppingmetropolitanobarra":
                return Enviroment.getEnv("X_VTEX_API_KEY_ONSERVICOSDIGITAIS");
            case "shoppingd":
                return Enviroment.getEnv("X_VTEX_API_KEY_ONSERVICOSDIGITAIS");
            case "grandplazashopping":
                return Enviroment.getEnv("X_VTEX_API_KEY_ONSERVICOSDIGITAIS");
            case "tieteplazashopping":
                return Enviroment.getEnv("X_VTEX_API_KEY_ONSERVICOSDIGITAIS");
            case "onservicosdigitais":
                return Enviroment.getEnv("X_VTEX_API_KEY_ONSERVICOSDIGITAIS");
            default:
                log.debug("Shopping ID inválido.");
                return null;
        }
    }

    public String getAppToken(String accountName){
        switch(accountName) {
            case "onstores":
                return Enviroment.getEnv("X_VTEX_API_TOKEN_ONSTORES");
            case "shoppingcidadesp":
                return Enviroment.getEnv("X_VTEX_API_TOKEN_ONSERVICOSDIGITAIS");
            case "shoppingcerrado":
                return Enviroment.getEnv("X_VTEX_API_TOKEN_ONSERVICOSDIGITAIS");
            case "shoppingmetropolitanobarra":
                return Enviroment.getEnv("X_VTEX_API_TOKEN_ONSERVICOSDIGITAIS");
            case "shoppingd":
                return Enviroment.getEnv("X_VTEX_API_TOKEN_ONSERVICOSDIGITAIS");
            case "grandplazashopping":
                return Enviroment.getEnv("X_VTEX_API_TOKEN_ONSERVICOSDIGITAIS");
            case "tieteplazashopping":
                return Enviroment.getEnv("X_VTEX_API_TOKEN_ONSERVICOSDIGITAIS");
            case "onservicosdigitais":
                return Enviroment.getEnv("X_VTEX_API_TOKEN_ONSERVICOSDIGITAIS");
            default:
                log.debug("Shopping ID inválido.");
                return null;
        }
    }

    public String getUrl(String accountName){
        switch(accountName) {
            case "onstores":
                return "onstores";
            case "shoppingcidadesp":
                return "onservicosdigitais";
            case "shoppingcerrado":
                return "onservicosdigitais";
            case "shoppingmetropolitanobarra":
                return "onservicosdigitais";
            case "shoppingd":
                return "onservicosdigitais";
            case "grandplazashopping":
                return "onservicosdigitais";
            case "tieteplazashopping":
                return "onservicosdigitais";
            case "onservicosdigitais":
                return "onservicosdigitais";
            default:
                log.debug("Shopping ID inválido.");
                return null;
        }
    }
}
