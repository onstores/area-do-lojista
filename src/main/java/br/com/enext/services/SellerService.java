package br.com.enext.services;

import br.com.enext.model.Seller;
import br.com.enext.repositories.SellerRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/*
    Classe responsável por:
    - Buscar todos os filiais de um Seller Matriz
    - Retornar o usuário logado
    - Lógica de salvar o Seller no Masterdata e no MongoDB
    - Retornar todos os Sellers de um Shopping específico
    - Atualizar hora do último login
*/

@Service
@Slf4j
public class SellerService {
    @Autowired
    SellerRepository sellerRepository;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    VtexKeysService vtexKeysService;
    @Autowired
    SavePasswordInVtexService savePasswordInVtexService;
    

    /**
     *  Função que retorna uma lista com os sellerEnvIds dependentes.
     *  No caso em questão, se o seller for uma filial, será retornado apenas o sellerEnvId deste seller.
     *  Caso o seller seja uma uma matriz, será retornado o próprio sellerEnvId da matriz mais todos os sellerEnvIds
     *  que fazem referência ao seller matriz em questão por meio do campo cnpjMatriz.
     */
    // TODO: Finalizar de implementar o retorno das filiais
    public List<String> getAllDependentSellerEnvIds(String cnpj) {
        log.debug(String.format("getAllDependentSellerEnvIds chamado com cnpj: %s", cnpj));

        List<String> allDependentSellerEnvIds = new LinkedList<>();

        Seller currSeller = sellerRepository.findFirstByCnpj(cnpj);

        allDependentSellerEnvIds.add(currSeller.getIdVtex());

        List<Seller> filiais = sellerRepository.findByCnpjMatriz(cnpj);

        log.debug(String.format("filiais.size(): %s", filiais.size()));

        for(Seller s : filiais) {
            log.debug(String.format("seller in dependents: %s", s));
            // Só são incluídas as filiais ativas
            if(s.isEnabled()){
                allDependentSellerEnvIds.add(s.getIdVtex());
            }
        }

        // Retornando seller_env_ids distintos
        return allDependentSellerEnvIds.stream().distinct().collect(Collectors.toList());
    }

    public Seller getSellerFromFromSecurityContext() throws SellerNotFoundException {
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication auth = context.getAuthentication();

        try {
            Seller s = (Seller) auth.getPrincipal();
            if(s == null) {
                throw new SellerNotFoundException();
            }
            log.debug(String.format("Seller obtido do SecurityContext: %s", s));
            return s;
        }
        catch(ClassCastException e) {
            log.debug("Seller não foi encontrado");
            throw new SellerNotFoundException();
        }
    }

    public Seller saveSeller(Seller sellerInMasterData, String vtexAccountName) {
        log.info("Searching if seller exists");
        Optional<Seller> sellerOptional = sellerRepository.findById(sellerInMasterData.getId());
        sellerInMasterData.setTermwasaccepted(true);


        String generatedPassword = null;
        Seller persistedSeller = null;

        // Criando uma nova senha caso o usuário não exista no banco
        if (!sellerOptional.isPresent()) {
            generatedPassword = RandomStringUtils.randomAlphanumeric(10);
            log.info("Seller dont exists, creating password");

            sellerInMasterData.setPassword(encoder.encode(generatedPassword));
            log.info(generatedPassword);
            
            sellerInMasterData.setIsNew(true);
            log.debug("isNew setado para true");

            boolean createdPasswordOnMasterdata = savePasswordInVtexService.savePassword(sellerInMasterData, vtexAccountName, generatedPassword);

            if(createdPasswordOnMasterdata) {
                sellerInMasterData.setPassword(encoder.encode(generatedPassword));
                persistedSeller = sellerRepository.save(sellerInMasterData);
            }
            else {
                log.error("Senha não cadastrada no masterdata");
            }
        }
        else {
            log.info("Seller já existe, apenas atualizando seus dados no banco de dados mongoDB");
            Seller originalSeller = sellerOptional.get();
            modelMapper.map(sellerInMasterData, originalSeller);
            persistedSeller = sellerRepository.save(originalSeller);
        }

        return persistedSeller;
    }

    public List<Seller> getSellersOfShopping(String shoppingId) {
        return sellerRepository.findByShoppingIdOrderByNomeFantasia(shoppingId);
    }

    public void updateLoginTimes(Seller seller) throws SellerNotFoundException {
        log.debug("updateLoginTimes iniciou");

        Optional<Seller> newSellerOptional = sellerRepository.findById(seller.getId());

        Seller newSeller = newSellerOptional.orElseThrow(SellerNotFoundException::new);

        LocalDateTime today = LocalDateTime.now();  //Create date
        ZoneId id = ZoneId.of("America/Sao_Paulo");  //Create timezone
        ZonedDateTime zonedDateTime = ZonedDateTime.of(today, id);
//      DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        
        log.debug(timestamp.toString());
        newSeller.setLastLoginAt(seller.getCurrentLoginAt());
        newSeller.setCurrentLoginAt(timestamp.toString());

        log.debug("updateLoginTimes finalizou");

        log.debug(String.format("%s", newSeller));

        sellerRepository.save(newSeller);
    }

    public static class SellerNotFoundException extends Exception {}
}
