package br.com.enext.services;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.enext.model.Seller;
import br.com.enext.repositories.SellerRepository;

import java.util.Optional;

/*
	Classe responsável por
	- gerar e retornar o token de sessão do seller.
	- Retornar nova instância do encriptador de senha (BCryptPasswordEncoder).
*/

@Service
@Transactional
@Slf4j
public class SellerDetailService implements UserDetailsService {
	
	@Autowired
	private SellerRepository repository;
	
	@Override
	public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
		log.info("Searching for Seller");
		Optional<Seller> sellerOptional = repository.findById(id);
		if (!sellerOptional.isPresent()) {
			log.info("Seller not found");
            throw new UsernameNotFoundException(
              "No user found with username: "+ id);
        }
        else {
			Seller s = sellerOptional.get();
			String token = RandomStringUtils.randomAlphanumeric(60);
			s.setToken(token);
			log.info("Instantiating Seller");
			log.info(String.format("s.getToken(): %s", s.getToken()));
			return  s;
		}
	}
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
