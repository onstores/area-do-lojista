package br.com.enext.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import br.com.enext.model.Seller;
import br.com.enext.model.Session;
import br.com.enext.repositories.SessionRepository;

/*
	Classe responsável por, ao logout ser efetuado com sucesso, deletar o session do user
	na tabela Session no MongoDB e redirecionar-lo para a página de login.
*/

@Slf4j
public class SuccessfullLogoutHandler implements LogoutSuccessHandler {

	@Autowired
	private SessionRepository repository;

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public void onLogoutSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth)
			throws IOException, ServletException {

		log.debug("onLogoutSuccess foi chamado");

		if (auth != null) {
			Seller seller = (Seller) auth.getPrincipal();

			if (seller != null) {
				Session s = repository.findBySessionToken(seller.getToken());

				if (s != null) {
					repository.deleteBySessionToken(seller.getToken());
					log.debug(String.format("Seller de token %s foi removido com sucesso do banco de dados", seller.getToken()));
				} else {
					log.debug("Session com o cnpj de auth.getPrincipal() não foi encontrado");
				}
			}
		}

		redirectStrategy.sendRedirect(req, res, "/login");
	}
}
