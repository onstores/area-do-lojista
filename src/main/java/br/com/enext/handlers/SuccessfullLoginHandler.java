package br.com.enext.handlers;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.enext.services.SellerService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import br.com.enext.model.Seller;
import br.com.enext.model.Session;
import br.com.enext.repositories.SessionRepository;

/*
	Classe responsável por, ao login ser efetuado com sucesso, criar o session do user, guardar
	na tabela Session no MongoDB e redirecionar-lo para a página principal.
*/

@Slf4j
public class SuccessfullLoginHandler implements AuthenticationSuccessHandler {
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Autowired
	private SessionRepository repository;
	@Autowired
	SellerService sellerService;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth)
			throws IOException, ServletException {
		req.getSession().setMaxInactiveInterval(30*60);
		log.info("Usuário logado com sucesso");
		Seller seller = (Seller) auth.getPrincipal();

		Date date = new Date();
		Session session = new Session(seller.getId(), seller.getToken(), date);
		log.debug(String.format("O seguinte session foi salvo no repositório: %s", session));
		repository.save(session);
		
		log.debug(String.format("O usuário logado é o %s", seller));

		try {
			sellerService.updateLoginTimes(seller);
		} catch (SellerService.SellerNotFoundException e) {
			log.debug("sellerService.updateLoginTimes falhou");
		}

		redirectStrategy.sendRedirect(req, res, "/loja/recebimentos");
	}
}
