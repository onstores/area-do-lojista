package br.com.enext.filters;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
	Classe responsável pela lógica de autenticação do filtro para os endpoints que comessem com /resources/.
	Caso o endpoint entre no filtro, será necessário que haja no header os parâmetros X-API-KEY e X-API-TOKEN.
*/

@Slf4j
public class ResourceHeaderFilter implements Filter{
	private FilterConfig filterConfig = null;
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		log.debug("Filtro ResourceHeaderFilter chamado");

		HttpServletRequest hreq = (HttpServletRequest) req;
		HttpServletResponse hres = (HttpServletResponse) res;

		log.debug(String.format("hreq: %s", hreq));

		String token = hreq.getHeader("X-API-TOKEN") != null ? hreq.getHeader("X-API-TOKEN") : "";
		String key = hreq.getHeader("X-API-KEY") != null ? hreq.getHeader("X-API-KEY") : "" ;

		log.debug(String.format("token no header do request: %s", token));
		log.debug(String.format("key no header do request: %s", key));
		log.debug(String.format("filterConfig.getInitParameter(\"X-API-TOKEN\"): %s", filterConfig.getInitParameter("X-API-TOKEN")));
		log.debug(String.format("filterConfig.getInitParameter(\"X-API-KEY\"): %s", filterConfig.getInitParameter("X-API-KEY")));

		if(token.equals(filterConfig.getInitParameter("X-API-TOKEN")) &&
				key.equals(filterConfig.getInitParameter("X-API-KEY"))) {
			log.info("X-API-TOKEN e X-API-KEY válidos. Filtro passou");
			chain.doFilter(req, res);
		}else {
			log.error("Faltando/inválido X-API-KEY e/ou X-API-TOKEN");
			hres.setStatus(401);
		}
	}
	
	public void init(FilterConfig filterConfig) {
		log.info(String.format("ResourceHeaderFilter.init chamado com filterConfig: %s", filterConfig));
		Enumeration<String> initParams = filterConfig.getInitParameterNames();
		log.debug("filterConfig params:");
		while(initParams.hasMoreElements()) {
			String paramKey = initParams.nextElement();
			log.debug(String.format("(%s, %s)", paramKey, filterConfig.getInitParameter(paramKey)));
		}
		this.filterConfig = filterConfig;
  	}
}
