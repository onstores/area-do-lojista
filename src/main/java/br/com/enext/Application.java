package br.com.enext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import br.com.enext.repositories.SessionRepository;

/*
	Classe incial
	- Starta a aplicação Spring.
*/

@SpringBootApplication
public class Application {
	@Autowired
	private SessionRepository repository;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	@EventListener(ApplicationReadyEvent.class)
    public void EventListenerExecute(){
		logger.info("cleaning session token cache");
        //repository.deleteAll();
    }
}
