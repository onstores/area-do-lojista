<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<meta http-equiv="Content-Language" content="pt-br">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />
	<title>On Stores</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato" />
	<link rel="stylesheet" href="/css/desktop.css" />
	<script>
		(function() {
			function initScope(meta) {
				return {BaseUrl: baseUrl, compMap: {}, meta: meta, scopes: scopes};
			}
			var baseUrl = "/";
			var scopes = {
				"App":{"JS_GLOBAL":"_app$","PREFIX":"app","COMP_PATH_PREFIX":"app/comp/","NAME":"App"},
				"VComp":{"JS_GLOBAL":"_vcomp$","PREFIX":"vcomp","COMP_URL":"https://unpkg.com/@arijs/frontend@0.1.5/vcomp/","NAME":"VComp"},
				"ENext":{"JS_GLOBAL":"_enext$","PREFIX":"enext","COMP_PATH_PREFIX":"enext/","NAME":"ENext"},
				"Global":{"JS_GLOBAL":"_var$","NAME":"Global"}
			};
			this._app$ = initScope(scopes.App);
			this._vcomp$ = initScope(scopes.VComp);
			this._enext$ = initScope(scopes.ENext);
			this._var$ = initScope(scopes.Global);
			this._app$.compInit = [{"MOUNT":"#mount","ID":"app/root","SAVE":"$root"}];
			this._app$.device = "desktop";
		})();
	</script>
</head>
<body class="root-desktop">
	<div id="mount"></div>
	<script src="https://unpkg.com/@arijs/frontend@0.1.5/utils/javascript.js"></script>
	<script src="https://unpkg.com/@arijs/frontend@0.1.5/utils/loaders.js"></script>
	<script src="https://unpkg.com/@arijs/frontend@0.1.5/utils/form.js"></script>
	<script src="https://unpkg.com/@arijs/frontend@0.1.5/utils/string.js"></script>
	<script src="https://unpkg.com/@arijs/frontend@0.1.5/utils/animation.js"></script>
	<script src="/vendor/js/vue.js"></script>
	<script src="https://unpkg.com/vuex@3.0.1/dist/vuex.min.js"></script>
	<script src="https://unpkg.com/vue-router@3.0.2/dist/vue-router.min.js"></script>
	<script src="https://unpkg.com/vue-text-mask@6.1.2/dist/vueTextMask.js"></script>
	<script src="/app/env/prod.js"></script>
	<script src="/app/services.js"></script>
	<script src="/app/store.js"></script>
	<script src="/app/router.js"></script>
	<script src="/app/initialize.js"></script>
</body>
</html>
