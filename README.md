# Projeto Onstores

## Objetivo

O projeto OnStores é uma plataforma web desenvolvida em Spring focada em possibilitar que um lojista possa:  
* Verificar quais são os pedidos efetuados em sua loja web, entrega-los e estorna-los.  
* Verificar quais são seus recebíveis, diferenciando-se entre 'recebidos' e 'à receber'.  

## Tecnologias e Framworks utilizados

### Spring Framework  
[https://spring.io/](https://spring.io/)  

### MongoDB  
[https://www.mongodb.com/](https://www.mongodb.com/)  

### MasterData  
[https://help.vtex.com/tutorial/o-que-e-o-master-data?locale=pt](https://help.vtex.com/tutorial/o-que-e-o-master-data?locale=pt)  

Acesso ao MasterData:  
* Shopping Cidade Sp:  
[https://shoppingcidadesp.vtexcrm.com.br/#](https://shoppingcidadesp.vtexcrm.com.br/#)  
* Shopping Cerrado:  
[https://shoppingcerrado.vtexcrm.com.br/#](https://shoppingcerrado.vtexcrm.com.br/#)  
* Grand Plaza Shopping:  
[https://grandplazashopping.vtexcrm.com.br/#](https://grandplazashopping.vtexcrm.com.br/#)  
* Shopping D:  
[https://shoppingd.vtexcrm.com.br/#](https://shoppingd.vtexcrm.com.br/#)  
* Shopping Metroplitano Barra:  
[https://shoppingmetropolitanobarra.vtexcrm.com.br/#](https://shoppingmetropolitanobarra.vtexcrm.com.br/#)  
* Tiete Plaza Shopping:  
[https://tieteplazashopping.vtexcrm.com.br/#](https://tieteplazashopping.vtexcrm.com.br/#)

### Docker / Docker Compose

[https://www.docker.com/](https://www.docker.com/)  
[https://docs.docker.com/compose/](https://docs.docker.com/compose/)

### AWS EC2

[https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Home:](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Home:)


## Regras de Negócio

#### Hierarquia Matriz-Filial
O projeto possui um sistema hierárquico para quando houver mais de uma loja física por franquia.  
Para quando esse caso existir, é necessário que haja uma Loja Matriz e as restantes serem filiais:  
* Matriz: Possui visão de todos os dados (pedidos e recebíveis) de todas as suas lojas filiais, e de seus próprios dados.  
  
Para efetuar a criação de uma loja Matriz voce deve:  
1. Criar uma loja comum com o CNPJ que será utilizado como matriz.  
2. Ao criar uma loja que será filial, informar o CNPJ da loja Matriz no campo CNPJ Matriz.  

OBS:  
* É possível criar n lojas Matriz, porém vai contra a ideia da regra de negócio.  

![Hierarquia Matriz](./readme_assets/hierarquia-matriz.png)

#### RoleType
No projeto existe a necessidade de diferenciar a visão que cada usuário possui de acordo com sua função, com isso foi criado quatro RoleTypes distintos para atender todas as demandas:
* ADMIN_ONSTORES: Possui visão de todos os dados (Pedidos e recebíveis) de todas as lojas de todos os shoppings. (Admin Master)  
* ADMIN_SHOPPING: Possui visão de todo os dados (Pedidos e recebíveis) de todas as lojas de seu shopping.  
* ADMIN_LOJISTA: Possui visão de todo os dados (Pedidos e recebíveis) de sua loja.  
* FUNCIONARIO_LOJISTA: Possui visão de somente os Pedidos de sua loja.  
  
#### Criação de usuário  
A loja é cadastrada pela UI de cadastro:  
[https://areadolojista.onstores.com.br/cadastro](https://areadolojista.onstores.com.br/cadastro)  

Ao efetuar o cadastro da loja:  
1. O User de RoleType ADMIN_LOJISTA é criado e enviado para o Masterdata setando o campo "Cadastro aprovado" para false.  
2. O Masterdata, ao receber o novo user na tabela "Área do Lojista", aciona os triggers:  
    * 'Cadastro Ambiente Seller' que chama o endpoint /resource/seller.  
    * 'email_envio_senha' que envia um email contendo o login e a senha para o seller.  
3. O endpoint /resource/seller chamado efetua a criação do seller com RoleType FUNCIONARIO_LOJISTA, clonando as informações do ADMIN_LOJISTA, e salva ambos no MongoDB.  
  
OBS:  
* Para que user seja aprovado é necessário acessar a Vtex do shopping e alterar o campo "Cadastro aprovado" para true [Esse processo é efetuado pelo pessoal da Cyrela].  
* Caso o seller tente acessar com o cadastro ainda não aprovado, ele simplesmente será retornado para a tela de login sem nenhuma mensagem de erro.  

#### Download de Relatórios
  
Os dados dos relatórios são gerados pela Napp.  
O endpoint /api/receivables/* retorna todos os dados de acordo com a solicitação (/to_receive || /received || /analytical || /seller). Porém, como a chamada precisa de autenticação, não foi possível gerar um link de download direto do endpoint.  
Para resolver esse impecílio, foi desenvolvida a seguinte lógica:  
1. É aberta a conexão com a Napp através do endpoint /api/receivables/*;  
2. O arquivo gerado pelo endpoint é guardado na máquina (servidor) local;  
3. É aberta comunicação com o S3 da AWS e o arquivo gerado é guardado no repositório com timelife de dois dias;  
4. O link de Download é gerado e chamado;  
5. O arquivo local é apagado.

#### Troca e recuperação de senha

Recuperação de senha:  
* O usuário consegue recuperar sua senha pela UI de recuperação disponível na UI de login através do botão "Esqueceu sua senha?".  
* Ao inserir seu CNPJ e email cadastrado, é gerada uma nova senha que substitui a anterior no MongoDB. Após isso, ela é enviada para o Masterdata.  
* Com esse envio, um trigger de envio de email é acionado, enviando a nova senha via email.  
  
Troca de senha:  
* O usuário consegue troca sua senha pela UI de configuração da area do logista ao estar logado.  
* Quando confirmar a senha atual e inserir sua nova senha, a nova senha substitui a senha antiga no MongoDB e NÃO é enviada para o Masterdata.  

#### Chamadas API NAPP

Para todo endpoint chamado da Napp, é necessário informar o token de sessão do usuário para que eles possam estar confirmando se o seller realmente está logado. Para isso, desenvolvemos uma espécie de Triple Handshake:  
1. Chamamos o endpoint da napp passando os dados, quando necessário, no body como Json e o token no header;  
2. A Napp, com o token de sessão em mãos, chama nosso endpoint /resource/token que, validando o token, retorna o seller_env_id e o RoleType do seller ativo.  
3. Ao receber os dados do seller ativo, a Napp trabalha para retornar o que foi solicitado pela chamada do Endpoint, finalizando a chamada.  

#### Notificações

Para que seja possível a verificação das notificações já visualizadas, foi desenvolvido a seguinte lógica:  
* Três timestamps são gerados e guardados nas informações do Seller no MongoDB nos campos 'lastLoginAt, 'currentLoginAt' e 'sawNotificationsAt';  
    * lastLoginAt: Ultima data em que o user fez login;
    * currentLoginAt: Data do login Atual;
    * sawNotificationsAt: Ultima data em que o Seller visualizou as notificações.
* Com esses timestamps, o front é capaz de medir quando foi a ultima vez que o seller visualizou as notificações e quais itens foram visualizados.

# Ambiente local

## Clonar repositório na pasta que deseja

Versão java: 1.8.0_252
Versão javac: 1.8.0_252

## Baixar tomcat(http://tomcat.apache.org/) e configurar

![Baixar tomcat](./readme_assets/baixar-tomcat.png)

![Descompactar tomcat](./readme_assets/descompactar-tomcat.png)

```
cd apache-tomcat-9.0.35/;
chmod 700 bin/*;
./bin/startup.sh;
```

## Ajustar pasta webapps, renomear a pasta ROOT para index

![Renomar ROOT - passo 1](./readme_assets/renomear-root-1.png)

![Renomar ROOT - passo 2](./readme_assets/renomear-root-2.png)

## Configurar variáveis de ambiente e buildar repositório

```
cd onstore.seller.admin/;
```

## Configurar as variáveis de ambiente presentes em devops/dev-local.env

## Build war

```
git checkout develop;
mvn clean; mvn package -DskipTests;
cd target/;
```

![Renomar war - passo 1](./readme_assets/renomear-war-1.png)

![Renomar war - passo 2](./readme_assets/renomear-war-2.png)

## Colocar war no tomcat

Copiar arquivo ROOT.war para a pasta webapps dentro do diretório tomcat

![Colocar war em webapps - passo 1](./readme_assets/colocar-arquivo-war-webapps.png)

Como o tomcat está rodando, em instantes uma pasta com o mesmo nome do arquivo ROOT.war será criada

Após alguns segundos, acessar http://localhost:8080/ no navegador.

![Ambiente local funcionando](./readme_assets/ambiente-local-funcionando.png)

# Processo de deploy

## Gerar o war com os arquivos do diretório .ebextensions

```
mvn clean;
mvn package -DskipTests;
zip -ur target/onstore.seller.admin.war .ebextensions;
```

## Fazer o upload do war para o ELB

[https://docs.aws.amazon.com/pt_br/elasticbeanstalk/latest/dg/using-features.deploy-existing-version.html](https://docs.aws.amazon.com/pt_br/elasticbeanstalk/latest/dg/using-features.deploy-existing-version.html)

# Subdomínio apontando para o servidor da OnStore na AWS hospedado no serviço Elastic Beanstalk

1. Criar o certificado com o AWS Certificate Manager: [https://console.aws.amazon.com/acm/home?region=us-east-1#/](https://console.aws.amazon.com/acm/home?region=us-east-1#/)
2. Configurar o Load Balancer para suportar https: [https://docs.aws.amazon.com/pt_br/elasticbeanstalk/latest/dg/configuring-https-elb.html](https://docs.aws.amazon.com/pt_br/elasticbeanstalk/latest/dg/configuring-https-elb.html)
3. Configurar o redirecionamento de http para https: [https://stackoverflow.com/questions/14693852/how-to-force-https-on-elastic-beanstalk](https://stackoverflow.com/questions/14693852/how-to-force-https-on-elastic-beanstalk)

* Cuidado para no registro CNAME não incluir o domínio duas vezes ao inseri-lo no campo host. Ex.: No processo de gração do certificado pelo AWS CM, só devo colocar _978aaa788e68ef2e35823c88e9d50333.arealojista no host e não _978aaa788e68ef2e35823c88e9d50333.arealojista.mprado.me 

![](./readme_assets/load_balancer_config.png)

# Setup MongoDB (Instância t2.micro Ubuntu 20.04)

## Instalando docker-ce=17.12.0~ce-0~ubuntu

```
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common -y;
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable";
sudo apt-get update;
sudo apt-get install docker-ce=17.12.0~ce-0~ubuntu -y;
```

## Executar o mongodb sem autenticação

```
sudo docker run --name mongodb -p 27017:27017 -v ~/mongodb_data:/data/db -d mongo:3.6.2;
```

## Instalar o mongo client e conectar ao mongo

```
sudo apt install mongodb-clients;
mongo;
```

## Criar o usuário

```
use admin
db.createUser({
    user: 'root',
    pwd: '******',
    roles: [{ role: 'readWrite', db:'onstores'}]
})
```

## Reinciar o mongo com a flag --auth

```
sudo docker stop mongodb;
sudo docker rm mongodb;
sudo docker run --name mongodb -p 27017:27017 -v ~/mongodb_data:/data/db -d mongo:3.6.2 --auth;
```
## Criando regra de backup automático (EC2 Snapshots)

Link da documentação
[https://docs.aws.amazon.com/pt_br/AmazonCloudWatch/latest/events/TakeScheduledSnapshot.html](https://docs.aws.amazon.com/pt_br/AmazonCloudWatch/latest/events/TakeScheduledSnapshot.html)


# TODO

```
Nothing here.
```

# Setup ambiente de Staging (Ubuntu 20.04)

## Conectar no servidor 

```
ssh -i "loja-onstores.pem" ubuntu@ec2-34-238-25-203.compute-1.amazonaws.com
```

## Instalando docker-ce=17.12.0~ce-0~ubuntu

```
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common -y;
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable";
sudo apt-get update;
sudo apt-get install docker-ce=17.12.0~ce-0~ubuntu -y;
```
## Instalando docker-compose=1.19.0

```
sudo curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose;
sudo chmod +x /usr/local/bin/docker-compose;
```

## Instalar o mvn

```
sudo apt-get install mvn;
```

## Clonando/fazendo pull no repositório

```
cd ~;
git clone https://marcoprado17@bitbucket.org/enext/onstore.seller.admin.git;
git checkout develop;
```

## Criando o diretório com os arquivos estáticos do site e habilitando a escrita pelo user ubuntu

```
cd ~;
mkdir pub;
sudo chown -R ubuntu:daemon /home/ubuntu/pub;
sudo chmod -R g+w /home/ubuntu/pub;
```

# Atualizando o repositório, compilando o .war e iniciando os containers docker no ambiente de staging

## Conectar no servidor 

```
ssh -i "loja-onstores.pem" ubuntu@ec2-34-238-25-203.compute-1.amazonaws.com
```

## fazendo pull no repositório e reiniciando os containers

```
sudo /home/ubuntu/onstore.seller.admin/bash_scripts/restart_staging.sh;
```

# Acessando os logs no ambiente de staging

```
cd ~/onstore.seller.admin;
sudo docker-compose -f devops/docker-compose.yaml logs --follow --tail 100 tomcat;
sudo docker-compose -f devops/docker-compose.yaml logs --follow --tail 100 nginx;
```
