# Onstore Área do Logista

## 1. Integração Aplicação - Master Data

Finalizado endpoint para receber as informações do Master Data e persisti-lo na aplicação, foram utilizadas as seguintes tecnologias:

1. Linguagem de Programação: Java;
2. Framework de desenvolvimento: Spring, Spring Security;
3. Persistência de dados: MongoDB;
4. Trigger de alteração de dados no Master Data;

### Fluxo

1. O seller se cadastrará na loja pedindo acesso; 
2. Após constatados as informações o administrador do shopping habilita o seller;
3. Na hora que o campo de habilitação é alterado o Master Data enviará um json para nosso endpoint (ainda a ser definido na seguinte estrutura):

```

{
	"id" : "shopping_xxxx_xxxxx", // pensei em concaternar nome do shopping + cnpj
	"cnpj" : "xxxxxxxxxxx",
	"razaoSocial" : "Enext Consultoria de Ecommerce",
	"nomeFantasia" : "Enext",
	"suc" : "1", // não sei qual o tipo de informação é o suc String ou int
	"andar" : "3", // Mantive andar como string para podermos trabalhar com variações em shoppings que possuem mais de uma torre
	"antecipacao" : true | false,
	"categoria" : "xxxxxxxxxx",
	"telefoneResponsavel" : "119232312343",
	"emailResponsavel" : "xxxxxx@xxxx.com.br",
	"enviarEmail" : true,
	"nomeResponsavel" : "XXXXX XXXXX XXXXX",
	"taxaonstore" : 0.01,
	"datarecebimento" : "YYYY-MM-dd",
	"bankInfo" : {
		"banco" : "xxxxx",
		"agencia" : "XXXX",
		"contaCorrente" : "XXXXX-X",
		"comprovanteBancario" : "base64_do_arquivo ou url"
	},
	"roleType" : {
		"nome" : "ADMIN",
		"permissoes" : ["create", "read"]
	}
}

```

## 2. Login e Controle de sessão

Finalizadas ação de Login e Controle de sessão utilizando Spring Security, além disso informações da sessão, tais como token de autenticação e id do seller, serão armazenadas no MongoDB para uso posterior de validação da API da Omnicore.

## 3. Próximos Passos

- Criação do Enpoint para Validação das Requests por parte da Omnicore - entrega prevista para 9/11;
- Disponibilização das rotas online - entrega prevista para 9/11;
- Consumir as API's da Omnicore para fornecer informações dinâmicas para o Front - entrega do consumo de todas as rotas prevista para 21/11;

## 4. Dependências
Segunda-feira (12/11) começo o processo para o consumo das rotas, precisarei dos enpoints (urls) dos serviços, o endpoint do email, não estava funcionando. Essa parte tem como depêndencia a entrega do nosso endpoint de validação de token, porém seria interessante se conseguissem nos disponibilizar algum endpoint de teste.

